<?php
include_once 'Builder.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
class Package_builder extends Builder
{
    protected $_config;
    protected $_render_list = [];
    protected $_routes = [];
    protected $_file_path = '';
    protected $_template = '';
    protected $_locale = null;

    public function __construct($config, $locale)
    {
        $this->_config = $config;
        $this->_template = '';
        $this->_locale = $locale;
    }

    public function set_package ($package)
    {
        $this->_packages = $package;
    }

    public function get_routes ()
    {
        $routes = [];
        foreach ($this->_packages as $package_type => $allow)
        {
            if ($allow)
            {
                switch ($package_type)
                {
                    case 'voice':
                        $routes['v1/api/voice/gather'] = 'Guest/Voice_controller/gather';
                }
            }
        }
        return $routes;
    }

    public function build()
    {

        foreach ($this->_packages as $package_type => $allow)
        {
            if ($allow)
            {
                switch ($package_type)
                {
                    case 'pdf':
                        $template = file_get_contents('templates/source/pdf/Pdf_service.php');
                        $template = $this->inject_substitute($template, 'subclass_prefix', $this->_config['subclass_prefix']);
                        file_put_contents('src/application/libraries/Pdf_service.php', $template);
                        break;
                    case 'voice':
                        $template = file_get_contents('templates/source/voice/Voice_service.php');
                        $template = $this->inject_substitute($template, 'subclass_prefix', $this->_config['subclass_prefix']);
                        file_put_contents('src/application/libraries/Voice_service.php', $template);

                        $template = file_get_contents('templates/source/voice/Voice_controller.php');
                        $template = $this->inject_substitute($template, 'subclass_prefix', $this->_config['subclass_prefix']);
                        file_put_contents('src/application/controllers/Guest/Voice_controller.php', $template);
                    default:
                        # code...
                        break;
                }
            }
        }
    }

    public function destroy()
    {
        $destroy_list = [
            'src/application/libraries/Pdf_service.php',
            'src/application/libraries/Voice_service.php',
            'src/application/controllers/Guest/Voice_controller.php'
        ];

        foreach ($destroy_list as $key => $value)
        {
            if (file_exists($value))
            {
                unlink($value);
            }
        }
    }
}
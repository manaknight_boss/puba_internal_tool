<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
?>
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="page-header">
			<h2 class="pageheader-title"><?php echo $view_model->get_heading();?> </h2>
			<div class="page-breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/admin/dashboard" class="breadcrumb-link">Dashboard</a></li>
						<li class="breadcrumb-item"><a href="/admin/dvd/0" class="breadcrumb-link"><?php echo $view_model->get_heading();?></a></li>
						<li class="breadcrumb-item active" aria-current="page">Add</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</div>
<div class="row">
    <?php if (validation_errors()) : ?>
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?= validation_errors() ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (strlen($error) > 0) : ?>
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (strlen($success) > 0) : ?>
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    <?php endif; ?>
</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Add <?php echo $view_model->get_heading();?></h5>
                <div class="card-body">
					<?= form_open() ?>
						<div class='form-row'>
						    <div class="form-group col-md-4">
								<label for="Title">Title </label>
								<input type="text" class="form-control" id="form_title" name="title" value="<?php echo set_value('title', $this->_data['view_model']->get_title());?>"/>
							</div>
							<div class="form-group col-md-4">
								<label>Company</label>		
								<select name='company_id' value="<?php echo set_value('company_id'); ?>" class="form-control">
                    				<option  value='0' selected>Choose...</option>
                    				<?php foreach($this->_data[0]["view_data"]['companies'] as $company ): ?>
                    					<option value="<?= $company->id?>"><?= $company->name ?></option>
                    				<?php endforeach;?>
                				</select>		
							</div>
							<div class="form-group col-md-4">
								<label>Production Start Date</label>		
								<label for="Start Of Production">Start Of Production </label>
								<input type="date" class="form-control" id="form_start_production_date" name="start_production_date" value="<?php echo set_value('start_production_date'); ?>"/>	
							</div>			
						</div>
						<div class="form-row">
							<div class="form-group col-md-6">
								<label>Select Studio</label>
								<select name='studio_id' value="<?php echo set_value('studio_id'); ?>" class="form-control">
                    				<option  value='0' selected>Choose...</option>
                    				<?php foreach($this->_data[0]["view_data"]['studios'] as $studio ): ?>
                    					<option value="<?= $studio->id?>"><?= $studio->name ?></option>
                    				<?php endforeach;?>
                				</select>		
							</div>
							<div class="form-group col-md-6">
								<label for="End Of Production">End Of Production </label>
								<input type="date" class="form-control" id="form_end_production_date" name="end_production_date" value="<?php echo set_value('end_production_date'); ?>"/>		
							</div>			
						</div>
						<div class="form-row">
							<div class="form-group">
								<select name="filter_after" class='form-control'>
									<option value="1">1 Months</option>
									<option value="2">2 Months</option>
									<option value="3">3 Months</option>
									<option value="4">4 Months</option>
									<option value="5">5 Months</option>
									<option value="6">6 Months</option>
									<option value="7">7 Months</option>
									<option value="8">8 Months</option>
									<option value="9">9 Months</option>
									<option value="10">10 Months</option>
									<option value="11">11 Months</option>
									<option value="12">12 Months</option>	
								</select>		
							</div>
							<div class="form-group col-md-6">
								<label>Select Type</label>
								<select name='type_id' value="<?php echo set_value('type_id'); ?>" class="form-control">
                   	 				<option  value='0' selected>Choose...</option>
                    				<?php foreach($this->_data[0]["view_data"]['types'] as $type ): ?>
                    					<option value="<?= $type->id?>"><?= $type->name ?></option>
                    				<?php endforeach;?>
                				</select>		
							</div>			
						</div>
						<div class="form-row">
							<div class="form-group col-md-6">
								<label>Categories</label>
            					<input type='hidden' id="categories-object" value="<?php echo htmlspecialchars(json_encode($this->_data[0]["view_data"]['categories'])); ?>">
            					<input type="text" id="categories-auto-complete" class="form-control"  placeholder="Categories">
            					<input type="hidden" name="dvd_categories" id="dvd_categories" value="">
            					<div id='categories-list'></div>		
							</div>
							<div class="form-group col-md-6">
								<label>Net Categories</label>
            					<input type="text" id="net-categories-auto-complete" class="form-control"  placeholder="Net Categories">
            					<input type="hidden" name="dvd_net_categories" id="dvd_net_categories" value="">
            					<div id='dvd_net_categories-list'></div>		
							</div>			
						</div>
						<div class="form-row">
							<div class="form-group col-md-6">
								<label>Filter Scene </label>
								<select id='scene_filer'  class="form-control">
									<option> Choose...</option>		
								</select>		
							</div>			
						</div>
						<div class="form-row">
                			<div class="form-group">
                    			<input type="submit" class="btn btn-primary" value="Submit">
                			</div>
						</div>
                	</form>
           	 	</div>
        	</div>
    	</div>
	</div>
</div>
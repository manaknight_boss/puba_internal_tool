<?php

class Snippet_service
{
    /**
     * Wrap Text
     *
     * @param [type] $text
     * @param [type] $width
     * @param [type] $indent
     * @return void
     */
    public function wrap_text($text, $width, $indent)
    {
        $wrapped    = "";
        $paragraphs = explode("\n", $text);
        foreach($paragraphs as $paragraph)
        {
            if ($indent > 0)
            {
                $wrapped .= str_repeat("&nbsp;", $indent);
            }
            $words = explode(" ", $paragraph);
            $len   = $indent;
            foreach($words as $word)
            {
                $wlen = strlen($word);

                if (($len + $wlen) < $width)
                {
                    $wrapped .= "$word ";
                    $len     += $wlen + 1;
                }
                else
                {
                    $wrapped = rtrim($wrapped);
                    $wrapped .= "<br />\n$word ";
                    $len      = $wlen;
                }
            }

            $wrapped = rtrim($wrapped);
            $wrapped .= "<br />\n";
        }

        return $wrapped;
    }

    public function spell_check($text, $action)
    {
        $dictionary = $this->load_dictionary("dictionary.txt");
        $text .= ' ';
        $newtext = "";
        $offset = 0;
        while ($offset < strlen($text))
        {
            $result = preg_match('/[^\w]*([\w]+)[^\w]+/',
            $text, $matches, PREG_OFFSET_CAPTURE, $offset);
            $word   = $matches[1][0];
            $offset = $matches[0][1] + strlen($matches[0][0]);
            if (!$this->spell_check_word($word, $dictionary))
            {
                $newtext .= "<$action>$word</$action> ";
            }
            else
            {
                $newtext .= "$word ";
            }
        }
        return rtrim($newtext);
     }

     public function load_dictionary($filename)
     {
        return explode("\r\n", file_get_contents($filename));
     }

     public function spell_check_word($word, $dictionary)
     {
        $top = sizeof($dictionary) - 1;
        $bot  = 0;
        $word = strtolower($word);

        while($top >= $bot)
        {
            $p =   floor(($top + $bot) / 2);
            if     ($dictionary[$p] < $word)
            {
                $bot = $p + 1;
            }
            elseif ($dictionary[$p] > $word)
            {
                $top = $p - 1;
            }
            else
            {
                return TRUE;
            }
        }

        return FALSE;
     }

    public function remove_accents($text)
    {
        $from = [
            "ç", "æ", "œ", "á", "é", "í", "ó", "ú", "à", "è",
            "ì", "ò", "ù", "ä", "ë", "ï", "ö", "ü", "ÿ", "â",
            "ê", "î", "ô", "û", "å", "e", "i", "ø", "u", "Ç",
            "Æ", "Œ", "Á", "É", "Í", "Ó", "Ú", "À", "È", "Ì",
            "Ò", "Ù", "Ä", "Ë", "Ï", "Ö", "Ü", "Ÿ", "Â", "Ê",
            "Î", "Ô", "Û", "Å", "Ø"
        ];

        $to = [
            "c", "ae", "oe", "a", "e", "i", "o", "u", "a", "e",
            "i", "o", "u", "a", "e", "i", "o", "u", "y", "a",
            "e", "i", "o", "u", "a", "e", "i", "o", "u", "C",
            "AE", "OE", "A", "E", "I", "O", "U", "A", "E", "I",
            "O", "U", "A", "E", "I", "O", "U", "Y", "A", "E",
            "I", "O", "U", "A", "O"
        ];

        return str_replace($from, $to, $text);
    }

    public function upload_file($name, $filetypes, $maxlen)
    {
        if (!isset($_FILES[$name]['name']))
        {
            return array(-1, NULL, NULL);
        }

        if (!in_array($_FILES[$name]['type'], $filetypes))
        {
            return array(-2, NULL, NULL);
        }

        if ($_FILES[$name]['size'] > $maxlen)
        {
            return array(-3, NULL, NULL);
        }

        if ($_FILES[$name]['error'] > 0)
        {
            return array($_FILES[$name]['error'], NULL, NULL);
        }

        $temp = file_get_contents($_FILES[$name]['tmp_name']);
        return array(0, $_FILES[$name]['type'], $temp);
    }

    public function image_resize($image, $w, $h)
    {
        $oldw = imagesx($image);
        $oldh = imagesy($image);
        $temp = imagecreatetruecolor($w, $h);
        imagecopyresampled($temp, $image, 0, 0, 0, 0, $w, $h, $oldw, $oldh);
        return $temp;
    }

    public function make_thumbnail($image, $max)
    {
        $thumbw = $w = imagesx($image);
        $thumbh = $h = imagesy($image);
        if ($w > $h && $max < $w)
        {
            $thumbh = $max / $w * $h;
            $thumbw = $max;
        }
        elseif ($h > $w && $max < $h)
        {
            $thumbw = $max / $h * $w;
            $thumbh = $max;
        }
        elseif ($max < $w)
        {
            $thumbw = $thumbh = $max;
        }
        return $this->image_resize($image, $thumbw, $thumbh);
    }

    public function image_crop($image, $x, $y, $w, $h)
    {
        $tw = imagesx($image);
        $th = imagesy($image);
        if ($x > $tw || $y > $th || $w > $tw || $h > $th)
        {
            return FALSE;
        }
        $temp = imagecreatetruecolor($w, $h);
        imagecopyresampled($temp, $image, 0, 0, $x, $y,
        $w, $h, $w, $h);
        return $temp;
    }

    public function image_enlarge($image, $w, $h, $smoothing)
    {
        $oldw  = imagesx($image);
        $step  = 3.1415927 * ((100 - $smoothing) / 100);
        $max   = $w / $step;
        $ratio = $h / $w;
        for ($j = $oldw ; $j < $max; $j += $step)
        {
           $image = $this->image_resize($image, $j * $step,
              $j * $step * $ratio);
        }

        return $this->image_resize($image, $w, $h);
    }

    public function image_watermark($fromfile, $tofile, $type,
    $quality, $text, $font, $size, $fore, $opacity)
    {
        $contents = file_get_contents($fromfile);
        $image1   = imagecreatefromstring($contents);
        $bound    = imagettfbbox($size, 0, $font, $text);
        $width    = $bound[2] + $bound[0] + 6;
        $height   = abs($bound[1]) + abs($bound[7]) + 5;
        $image2   = imagecreatetruecolor($width, $height);
        $bgcol    = $this->gd_fn1($image2, "fedcba");
        $fgcol    = $this->gd_fn1($image2, $fore);
        imagecolortransparent($image2, $bgcol);
        imagefilledrectangle($image2, 0, 0, $width, $height, $bgcol);
        imagettftext($image2, $size, 0, 2, abs($bound[5]) + 2, $fgcol, $font, $text);
        imagecopymerge($image1, $image2,
            (imagesx($image1) - $width) / 2,
            (imagesy($image1) - $height) / 2,
            0, 0, $width, $height, $opacity);

        switch($type)
        {
            case 'gif':
                imagegif($image1,  $tofile);
                break;
            case 'jpeg':
                imagejpeg($image1, $tofile, $quality);
                break;
            case 'png':
                imagepng($image1,  $tofile, round(9 - $quality * .09));
                break;
        }
    }

    public function gd_fn1($image, $color)
    {
        return imagecolorallocate($image,
        hexdec(substr($color, 0, 2)),
        hexdec(substr($color, 2, 2)),
        hexdec(substr($color, 4, 2)));
    }

    public function rolling_copyright($message, $year)
    {
        return "$message &copy;$year-" . date("Y");
    }

    public function validate_credit_Card($number, $expiry)
    {
        $ccnum  = preg_replace('/[^\d]/', '', $number);
        $expiry = preg_replace('/[^\d]/', '', $expiry);
        $left   = substr($ccnum, 0, 4);
        $cclen  = strlen($ccnum);
        $chksum = 0;

        // Diners Club
        if (($left >= 3000) && ($left <= 3059) ||
           ($left >= 3600) && ($left <= 3699) ||
           ($left >= 3800) && ($left <= 3889))
        {
            if ($cclen != 14)
            {
                return FALSE;
            }

        }

        // JCB
        if (($left >= 3088) && ($left <= 3094) ||
        ($left >= 3096) && ($left <= 3102) ||
        ($left >= 3112) && ($left <= 3120) ||
        ($left >= 3158) && ($left <= 3159) ||
        ($left >= 3337) && ($left <= 3349) ||
        ($left >= 3528) && ($left <= 3589))
        {
            if ($cclen != 16)
            {
                return FALSE;
            }

        }
        // American Express
        elseif (($left >= 3400) && ($left <= 3499) ||
                ($left >= 3700) && ($left <= 3799))
        {
                if ($cclen != 15)
                {
                    return FALSE;
                }
        }
        // Carte Blanche
        elseif (($left >= 3890) && ($left <= 3899))
        {
            if ($cclen != 14) return FALSE;
        }
        // Visa
        elseif (($left >= 4000) && ($left <= 4999))
        {
            if ($cclen != 13 && $cclen != 16)
            {
                return FALSE;
            }
        }
        // MasterCard
        elseif (($left >= 5100) && ($left <= 5599))
        {
            if ($cclen != 16)
            {
                return FALSE;
            }
        }
        // Australian BankCard
        elseif ($left == 5610)
        {
            if ($cclen != 16)
            {
                return FALSE;
            }
        }
        // Discover
        elseif ($left == 6011)
        {
            if ($cclen != 16)
            {
                return FALSE;
            }
        }
        // Unknown
        else
        {
            return FALSE;
        }

        for ($j = 1 - ($cclen % 2); $j < $cclen; $j += 2)
        {
            $chksum += substr($ccnum, $j, 1);
        }

        for ($j = $cclen % 2; $j < $cclen; $j += 2)
        {
            $d = substr($ccnum, $j, 1) * 2;
            $chksum += $d < 10 ? $d : $d - 9;
        }

        if ($chksum % 10 != 0)
        {
            return FALSE;
        }

        if (mktime(0, 0, 0, substr($expiry, 0, 2), date("t"),
            substr($expiry, 2, 2)) < time())
        {
            return FALSE;
        }

        return TRUE;
    }


}
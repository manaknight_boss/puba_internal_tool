<?php 
include_once dirname(__FILE__) . '/../../core/CLI_Controller.php';
 
class Scene_comp_cronjob_controller extends CLI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
   
    public function comp_scene_to_next_studio()
    {
        $this->load->database();
        $this->load->model('Scenes_model', 'scenes');
        $scenes_array = $this->scenes->get_all();
        $current_date =  date("Y-m-d", strtotime( date("Y-m-d")));
        foreach($scenes_array as $scene_obj)
        {
            if($scene_obj->is_for_comp !== 1)
            {
                if(date("Y-m-d", strtotime( $scene_obj->comp_after_date)) < $current_date)
                {
                    $temp = [
                       'studio_id' =>  ($scene_obj->next_studio_id === null ? $scene_obj->studio_id : $scene_obj->next_studio_id ),
                       'next_studio_id' => null,
                       'is_for_comp' => 1
                    ];
                    $this->scenes->edit($temp, $scene_obj->id);
                    $temp = [];
                }
            }
        }
        exit();
    }
    
}
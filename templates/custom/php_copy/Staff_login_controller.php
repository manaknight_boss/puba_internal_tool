<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once __DIR__ . '/../../services/User_service.php';
include_once 'Staff_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Login Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Staff_login_controller extends Manaknight_Controller
{
	protected $_redirect = '/staff/dashboard';

    public function __construct()
    {
        parent::__construct();
    }

	public function index ()
	{
        $this->load->library('user_agent');
        $this->load->model('Login_log_model');
        $this->load->model('user_model');
        $this->load->helper('cookie');
        $this->config->load('Ip_whitelist');

        $service = new User_service($this->user_model);

        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() === FALSE)
        {
            echo $this->load->view('Staff/Login', $this->_data, TRUE);
            exit;
        }

        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $user_ip_address = $this->input->ip_address();
        $redirect = $service->get_redirect($this->input->cookie('redirect', TRUE), $this->_redirect);
        
        if(!$this->input->valid_ip($user_ip_address))
        {
            $this->error('Wrong email or password.');
            return $this->redirect('staff/login');
        }

        if(in_array($user_ip_address, $this->config->item('permitted_ips') ))
        {
            $authenticated_user = $service->login($email, $password);

            if ($authenticated_user)
            {
                delete_cookie('redirect');
                $this->set_session('user_id', (int) $authenticated_user->id);
                $this->set_session('email', (string) $authenticated_user->email);
                $this->set_session('role', (string) $authenticated_user->role_id);
                $this->Login_log_model->create([
                    'user_agent' =>  $this->agent->agent_string(),
                    'ip' =>  $user_ip_address,
                    'user_id' => $authenticated_user->id
                ]);
                return $this->redirect($redirect);
            }
        }

        $this->error('Wrong email or password.');
        return $this->redirect('staff/login');
    }

    public function logout ()
    {
        $this->destroy_session();
		return $this->redirect('staff/login');
    }
}
<!DOCTYPE html>
<html>
<head>
	<title>Dvd Cast</title>
	<style>
		*{
			padding:  0;
			margin: 0;
			font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
		}
		#header-section{
			width: 100%;
			padding: 3%;
			background-color: #f3f3f3;
		}
		#content{
			margin-top:  10px;
			padding: 2%;
			width: 80%;
			margin: auto;
			border-top : 1px solid #ccc;
		}
		table th{
			float: left;
		}
	</style>
</head>
<body>
	<div id="header-section">
		<h4><i>Mr Makamoto</i> 18 U.S.C. 2257 PRODUCTION SHEET</h4>
		<h5> Production <small><?= $dvd_details['title'] ?></small></h5>
		<h5> Primary Producer <small><?= $dvd_details['name'] ?></small></h5>
		<h5> Address <small><?= $dvd_details['address'] ?></small></h5>
	</div>
	<div id='content'>
		<h5>Number of scenes <?= count($dvd_scenes)?></h5><br><hr></hr>
		<?php for($i = 0; $i < count($dvd_scenes) ;$i ++):?>
			<table>
				<thead>
					<tr>
						<th>Scene <?= $i + 1 ?> <?= $dvd_scenes[$i]['scene']->title?> </th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<h3><small>Scene Actors</small></h3><br>
							<?php foreach($dvd_scenes[$i]['scene_actors'] as $actor ):?>
								<h5> Legal Name <small><?= $actor['name'] ?></small></h5>
								<h5> Stage Name <small><?= $actor['stage_name'] ?></small></h5>
								<h5> Credited As <small><?= $dvd_scenes[$i]['scene']->set_id ?></small></h5>	
								<h5>DOP <small><?=  $dvd_scenes[$i]['scene']->date_released?></small></h5>
								<br>
							<?php endforeach;?>	
						</td>
					</tr>
				</tbody>
			</table>
			<br><hr></hr>
		<?php endfor;?>	
	</div>
</body>
</html>
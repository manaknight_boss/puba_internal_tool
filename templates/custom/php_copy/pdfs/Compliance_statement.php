<!DOCTYPE html>
<html>
<head>
	<title>Compliance Statement</title>
	<style>
		*{
			padding:  0;
			margin: 0;
			font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
		}
		#header-section{
			width: 100%;
			padding: 3%;
			background-color: #f3f3f3;
		}
		#content{
			margin-top:  10px;
			padding: 2%;
			width: 80%;
			margin: auto;
			border-top : 1px solid #ccc;
		}
		table th{
			float: left;
		}
	</style>
</head>
</head>
<body>
	<div id="header-section">
		<h2> COMPLIANCE STATEMENT</h2>
		<h5> Primary Producer <small><?= $dvd_details['name'] ?></small></h5>
	</div>
	<div id='content'>
		<p>
			I am the primary producer of the videotape, DVD CD Rom, or other media, and all associated graphical images,the title of which is below. The last day on which the attached images were created is also listed below.<br><br>						
		   	The following statement is made in compliance with the provisions of 18 U.S.C. § 2257 and 28 CFR 75.Any images that were created prior to July 3, 1995, are therefore exempt.I have examined all documents and materials and can confirm that they contain true and correct information.<br><br>						
			I am the Custodian of Records. The original records are located at 5821 Mac Duff Court, Las Vegas, NV 89141.						
			I declare under penalty of perjury that the foregoing is true and correct.				
		</p><br>
		<h3><?= $dvd_details['title'] ?></h3>


	</div>
</body>
</html>
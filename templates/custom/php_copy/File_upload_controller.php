<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class File_upload_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function upload_multiple_files()
	{	
		$file_array = [];
        
        foreach($_FILES['file']['name'] as $key=>$val)
		{
			$file_name = $_FILES['file']['name'][$key];
			$hash = substr(hash('md5',date("m.d.y")), 0, 15);
			$allowed_files = ['pdf', 'doc', 'docx', 'png', 'jpeg', 'jpg' ];
			$ext = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));
			$file = strtolower(pathinfo($file_name, PATHINFO_FILENAME));
            
            if(in_array($ext, $allowed_files))
			{
				if(move_uploaded_file($_FILES['file']['tmp_name'][$key], getcwd(). '/uploads/'. $file . $hash. $ext ))
				{
                     $file_array[] = $file. $hash. $ext;
				}
			}
		}
        
        echo json_encode($file_array);
		exit();
	}
}
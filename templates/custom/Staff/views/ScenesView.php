<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
?>
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="page-header" id="top">
			<h2 class="pageheader-title"><?php echo $view_model->get_heading();?> </h2>
			<div class="page-breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/staff/dashboard" class="breadcrumb-link">Dashboard</a></li>
						<li class="breadcrumb-item"><a href="/staff/scenes/0" class="breadcrumb-link"><?php echo $view_model->get_heading();?></a></li>
						<li class="breadcrumb-item active" aria-current="page">View</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header"><?php echo $view_model->get_heading();?> Details</h5>
                <div class="card-body">
						<h6>title:&nbsp; <?php echo $view_model->get_title();?></h6>
						<h6>tubeclip url:&nbsp; <?php echo $view_model->get_tubeclip_url();?></h6>
						<h6>xvideos url:&nbsp; <?php echo $view_model->get_xvideos_url();?></h6>
						<h6>type:&nbsp; <?php echo $view_model->get_type();?></h6>
						<h6>Producer Id:&nbsp; <?php echo $view_model->get_primary_producer_id();?></h6>
						<h6>Group Id:&nbsp; <?php echo $view_model->get_group_id();?></h6>
						<h6>channel:&nbsp; <?php echo $view_model->get_channel();?></h6>
						<h6>Set Id:&nbsp; <?php echo $view_model->get_set_id();?></h6>
						<h6>Tss url:&nbsp; <?php echo $view_model->get_tss_url();?></h6>
						<h6>media type:&nbsp; <?php echo $view_model->get_media_type();?></h6>
						<h6>Select:&nbsp; <?php echo $view_model->get_studio_id();?></h6>
						<h6>Is For Comp:&nbsp; <?php echo $view_model->is_for_comp_mapping()[$view_model->get_is_for_comp()];?></h6>

                </div>
        </div>
    </div>
</div>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
?>
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="page-header">
			<h2 class="pageheader-title"><?php echo $view_model->get_heading();?> </h2>
			<div class="page-breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/staff/dashboard" class="breadcrumb-link">Dashboard</a></li>
						<li class="breadcrumb-item"><a href="/staff/dvd/0" class="breadcrumb-link"><?php echo $view_model->get_heading();?></a></li>
						<li class="breadcrumb-item active" aria-current="page">Add</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</div>
<div class="row">
    <?php if (validation_errors()) : ?>
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?= validation_errors() ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (strlen($error) > 0) : ?>
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (strlen($success) > 0) : ?>
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    <?php endif; ?>
</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header"><?php echo $view_model->get_heading();?></h5>
                <div class="card-body">
					<?= form_open() ?>
						<div class='form-row'>
							<div class="form-group col-md-4">
								<label>Company</label>		
								<select name='company_id' id='company_id' value="<?php echo set_value('company_id'); ?>" class="form-control">
                    				<option  value='0' selected>Choose...</option>
                    				<?php foreach($this->_data['view_data']['companies'] as $company ): ?>
                    					<option value="<?php echo $company->company_id; ?>"><?php echo $company->name; ?></option>
                    				<?php endforeach;?>
                				</select>		
							</div>
							<div class="form-group col-md-4">		
								<label for="Start Of Production">Start Of Production </label>
								<input type="date" class="form-control" id="form_start_production_date" name="start_production_date" value="<?php echo set_value('start_production_date'); ?>"/>	
							</div>			
						</div>
						<div class="form-row">
							<div class="form-group col-md-4">
								<label>Select Studio</label>
								<select name='studio_id' id='studio_id' value="<?php echo set_value('studio_id'); ?>" class="form-control">
                    				<option  value='0' selected>Choose...</option>
                				</select>		
							</div>
							<div class="form-group col-md-4">
								<label for="End Of Production">End Of Production </label>
								<input type="date" class="form-control" id="form_end_production_date" name="end_production_date" value="<?php echo set_value('end_production_date'); ?>"/>		
							</div>			
							<div class="form-group col-md-4">
								<label for="Filter After">Filter After (Months)</label>
								<select name="filter_after" class='form-control'>
									<option value='3'>3 Months</option>	
									<option value='6'>6 Months</option>	
								</select>
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-md-6">
								<label>Select Type</label>
								<select name='type_id' value="<?php echo set_value('type_id'); ?>" class="form-control">
                   	 				<option  value='0' selected>Choose...</option>
                    				<?php foreach($this->_data['view_data']['types'] as $type ): ?>
                    					<option value="<?php echo $type->id; ?>"><?php echo $type->name; ?></option>
                    				<?php endforeach;?>
                				</select>		
							</div>
							<div class='form-group col-md-6'>
								<label for="Status">Status </label>
								<select id="form_status" name="status" class="form-control">
									<?php foreach ($view_model->status_mapping() as $key => $value) {
										echo "<option value='{$key}'> {$value} </option>";
									}?>
								</select>
							</div>			
						</div>
						<div class="form-row">
							<div class="form-group col-md-6">
							    <label>Categories</label>	
								<select class='multiple-select-control' name='dvd_categories[]' id="example-filter-placeholder" multiple="multiple">
									<?php foreach($this->_data['view_data']['categories'] as $cat ): ?>
                    					<option value="<?php echo $cat['id']; ?>"><?php echo $cat['name']; ?></option>
                    				<?php endforeach;?>
								</select>
							</div>
							<div class="form-group col-md-6">
								<label>Not Categories</label>
            					<select class='multiple-select-control' name='dvd_net_categories[]' id="example-filter-placeholder" multiple="multiple">
									<?php foreach($this->_data['view_data']['categories'] as $cat ): ?>
                    					<option value="<?php echo $cat['id']; ?>"><?php echo $cat['name']; ?></option>
                    				<?php endforeach;?>
								</select>	
							</div>			
						</div>
						<div class="form-row">
							<div class="form-group col-md-6">
								<label>Filter Scene By Comp </label>
								<select id='scene_filer'  class="form-control">
									<option value='1'>1 month</option>
									<option value='2'>2 months</option>
									<option value='3'>3 months</option>
									<option value='4'>4 months</option>
									<option value='5'>5 months</option>
									<option value='6' selected>6 months</option>
									<option value='7'>7 months</option>
									<option value='8'>8 months</option>
									<option value='9'>9 months</option>
									<option value='10'>10 months</option>
									<option value='11'>11 months</option>
									<option value='12'>12 months</option>		
								</select>		
							</div>			
						</div>
						<div class="row">
							<div class="col col-md-12 card p-2">
								<strong>Selected Scenes</strong>
								<input type='hidden'  name='dvd_scenes' id="dvd_scenes" value=""/>
								<input type='hidden' name='dvd_cover_scene' id='dvd_cover_scene' value=""/>
								<table class='table'>
									<thead>
										<th>Scene</th>
										<th>Studio</th>
										<th>Status</th>
										<th>Choose</th>
										<th>Remove</th>
									</thead>
									<tbody id='selected-dvd-scenes' data-action='add' data-id='0'>
											
									</tbody>
								</table>		
							</div>		
						</div>
						<div class='form-row'>
                            <div class="col col-md-12 pd-1 ">
								<?php foreach($this->_data['view_data']['categories'] as $cat):?>
									<a style='padding:1%;'href="#" class='filter-scene-by-category btn-link' data-id="<?=$cat['id'] ?>" >
										<?php echo $cat['name']; ?>(<?php echo $cat['items']; ?>)
									</a>
								<?php endforeach;?>	
							</div>
							<hr></hr>
						</div>
						<div class="form-row">
							<div class='col col-md-12 card p-2'>
                                <table class='table'>
									<strong>Filter Scenes</strong>
									<thead class='thead-dark'>
                                        <th>Scene</th>
                                        <th>Studio</th>
                                        <th>Status</th>
										<th>Choose</th>
                                    </thead>
									<tbody id="filter-scenes">
									</tbody>
                                </table>    
                            </div>        
                        </div>
						<div class='divider'></div>
						<div class="form-row">
							<div class='form-group col-md-4'>
								<label>DVD Title</label>
								<input type="text"  class="form-control"  name='title' value="<?php echo set_value('title'); ?>"  placeholder="Title">
							</div>
							<div class='form-group col-md-4'>
								<label for="Image">Select Dvd Cover </label><br>
								<img id="output_cover_image" />
								<div class="btn btn-info btn-sm mkd-choose-image" data-image-url="cover_image" data-image-id="cover_image_id" data-image-preview="output_cover_image" data-view-width="250" data-view-height="250" data-boundary-width="500" data-boundary-height="500">Choose Image</div>
								<input type="hidden" id="cover_image" name="cover_image" value=""/>
								<input type="hidden" id="cover_image_id" name="cover_image_id" value=""/>	
							</div>		
						</div>
						<div class="form-row">
							<div class="form-group col-md-4">
							  	<label for="End Of Production">Release Date </label>
								<input type="date" class="form-control" id="form_release_date" name="release_date" value="<?php echo set_value('release_date'); ?>"/>			
							</div>
							<div class="form-group col-md-4">
								<div class="form-group">
									<label for="Is for comp">Is for comp </label>
									<select id="form_is_for_comp" name="is_for_comp" class="form-control">
										<?php foreach ($view_model->is_for_comp_mapping() as $key => $value) {
											echo "<option value='{$key}'> {$value} </option>";
										}?>
									</select>
								</div>	
							</div>		
						</div>
						<div class="form-row">
                			<div class="form-group">
                    			<input type="submit" class="btn btn-primary" value="Submit">
                			</div>
						</div>
                	</form>
           	 	</div>
        	</div>
    	</div>
	</div>
</div>
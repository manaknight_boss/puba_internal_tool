<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html lang="en" >
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="description" content="">
    <title>{{{company}}}</title>

    <script src="http://cdn.jsdelivr.net/webshim/1.12.4/extras/modernizr-custom.js"></script>
    <script src="http://cdn.jsdelivr.net/webshim/1.12.4/polyfiller.js"></script>
    <!--  CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" crossorigin="anonymous">
    <!-- YOUR CSS -->
{{{css}}}
  </head>
  <body class="page">

  <nav id="navbarMain" class="global-nav navbar navbar-expand-lg navbar-dark fixed-top">

<a class="navbar-brand" href="/">
    {{{company}}}
</a>
<button class="navbar-toggler my-2" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
  <span>&#9776;</span>
</button>
<div class="collapse navbar-collapse justify-content-end" id="navbarNav">
  <ul class="navbar-nav text-right">
    <li class="nav-item ">
      <a class="nav-link " href="/about" id="navbarDropdown">
      About
      </a>
    </li>

    <li class="dropdown-divider"></li>
    <li class="nav-item">
      <a class="nav-link " href="/pricing">Pricing</a>
    </li>
    <li class="dropdown-divider"></li>
    <li class="nav-item">
    <?php
      if ($loggedin) {
        echo '<a class="nav-link" href="/member/dashboard">My Account</a>';
      } else {
        echo '<a class="nav-link" href="/member/login">Login</a>';
      }
    ?>
    </li>
    <li class="dropdown-divider"></li>
    <li class="nav-item">
    <a class="nav-link " href="/contact">Contact</a>
    </li>
  </ul>
</div>
</nav>
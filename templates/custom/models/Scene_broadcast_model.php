<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Scene_broadcast_model Model
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Scene_broadcast_model extends Manaknight_Model
{
	protected $_table = 'scene_broadcast';
	protected $_primary_key = 'id';
	protected $_return_type = 'array';
	protected $_allowed_fields = [
    'id',
		'scene_id',
		'company_id',
		'right_id',
		'terms',
		
    ];
	protected $_label_fields = [
    'ID','Scene Id','Company Id','Right Id','Terms',
    ];
	protected $_use_timestamps = TRUE;
	protected $_created_field = 'created_at';
	protected $_updated_field = 'updated_at';
	protected $_validation_rules = [
    ['id', 'ID', ''],
		['scene_id', 'Scene Id', ''],
		['company_id', 'Company Id', ''],
		['right_id', 'Right Id', 'required|max[255]'],
		['terms', 'Terms', ''],
		
    ];
	protected $_validation_edit_rules = [
    ['id', 'ID', ''],
		['scene_id', 'Scene Id', 'required'],
		['company_id', 'Company Id', 'required'],
		['right_id', 'Right Id', 'required|max[255]'],
		['terms', 'Terms', 'required'],
		
    ];
	protected $_validation_messages = [

    ];

	public function __construct()
	{
		parent::__construct();
	}

    /**
     * If you need to modify payload before create, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _pre_create_processing($data)
    {
        
        return $data;
    }

    /**
     * If you need to modify payload before edit, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _post_edit_processing($data)
    {
        
        return $data;
    }

    /**
     * Allow user to add extra counting condition so user don't have to change main function
     *
     * @param mixed $parameters
     * @return $db
     */
    protected function _custom_counting_conditions(&$db)
    {
        
        return $db;
    }


	public function batch_insert($params)
	{
		 return $this->db->insert_batch($this->_table,$params);
	}

	public function get_by_scene($scene_id)
	{
        $sql = "SELECT scene_broadcast.company_id, scene_broadcast.scene_id, scene_broadcast.right_id, scene_broadcast.terms, broadcast_company.name FROM scene_broadcast INNER JOIN broadcast_company ON scene_broadcast.company_id = broadcast_company.id WHERE scene_broadcast.scene_id = {$scene_id}";
        return $this->db->query($sql)->result_array();
	}

    public function delete_by_scene($scene_id)
	{
		$this->db->where('scene_id',$scene_id);
		return $this->db->delete($this->_table); 
	 }
}
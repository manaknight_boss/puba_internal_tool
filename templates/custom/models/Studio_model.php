<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Studio_model Model
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Studio_model extends Manaknight_Model
{
	protected $_table = 'studio';
	protected $_primary_key = 'id';
	protected $_return_type = 'array';
	protected $_allowed_fields = [
    'id',
		'company_id',
		'name',
		'status',
		
    ];
	protected $_label_fields = [
    'ID','Company','Name','Status',
    ];
	protected $_use_timestamps = TRUE;
	protected $_created_field = 'created_at';
	protected $_updated_field = 'updated_at';
	protected $_validation_rules = [
    ['id', 'ID', ''],
		['company_id', 'Company', 'required'],
		['name', 'Name', 'required|max[255]'],
		['status', 'Status', ''],
		
    ];
	protected $_validation_edit_rules = [
    ['id', 'ID', ''],
		['company_id', 'Company', 'required'],
		['name', 'Name', 'required|max[255]'],
		['status', 'Status', 'required'],
		
    ];
	protected $_validation_messages = [

    ];

	public function __construct()
	{
		parent::__construct();
	}

    /**
     * If you need to modify payload before create, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _pre_create_processing($data)
    {
        $data['status'] = 1;

        return $data;
    }

    /**
     * If you need to modify payload before edit, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _post_edit_processing($data)
    {
        
        return $data;
    }

    /**
     * Allow user to add extra counting condition so user don't have to change main function
     *
     * @param mixed $parameters
     * @return $db
     */
    protected function _custom_counting_conditions(&$db)
    {
        
        return $db;
    }


	public function status_mapping ()
	{
		return [
			0 => 'inactive',
			1 => 'active',
		];
	}


	public function get_company_studios_list($company_id)
	{
		$this->db->where('company_id', $company_id);
		return $this->db->get($this->_table)->result_array();
	}

	public function search_studios($search_query = '')
	{
		$this->db->like('name', $search_query);
		return $this->db->get($this->_table)->result_array();
	}

	public function get_studio_company($studio_id)
	{
		$sql = 'SELECT studio.id, studio.name AS studio_name, studio.company_id, company.name, company.address, company.address_2257 FROM studio INNER JOIN company ON studio.company_id = company.id WHERE studio.id = ' .$studio_id;		
		return $this->db->query($sql)->row_array();
	}

	public function get_studios_by_company_ids($companies)
	{
		$company_ids = implode(',', $companies);
		$sql = "SELECT studio.id FROM studio WHERE studio.company_id IN ({$company_ids})";
		return $this->db->query($sql)->row_array();
	}


}
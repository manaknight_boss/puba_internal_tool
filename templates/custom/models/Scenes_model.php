<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Scenes_model Model
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Scenes_model extends Manaknight_Model
{
	protected $_table = 'scenes';
	protected $_primary_key = 'id';
	protected $_return_type = 'array';
	protected $_allowed_fields = [
    'id',
		'title',
		'type',
		'primary_producer_id',
		'group_id',
		'channel',
		'set_id',
		'xvideos_url',
		'tss_url',
		'tubeclip_url',
		'media_type',
		'studio_id',
		'is_for_comp',
		'next_studio_id',
		'thumbnail',
		'status',
		'is_comp',
		'comp_after_date',
		'date_released',
		
    ];
	protected $_label_fields = [
    'ID','title','type','Producer Id','Group Id','channel','Set Id','xvideos url','Tss url','tubeclip url','media type','Select','Is For Comp','Next Studio','Image Type','Status','Status','Comp after date','Date Released',
    ];
	protected $_use_timestamps = TRUE;
	protected $_created_field = 'created_at';
	protected $_updated_field = 'updated_at';
	protected $_validation_rules = [
    ['id', 'ID', ''],
		['title', 'title', 'required|max[255]'],
		['type', 'type', 'required|max[255]'],
		['primary_producer_id', 'Producer Id', 'required'],
		['group_id', 'Group Id', 'required'],
		['channel', 'channel', 'required|max[255]'],
		['set_id', 'Set Id', 'required|max[255]'],
		['xvideos_url', 'xvideos url', 'required'],
		['tss_url', 'Tss url', 'required'],
		['tubeclip_url', 'tubeclip url', 'required'],
		['media_type', 'media type', 'required|max[255]'],
		['studio_id', 'Select', ''],
		['is_for_comp', 'Is For Comp', ''],
		['next_studio_id', 'Next Studio', ''],
		['thumbnail', 'Image Type', ''],
		['status', 'Status', ''],
		['comp_after_date', 'Comp after date', ''],
		['date_released', 'Date Released', 'required|date'],
		
    ];
	protected $_validation_edit_rules = [
    ['id', 'ID', ''],
		['title', 'title', 'required|max[255]'],
		['type', 'type', 'required|max[255]'],
		['primary_producer_id', 'Producer Id', ''],
		['group_id', 'Group Id', ''],
		['channel', 'channel', 'required|max[255]'],
		['set_id', 'Set Id', 'required|max[255]'],
		['xvideos_url', 'xvideos url', ''],
		['tss_url', 'Tss url', ''],
		['tubeclip_url', 'tubeclip url', ''],
		['media_type', 'media type', 'required|max[255]'],
		['studio_id', 'Select', 'required'],
		['is_for_comp', 'Is For Comp', 'required'],
		['next_studio_id', 'Next Studio', 'required'],
		['thumbnail', 'Image Type', ''],
		['status', 'Status', ''],
		['comp_after_date', 'Comp after date', ''],
		['date_released', 'Date Released', 'required|date'],
		
    ];
	
	protected $_validation_messages = [

    ];

	public function __construct()
	{
		parent::__construct();
	}

    /**
     * If you need to modify payload before create, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _pre_create_processing($data)
    {
        
        return $data;
    }

    /**
     * If you need to modify payload before edit, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _post_edit_processing($data)
    {
        
        return $data;
    }

    /**
     * Allow user to add extra counting condition so user don't have to change main function
     *
     * @param mixed $parameters
     * @return $db
     */
    protected function _custom_counting_conditions(&$db)
    {
        
        return $db;
    }


	public function status_mapping ()
	{
		return [
			0 => 'inactive',
			1 => 'active',
			2 => 'pending'
		];
	}

	public function is_for_comp_mapping ()
	{
		return [
			0 => 'no',
			1 => 'yes',
		];
	}

	private function _validate_date($date, $format = 'Y-m-d')
	{
		$d = DateTime::createFromFormat($format, $date);
		return $d && $d->format($format) == $date;
	}

	public function filter_scenes($category_id = 0, $filter_after = 0)
	{
		$where = "";
		if($category_id > 0)
		{
			$where .= "WHERE scenes.id IN (SELECT scene_categories.scene_id FROM scene_categories WHERE scene_categories.category_id = {$category_id})";
		}
		if($filter_after !== 0)
		{
			$where .= "AND DATE(scenes.date_released) >= DATE(NOW() - INTERVAL $filter_after MONTH)";
		}
		$sql = "SELECT scenes.title, scenes.id, studio.name, scenes.status, scenes.thumbnail FROM scenes INNER JOIN studio ON scenes.studio_id = studio.id " . $where ;
		return $this->db->query($sql)->result_array();	
	}

	public function get_scene_studios($scene_id)
	{
		$sql = "SELECT scenes.id, scenes.title,scenes.studio_id, studio.name, studio.company_id, company.name, company.address, company.address_2257 FROM (( scenes INNER JOIN studio ON scenes.studio_id = studio.id) INNER JOIN company ON studio.company_id = company.id) WHERE scenes.id {$scene_id}";
		return $this->db->query($sql)->row_array();
	}

	public function get_unique_scene_ids()
	{
		$sql = 'SELECT DISTINCT scenes.set_id FROM scenes';
		return $this->db->query($sql)->result_array();
	}

	public function update_scene($id, $params)
	{
		$this->db->where('id',$id);
        return $this->db->update($this->_table,$params);
	}

	/**
	 * @override
	 */
	public function count($filter_array = [])
	{
		$this->db->select('scenes.id');
		$this->db->from($this->_table);
		
		if(!empty($filter_array))
		{
			if($filter_array['title'] !== '')
			{
				$this->db->or_like('scenes.title', $filter_array['title']);
			}
			
			if($filter_array['group_id'] !== 0 && $filter_array['group_id'] === '')
			{
				$this->db->or_where('scenes.group_id =', (int) $filter_array['group_id'] );
			}
			
			if($filter_array['type_id'] !== 0 && $filter_array['type_id'] === '')
			{
				$this->db->or_where('scenes.type_id =', (int) $filter_array['type_id'] );
			}
			
			if(!empty($filter_array['categories']))
			{
				$scene_cat = implode(',', $filter_array['categories']);
				$this->db->or_where("scenes.id IN (SELECT DISTINCT scene_categories.scene_id FROM scene_categories WHERE scene_categories.category_id IN ({$scene_cat}) ) ");
			}
			
			if(!empty($filter_array['not_categories']))
			{
				$scene_cat = implode(',', $filter_array['not_categories']);
				$this->db->or_where("scenes.id NOT IN (SELECT DISTINCT scene_categories.scene_id FROM scene_categories WHERE scene_categories.category_id IN ({$scene_cat}) ) ");	
			}
			
			if(in_array($filter_array['status'], [0,1,2]))
			{
				$this->db->where('scenes.status =', (int) $filter_array['status'] );	
			}
			
			if($this->_validate_date($filter_array['date_released']))
			{
				if(date("Y-m-d", strtotime( date("Y-m-d"))) > date("Y-m-d", strtotime( $filter_array['date_released'] )))
				{
					$this->db->where('date_released =', date("Y-m-d", strtotime(  $filter_array['date_released'] )));	
				}
			}
			
			if(isset($filter_array['permitted_studios']) && !empty($filter_array['permitted_studios']))
			{
				$studio_ids = implode(',', $filter_array['permitted_studios']);
				$this->db->where("scenes.studio_id IN ({$studio_ids}) ");
			}
		}
		$query = $this->db->get();
		return $query->num_rows();	
	}

	/**
	 * @override
	 */
	public function get_paginated($offset = 0, $per_page = 5, $filter_array = [])
	{
		$this->db->select('scenes.thumbnail, scenes.status, scenes.comp_after_date,  scenes.id, scenes.title, (SELECT studio.name FROM studio WHERE studio.id = scenes.studio_id) AS current_studio_name, (SELECT studio.name FROM studio WHERE studio.id = scenes.next_studio_id) AS next_studio_name ');
		$this->db->from($this->_table);
		if(!empty($filter_array))
		{
			if($filter_array['title'] !== '')
			{
				$this->db->or_like('scenes.title', $filter_array['title']);
			}
			
			if($filter_array['group_id'] !== 0 && $filter_array['group_id'] === '')
			{
				$this->db->or_where('scenes.group_id =', (int) $filter_array['group_id'] );
			}
			
			if($filter_array['type_id'] !== 0 && $filter_array['type_id'] === '')
			{
				$this->db->or_where('scenes.type_id =', (int) $filter_array['type_id'] );
			}
			
			if(!empty($filter_array['categories']))
			{
				$scene_cat = implode(',', $filter_array['categories']);
				$this->db->or_where("scenes.id IN (SELECT DISTINCT scene_categories.scene_id FROM scene_categories WHERE scene_categories.category_id IN ({$scene_cat}) ) ");
			}
			
			if(!empty($filter_array['not_categories']))
			{
				$scene_cat = implode(',', $filter_array['not_categories']);
				$this->db->or_where("scenes.id NOT IN (SELECT DISTINCT scene_categories.scene_id FROM scene_categories WHERE scene_categories.category_id IN ({$scene_cat}) ) ");	
			}
			
			if(in_array($filter_array['status'], [0,1,2]))
			{
				$this->db->where('scenes.status =', (int) $filter_array['status'] );	
			}
			
			if($this->_validate_date($filter_array['date_released']))
			{
				if(date("Y-m-d", strtotime( date("Y-m-d"))) > date("Y-m-d", strtotime( $filter_array['date_released'] )))
				{
					$this->db->where('date_released =', date("Y-m-d", strtotime(  $filter_array['date_released'] )));	
				}
			}

		    if(isset($filter_array['permitted_studios']) && !empty($filter_array['permitted_studios']))
			{
				$studio_ids = implode(',', $filter_array['permitted_studios']);
				$this->db->where("scenes.studio_id IN ({$studio_ids}) ");
			}
			
		}
		$this->db->limit($per_page, $offset);
		return $this->db->get()->result();
	}
}
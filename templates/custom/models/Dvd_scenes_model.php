<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Dvd_scenes_model Model
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Dvd_scenes_model extends Manaknight_Model
{
	protected $_table = 'dvd_scenes';
	protected $_primary_key = 'id';
	protected $_return_type = 'array';
	protected $_allowed_fields = [
    'id',
		'dvd_id',
		'scene_id',
		
    ];
	protected $_label_fields = [
    'ID','Actor Id','Scene Id',
    ];
	protected $_use_timestamps = TRUE;
	protected $_created_field = 'created_at';
	protected $_updated_field = 'updated_at';
	protected $_validation_rules = [
    ['id', 'ID', ''],
		['dvd_id', 'Actor Id', ''],
		['scene_id', 'Scene Id', ''],
		
    ];
	protected $_validation_edit_rules = [
    ['id', 'ID', ''],
		['dvd_id', 'Actor Id', 'required'],
		['scene_id', 'Scene Id', 'required'],
		
    ];
	protected $_validation_messages = [

    ];

	public function __construct()
	{
		parent::__construct();
	}

    /**
     * If you need to modify payload before create, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _pre_create_processing($data)
    {
        
        return $data;
    }

    /**
     * If you need to modify payload before edit, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _post_edit_processing($data)
    {
        
        return $data;
    }

    /**
     * Allow user to add extra counting condition so user don't have to change main function
     *
     * @param mixed $parameters
     * @return $db
     */
    protected function _custom_counting_conditions(&$db)
    {
        
        return $db;
    }


	public function batch_insert($params)
	{
		 return $this->db->insert_batch($this->_table,$params);
	}

    public function get_dvd_scenes($dvd_id)
    {
        $sql = "SELECT dvd_scenes.id,dvd_scenes.is_cover_scene, dvd_scenes.scene_id, dvd_scenes.dvd_id,scenes.set_id,scenes.title,scenes.studio_id, scenes.next_studio_id, scenes.date_released, studio.name, scenes.status FROM ((dvd_scenes INNER JOIN scenes ON dvd_scenes.scene_id = scenes.id) INNER JOIN studio ON scenes.studio_id = studio.id) WHERE dvd_scenes.dvd_id = {$dvd_id} ORDER BY dvd_scenes.id ASC";
        return $this->db->query($sql)->result();
    }

    public function delete_dvd_scene($dvd_scene_id)
    {
        return $this->real_delete($dvd_scene_id);
    }

    public function delete_all_dvd_scenes($dvd_id)
    {
        $this->db->where('dvd_id', $dvd_id);
        return $this->db->delete($this->_table);
    }

    public function get_dvd_cover_scene($dvd_id)
    {
        $this->db->where(['dvd_id' => $dvd_id, 'is_cover_scene' => 1]);
        return $this->db->get($this->_table)->row_array();
    }

}
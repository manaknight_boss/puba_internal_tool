<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Dvds_model Model
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Dvds_model extends Manaknight_Model
{
	protected $_table = 'dvds';
	protected $_primary_key = 'id';
	protected $_return_type = 'array';
	protected $_allowed_fields = [
    'id',
		'title',
		'company_id',
		'studio_id',
		'type_id',
		'filter_after',
		'start_production_date',
		'end_production_date',
		'cover_image',
		'status',
		'is_for_comp',
		'release_date',
		
    ];
	protected $_label_fields = [
    'ID','Title','Company','Studio','DVD Type','Filter After','Start Of Production','End Of Production','Image','Status','Is for comp','Release date',
    ];
	protected $_use_timestamps = TRUE;
	protected $_created_field = 'created_at';
	protected $_updated_field = 'updated_at';
	protected $_validation_rules = [
    ['id', 'ID', ''],
		['title', 'Title', 'required|max[255]'],
		['company_id', 'Company', ''],
		['studio_id', 'Studio', ''],
		['type_id', 'DVD Type', ''],
		['filter_after', 'Filter After', ''],
		['start_production_date', 'Start Of Production', 'required|date'],
		['end_production_date', 'End Of Production', ''],
		['cover_image', 'Image', ''],
		['status', 'Status', ''],
		['is_for_comp', 'Is for comp', ''],
		['release_date', 'Release date', ''],
		
    ];
	protected $_validation_edit_rules = [
    ['id', 'ID', ''],
		['title', 'Title', 'required|max[255]'],
		['company_id', 'Company', 'required'],
		['studio_id', 'Studio', 'required'],
		['type_id', 'DVD Type', 'required'],
		['filter_after', 'Filter After', 'required'],
		['start_production_date', 'Start Of Production', 'required|date'],
		['end_production_date', 'End Of Production', ''],
		['cover_image', 'Image', ''],
		['status', 'Status', 'required'],
		['is_for_comp', 'Is for comp', ''],
		['release_date', 'Release date', 'required|date'],
		
    ];
	protected $_validation_messages = [

    ];

	public function __construct()
	{
		parent::__construct();
	}

    /**
     * If you need to modify payload before create, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _pre_create_processing($data)
    {
        
        return $data;
    }

    /**
     * If you need to modify payload before edit, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _post_edit_processing($data)
    {
        
        return $data;
    }

    /**
     * Allow user to add extra counting condition so user don't have to change main function
     *
     * @param mixed $parameters
     * @return $db
     */
    protected function _custom_counting_conditions(&$db)
    {
        
        return $db;
    }


	public function status_mapping ()
	{
		return [
			0 => 'inactive',
			1 => 'active',
		];
	}

	public function is_for_comp_mapping ()
	{
		return [
			0 => 'no',
			1 => 'yes',
		];
	}

	public function get_dvd_details($id)
	{
		$sql = "SELECT dvds.title, dvds.id, company.name, company.address_2257,company.address FROM dvds INNER JOIN company ON dvds.company_id = company.id WHERE  dvds.id = {$id}";
		return $this->db->query($sql)->row_array();
	}

	/**
	 * @override
	 */
	public function count($filter_array = [])
	{
		$this->db->select('dvds.id');
		$this->db->from($this->_table);
		if(!empty($filter_array))
		{
			if($filter_array['title'] !== '')
			{
				$this->db->or_like('dvds.title', $filter_array['title']);
			}

			if($filter_array['company_id'] !== 0 && $filter_array['company_id'] !== '')
			{
				$this->db->or_where('dvds.company_id =', $filter_array['company_id'] );
				//$this->db->escape_str($title);
			}

			if($filter_array['studio_id'] !== 0 && $filter_array['studio_id'] !== '')
			{
				$this->db->or_where('dvds.studio_id =', (int) $filter_array['studio_id'] );
			}

			if(in_array($filter_array['status'], [0,1]))
			{
				$this->db->or_where('dvds.status =', (int) $filter_array['status'] );	
			}

			if(!empty($filter_array['set_id']))
			{
				$set_ids = "'" . implode("','",$filter_array['set_id']) . "'";
				$this->db->or_where("dvds.id IN (SELECT dvd_scenes.dvd_id FROM dvd_scenes INNER JOIN scenes ON dvd_scenes.scene_id = scenes.id WHERE scenes.set_id IN ( $set_ids )) ");
			}

			if(!empty($filter_array['release_start_date']) && !empty($filter_array['release_end_date']) )
			{
				 $start_date = date("Y-m-d", strtotime($filter_array['release_start_date']));
				 $end_date = date("Y-m-d", strtotime($filter_array['release_end_date']));
				
				 if( $end_date > $start_date)
				 {
					
					$this->db->where('dvds.release_date >=', $start_date);
					$this->db->where('dvds.release_date <=', $end_date);
				 }	
			}
			
			if($filter_array['id'] !== 0 && $filter_array['id'] !== '')
			{
				$this->db->where('dvds.id =', $filter_array['id']);
			}
			
			if(isset($filter_array['permitted_studios']) && !empty($filter_array['permitted_studios']))
			{
				$studio_ids = implode(',', $filter_array['permitted_studios']);
				$this->db->where("dvds.studio_id IN ({$studio_ids}) ");
			}
		}
		
		$query = $this->db->get();
		return $query->num_rows();	
	}

	/**
	 * @override
	 */
	public function get_paginated($offset = 0, $per_page = 5, $filter_array = [])
	{	
		$this->db->select('dvds.id, dvds.cover_image, dvds.title, dvds.status, dvds.release_date, dvds.is_for_comp, studio.name AS studio_name');
		$this->db->from($this->_table);
		$this->db->join('studio','dvds.studio_id = studio.id','inner');
		
		if(!empty($filter_array))
		{
			
			if($filter_array['id'] !== '' && $filter_array['id'] !== 0)
			{
				$this->db->where('dvds.id =', $filter_array['id']);
			}
			
			if($filter_array['title'] !== '')
			{
				$this->db->or_like('dvds.title', $filter_array['title']);
			}
			
			if($filter_array['company_id'] !== 0 && $filter_array['company_id'] !== '')
			{
				$this->db->or_where('dvds.company_id =', $filter_array['company_id'] );
				//$this->db->escape_str($title);
			}
			
			if($filter_array['studio_id'] !== 0 && $filter_array['studio_id'] !== '')
			{
				$this->db->or_where('dvds.studio_id =', (int) $filter_array['studio_id'] );
			}
			
			if(in_array($filter_array['status'], [0,1]))
			{
				$this->db->or_where('dvds.status =', (int) $filter_array['status'] );	
			}
			
			if(!empty($filter_array['release_start_date']) && !empty($filter_array['release_end_date']) )
			{
				$start_date = date("Y-m-d", strtotime($filter_array['release_start_date']));
				 $end_date = date("Y-m-d", strtotime($filter_array['release_end_date']));
				
				 if($end_date > $start_date)
				{
					
					$this->db->where('dvds.release_date >=', $start_date);
					$this->db->where('dvds.release_date <=', $end_date);
				}	
			}
			
			if(!empty($filter_array['set_id']))
			{
				$set_ids = "'" . implode("','",$filter_array['set_id']) . "'";
				$this->db->or_where("dvds.id IN (SELECT dvd_scenes.dvd_id FROM dvd_scenes INNER JOIN scenes ON dvd_scenes.scene_id = scenes.id WHERE scenes.set_id IN ( $set_ids )) ");
			}

			if(isset($filter_array['permitted_studios']) && !empty($filter_array['permitted_studios']))
			{
				$studio_ids = implode(',', $filter_array['permitted_studios']);
				$this->db->where("dvds.studio_id IN ({$studio_ids}) ");
			}
			
			if($filter_array['sort'] !== '')
			{
				switch($filter_array['sort'])
				{
					case 'a-z':
						$this->db->order_by('dvds.title', 'ASC');
					break;
					case 'z-a':
						$this->db->order_by('dvds.title', 'DESC');
					break;
					case 'old-new':
						$this->db->order_by('dvds.id', 'ASC');
					break;
					default:
						$this->db->order_by('dvds.id', 'DESC');
					break;
				}
			}
		}
		$this->db->limit($per_page, $offset);
		return $this->db->get()->result();
	}
}
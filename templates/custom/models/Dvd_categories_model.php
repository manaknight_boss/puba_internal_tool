<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Dvd_categories_model Model
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Dvd_categories_model extends Manaknight_Model
{
	protected $_table = 'dvd_categories';
	protected $_primary_key = 'id';
	protected $_return_type = 'array';
	protected $_allowed_fields = [
    'id',
		'dvd_id',
		'category_id',
		
    ];
	protected $_label_fields = [
    'ID','Actor Id','Scene Id',
    ];
	protected $_use_timestamps = TRUE;
	protected $_created_field = 'created_at';
	protected $_updated_field = 'updated_at';
	protected $_validation_rules = [
    ['id', 'ID', ''],
		['dvd_id', 'Actor Id', ''],
		['category_id', 'Scene Id', ''],
		
    ];
	protected $_validation_edit_rules = [
    ['id', 'ID', ''],
		['dvd_id', 'Actor Id', 'required'],
		['category_id', 'Scene Id', 'required'],
		
    ];
	protected $_validation_messages = [

    ];

	public function __construct()
	{
		parent::__construct();
	}

    /**
     * If you need to modify payload before create, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _pre_create_processing($data)
    {
        
        return $data;
    }

    /**
     * If you need to modify payload before edit, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _post_edit_processing($data)
    {
        
        return $data;
    }

    /**
     * Allow user to add extra counting condition so user don't have to change main function
     *
     * @param mixed $parameters
     * @return $db
     */
    protected function _custom_counting_conditions(&$db)
    {
        
        return $db;
    }


	public function batch_insert($params)
	{
		 return $this->db->insert_batch($this->_table,$params);
	}

    public function get_by_dvd_id($id)
    {
        $this->db->select('category_id');
        $this->db->where('dvd_id',$id);
        $query =  $this->db->get($this->_table); 
        return $query->result_array();   
    }

    public function delete_all_dvd_categories($dvd_id)
    {
        $this->db->where('dvd_id', $dvd_id);
        return $this->db->delete($this->_table);
    }
}
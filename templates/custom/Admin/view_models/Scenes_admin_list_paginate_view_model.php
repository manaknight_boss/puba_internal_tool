<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Scenes List Paginate View Model
 *
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 */
class Scenes_admin_list_paginate_view_model
{
    protected $_library;
    protected $_base_url = '';
    protected $_heading = 'Scenes';
    protected $_total_rows = 0;
    protected $_per_page = 10;
    protected $_page;
    protected $_num_links = 0;
    protected $_column = ['Thumbnail','Name','Studio ','Status','Action'];
    protected $_list = [];
    protected $_categories;
    protected $_date_released;
    protected $_not_categories;
    protected $_status;
    protected $_group;
    protected $_type;
    protected $_links = '';
    protected $_entity;

    public function __construct($entity, $library, $base_url)
    {
        $this->_entity = $entity;
        $this->_library = $library;
		$this->_base_url = str_replace('/0', '/', $base_url);
    }

    /**
     * set_total_rows function
     *
     * @param integer $total_rows
     * @return void
     */
    public function set_total_rows ($total_rows)
    {
        $this->_total_rows = $total_rows;
    }

    /**
     * set_per_page function
     *
     * @param integer $per_page
     * @return void
     */
    public function set_per_page ($per_page)
    {
        $this->_per_page = $per_page;
    }

    /**
     * set_page function
     *
     * @param integer $page
     * @return void
     */
    public function set_page ($page)
    {
        $this->_page = $page;
    }

    /**
     * set_list function
     *
     * @param mixed $list
     * @return void
     */
    public function set_list ($list)
    {
        $this->_list = $list;
        $this->_num_links = round($this->_total_rows / $this->_per_page);
    }

    /**
     * get_total_rows function
     *
     * @return integer
     */
    public function get_total_rows ()
    {
        return $this->_total_rows;
    }

    /**
     * get_per_page function
     *
     * @return integer
     */
    public function get_per_page ()
    {
        return $this->_per_page;
    }

    /**
     * get_page function
     *
     * @return integer
     */
    public function get_page ()
    {
        return $this->_page;
    }

    /**
     * num_links function
     *
     * @return integer
     */
    public function get_num_links ()
    {
        return $this->_num_links;
    }

    /**
     * num_pages function
     *
     * @return integer
     */
    public function get_num_page ()
    {
        $num = ceil($this->_total_rows / $this->_per_page);
        return ($num > 0) ? (int) $num : 1;
    }
   
    public function image_or_file ($file)
    {
        $images = ['.jpg','.png', '.gif', '.jpeg', '.bmp'];
        $is_image = FALSE;
        if ($this->strposa($file, $images))
        {
            return "<div class='mkd-image-container'><img class='img-fluid' src='{$file}'/></div>";
        }

        return "<a href='{$file}' target='__blank'>{$file}</a>";
    }

    /**
     * Strpos for array
     *
     * @param string $haystack
     * @param array $needle
     * @return boolean
     */
    private function strposa($haystack, $needle)
    {
        foreach($needle as $query)
        {
            if(strpos($haystack, $query) !== FALSE)
            {
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * get_list function
     *
     * @return mixed
     */
    public function get_list ()
    {
        $this->_library->initialize([
            'reuse_query_string' => TRUE,
            'base_url' => $this->_base_url,
            'total_rows' => $this->_total_rows,
            'per_page' => $this->_per_page,
            'full_tag_open' => '<ul class="pagination justify-content-end">',
            'full_tag_close' => '</ul>',
            'attributes' => ['class' => 'page-link'],
            'first_link' => FALSE,
            'last_link' => FALSE,
            'first_tag_open' => '<li class="page-item">',
            'first_tag_close' => '</li>',
            'prev_link' => '&laquo',
            'prev_tag_open' => '<li class="page-item">',
            'prev_tag_close' => '</li>',
            'next_link' => '&raquo',
            'next_tag_open' => '<li class="page-item">',
            'next_tag_close' => '</li>',
            'last_tag_open' => '<li class="page-item">',
            'last_tag_close' => '</li>',
            'cur_tag_open' => '<li class="page-item active"><a href="#" class="page-link">',
            'cur_tag_close' => '<span class="sr-only">(current)</span></a></li>',
            'num_tag_open' => '<li class="page-item">',
            'num_tag_close' => '</li>'
        ]);
        return $this->_list;
    }

    /**
     * get_links function
     *
     * @return mixed
     */
    public function get_links ()
    {
        $this->_links = $this->_library->create_links();
        return $this->_links;
    }

    /**
     * set_heading function
     *
     * @param string $heading
     * @return void
     */
    public function set_heading ($heading)
    {
        $this->_heading = $heading;
    }

    /**
     * get_heading function
     *
     * @return string
     */
    public function get_heading ()
    {
        return $this->_heading;
    }

    public function set_column ($column)
    {
        $this->_column = $column;
    }

    public function get_column ()
    {
        return $this->_column;
    }

	public function get_title ()
	{
		return $this->_title;
	}

	public function set_title ($title)
	{
		$this->_title = $title;
	}

    public function set_categories($categories)
    {
        $this->_categories = $categories;
    }

    public function get_categories()
    {
        return $this->_categories;
    }

    public function set_not_categories($not_categories)
    {
        $this->_not_categories = $not_categories;
    }

    public function get_not_categories()
    {
        return $this->_not_categories;
    }

    public function set_status($status)
    {
        $this->_status = $status;
    }

    public function get_status()
    {
        return  $this->_status;
    }

    public function set_group($group)
    {
        $this->_group = $group;
    }

    public function get_group()
    {
        return $this->_group;
    }

	public function get_thumbnail ()
	{
		return $this->_thumbnail;
	}

	public function set_thumbnail ($thumbnail)
	{
		$this->_thumbnail = $thumbnail;
	}

	public function get_group_id ()
	{
		return $this->_group_id;
	}

	public function set_group_id ($group_id)
	{
		$this->_group_id = $group_id;
	}

	public function get_last_name ()
	{
		return $this->_last_name;
	}

	public function set_last_name ($last_name)
	{
		$this->_last_name = $last_name;
    }
    
    public function set_date_released($date_released)
    {
        $this->_date_released = $date_released;
    }

    public function get_date_released()
    {
        return $this->_date_released;
    }

	public function get_type ()
	{
		return $this->_type;
	}

	public function set_type ($type)
	{
		$this->_type = $type;
	}

	public function get_id ()
	{
		return $this->_id;
	}

	public function set_id ($id)
	{
		$this->_id = $id;
	}

	public function status_mapping ()
	{
		return $this->_entity->status_mapping();

	}

	public function is_for_comp_mapping ()
	{
		return $this->_entity->is_for_comp_mapping();

	}

	public function to_json ()
	{
		$list = $this->get_list();

		$clean_list = [];

		foreach ($list as $key => $value)
		{
			$list[$key]->status = $this->status_mapping()[$value->status];
			$list[$key]->is_for_comp = $this->is_for_comp_mapping()[$value->is_for_comp];
			$clean_list_entry = [];
			$clean_list[] = $clean_list_entry;
		}

		return [
			'page' => $this->get_page(),
			'num_page' => $this->get_num_page(),
			'num_item' => $this->get_total_rows(),
			'item' => $clean_list
		];
	}

}
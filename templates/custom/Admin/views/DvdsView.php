<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
?>
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="page-header" id="top">
			<h2 class="pageheader-title"><?php echo $view_model->get_heading();?> </h2>
			<div class="page-breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/admin/dashboard" class="breadcrumb-link">Dashboard</a></li>
						<li class="breadcrumb-item"><a href="/admin/dvds/0" class="breadcrumb-link"><?php echo $view_model->get_heading();?></a></li>
						<li class="breadcrumb-item active" aria-current="page">View</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header"><?php echo $view_model->get_heading();?> Details</h5>
                <div class="card-body">
				   <?= form_open('admin/dvds/update_dvd/'.$this->_data['view_model']->get_id()) ?>
						<?php if(isset($_GET['error'])):?>
							<?php if($_GET['error'] == 'false'): ?>
								<div class="alert alert-success" role="alert">
  									Dvd details updated
								</div>
							<?php else:?>	
								<div class="alert alert-danger" role="alert">
  									Error updating please provide title and a valid release date
								</div>
							<?php endif;?>
						<?php endif;?>
						<div class="row">
							<div class='form-group col-md-4'>
								<label>DVD Title</label>
								<input type="text"  class="form-control"  name='title' value="<?php echo set_value('title', $this->_data['view_model']->get_title());?>"  placeholder="Title">
							</div>
						</div>	
						<div class="row">
							<div class="form-group col-md-4">
								<label for="End Of Production">Release Date </label>
								<input type="date" class="form-control" id="form_release_date" name="release_date" value="<?php echo set_value('start_production_date', $this->_data['view_model']->get_start_production_date());?>"/>			
							</div>
							<div class="form-group col-md-4">
								<div class="form-group">
									<label for="Is for comp">Is for comp </label>
									<select id="form_is_for_comp" name="is_for_comp" class="form-control">
										<?php foreach ($view_model->is_for_comp_mapping() as $key => $value) {
											echo "<option value='{$key}' " . (($view_model->get_is_for_comp() == $key && $view_model->get_is_for_comp() != '') ? 'selected' : '') . "> {$value} </option>";
										}?>
									</select>	
								</div>		
							</div>
						</div>
						<div class="row">
							
							<div class='form-group col-md-4'>
								<div class="form-group"><br>
									<input type='hidden' name='dvd_cover_scene' id='dvd_cover_scene' value='<?php echo $this->_data[0]["view_data"]['dvd_cover_scene']['scene_id']; ?>'/>
									<input type='hidden' name='updated_scenes' id='updated_scenes' value=''/>
									<input type='submit' name='btn_update_dvd' value='update' class='btn btn-primary'>
								</div>			
							</div>
						</div>
						<hr></hr>
						<table class='table'>
							<thead>
								<th>Scene</th>
								<th>Studio Name</th>
								<th>Status</th>
								<th>Order</th>
								<th>Comp Date</th>
								<th>Next Action After Comp</th>
								<th>Cover</th>
							</thead>
							<tbody>
								<?php $index = 1; ?>
								<?php foreach($this->_data[0]["view_data"]['dvd_scenes']  as $scene):?>
									<tr>
										<td><?php echo $scene->title; ?></td>
										<td><?php echo $scene->name; ?></td>
										<td><?php echo ($scene->status === 0 ? 'inactive' : 'active') ; ?></td>
										<td><?php echo  $index ++ ?></td>
										<td><input type="date" id='scene-comp-date<?php echo  $scene->scene_id; ?>' data-id='<?php echo $scene->scene_id; ?>' class="form-control  scene-edited"  name="end_production_date" value="<?php echo set_value('end_production_date', $this->_data['view_model']->get_end_production_date());?>"/></td>
										<td>
											<select data-id='<?php echo $scene->scene_id; ?>' id='scene-studio<?php echo  $scene->scene_id; ?>' class='form-control scene-edited'>
												<?php foreach($this->_data[0]["view_data"]['studios'] as $studio):?>
													<option <?php if($studio->id === $scene->next_studio_id ) { echo "selected";} ?> value='<?php echo $studio->id ?>'><?php echo $studio->name ?></option>
												<?php endforeach;?>
											</select>
										</td>
										<td>
											<input class="float-right form-check-input scene-as-cover" type="checkbox" value="" data-id='<?= $scene->scene_id ?>'  <?php if($scene->is_cover_scene == 1){echo 'checked';}?>>
										</td>
									</tr>
								<?php endforeach;?>
							</tbody>
						</table>
					</div>
				</form>
			</div>
   	 	</div>
	</div>
</div>
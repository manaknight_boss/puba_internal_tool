<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
?>
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="page-header">
			<h2 class="pageheader-title"><?php echo $view_model->get_heading();?> </h2>
			<div class="page-breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/admin/dashboard" class="breadcrumb-link">Dashboard</a></li>
						<li class="breadcrumb-item"><a href="/admin/dvd/0" class="breadcrumb-link"><?php echo $view_model->get_heading();?></a></li>
						<li class="breadcrumb-item active" aria-current="page">Add</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</div>
<div class="row">
    <?php if (validation_errors()) : ?>
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?= validation_errors() ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (strlen($error) > 0) : ?>
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (strlen($success) > 0) : ?>
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    <?php endif; ?>
</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header"><?php echo $view_model->get_heading();?></h5>
                <div class="card-body">
					<?= form_open() ?>
						<div class='form-row'>
							<div class="form-group col-md-4">
								<label>Company</label>		
								<select name='company_id'value="<?php echo set_value('company_id', $this->_data['view_model']->get_company_id());?>"  class="form-control">
                    				<?php foreach($this->_data['view_data']['companies'] as $company ): ?>
                    					<option value="<?php echo $company->id; ?>"><?php echo $company->name; ?></option>
                    				<?php endforeach;?>
                				</select>		
							</div>
							<div class="form-group col-md-4">		
								<label for="Start Of Production">Start Of Production </label>
								<input type="date" class="form-control" id="form_start_production_date" name="start_production_date" value="<?php echo set_value('start_production_date', $this->_data['view_model']->get_start_production_date());?>"/>
							</div>	
							<div class="form-group col-md-4">
								<label for="End Of Production">End Of Production </label>
                    			<input type="date" class="form-control" id="form_end_production_date" name="end_production_date" value="<?php echo set_value('end_production_date', $this->_data['view_model']->get_end_production_date());?>"/>
							</div>			
						</div>
						<div class="form-row">
							<div class="form-group col-md-4">
								<label for="Studio">Studio </label>
								<select name='studio_id' value="<?php echo set_value('studio_id', $this->_data['view_model']->get_studio_id());?>" class="form-control">
                    				<?php foreach($this->_data['view_data']['studios'] as $studio ): ?>
                    					<option value="<?php echo $studio->id; ?>"  <?php if($studio->id === $this->_data['view_model']->get_studio_id()){ echo 'selected'; }?>><?php echo $studio->name; ?></option>
                    				<?php endforeach;?>
                				</select>
							</div>
							<div class="form-group col-md-4">
								<label for="DVD Type">DVD Type </label>
								<select name='type_id' value="<?php echo set_value('type_id', $this->_data['view_model']->get_type_id());?>" class="form-control">
                    				<?php foreach($this->_data['view_data']['types'] as $type ): ?>
                    					<option value="<?php echo $type->id; ?>" <?php if($type->id === $this->_data['view_model']->get_type_id() ){ echo 'selected'; } ?> ><?php echo $type->name; ?></option>
                    				<?php endforeach;?>
                				</select>
							</div>
							<div class='form-group col-md-4'>
								<label for="Status">Status </label>
								<select id="form_status" name="status" class="form-control">
                        			<?php foreach ($view_model->status_mapping() as $key => $value) {
                            			echo "<option value='{$key}' " . (($view_model->get_status() == $key && $view_model->get_status() != '') ? 'selected' : '') . "> {$value} </option>";
                        			}?>
                    			</select>
							</div>			
						</div>
						<div class="form-row">
							<div class="form-group col-md-6">
            				    <label >Categories</label> 
								<select class='multiple-select-control' name='dvd_categories[]' id="example-filter-placeholder" multiple="multiple">
									<?php foreach($this->_data['view_data']['categories'] as $cat ): ?>
                    					<?php if(is_numeric(array_search($cat['id'],array_column($this->_data['view_data']['dvd_category'], 'category_id') ))):?>
											<option  selected value="<?php echo $cat['id']; ?>"><?php echo $cat['name']; ?></option>
										<?php else:?>
											<option value="<?php echo $cat['id']; ?>"><?php echo $cat['name']; ?></option>
										<?php endif;?>
                    				<?php endforeach;?>
								</select>
							</div>
							<div class="form-group col-md-6">		
							</div>			
						</div>
						<div class="row">
							<div style='width:100%;' id="scenes-holder" class="card p-4">
							 	<input type='hidden'  name='dvd_scenes' id="dvd_scenes" value="<?php echo implode(',', array_column((array) $this->_data['view_data']['dvd_scenes'], 'scene_id')); ?>"/>
								<input type='hidden' name='dvd_cover_scene' id='dvd_cover_scene' value='<?php echo $this->_data['view_data']['dvd_cover_scene']['scene_id']; ?>'/>
								<strong>Selected Scenes</strong>
								<table class='table'>
									<thead>
										<th>Scene</th>
										<th>Studio</th>
										<th>Status</th>
										<th>Choose</th>
										<th>Remove</th>
									</thead>
									<tbody id='selected-dvd-scenes' data-action='edit' data-id='<?php echo $this->_data['view_model']->get_id(); ?>'>
										<?php foreach($this->_data['view_data']['dvd_scenes'] as $scene):?>
											<tr>
												<td><?php echo $scene->title; ?></td>
												<td><?php echo $scene->name; ?></td>
												<td><?php echo ($scene->status === 0 ? 'inactive' : 'active'); ?></td>
												<td>
												<input class="form-check-input scene-as-cover" type="checkbox" value="" data-id='<?php echo $scene->scene_id; ?>'  <?php if($scene->is_cover_scene == 1){echo 'checked';}?>>
  													<label class="form-check-label" for="defaultCheck1">
    													Make cover
 	 												</label>
												</td>
												<td><a href='#' class='btn btn-primary btn-remove-scene' data-scene-id='<?php echo $scene->scene_id;  ?>'  data-id='<?php echo $scene->id ?>' >remove</a></td>
											</tr>
										<?php endforeach;?>	
									</tbody>	
								</table>	
							</div>		
						</div>
						<div class="row pd-1">
							<div class="col col-md-12">
								<strong>Filter Scenes</strong>			
							</div>				
						</div>
						<div class='form-row'>
                            <div class="col col-md-12 pd-1">
								<?php foreach($this->_data['view_data']['categories'] as $cat):?>
									<a style='padding:1%;'href="#" class='filter-scene-by-category btn-link' data-id="<?php echo $cat['id']; ?>" >
										<?php echo $cat['name']; ?>(<?php echo $cat['items']; ?>)
									</a>
								<?php endforeach;?>	
							</div>
							<hr></hr>
						</div>
						<div class="form-row">
							<div class='col col-md-12'>
                                <table class='table'>
                                    <thead class='thead-dark'>
                                        <th>Scene</th>
                                        <th>Studio</th>
                                        <th>Status</th>
										<th>Choose</th>
                                    </thead>
									<tbody id="filter-scenes">
									</tbody>
                                </table>    
                            </div>        
                        </div>
						<div class="divider"></div>
						<div class="form-row">
							<div class='form-group col-md-4'>
								<label>DVD Title</label>
								<input type="text"  class="form-control"  name='title' value="<?php echo set_value('title', $this->_data['view_model']->get_title());?>"  placeholder="Title">
							</div>
							<div class='form-group col-md-4'>
								<label for="Image">Select Dvd Cover </label><br>
								<img id="output_cover_image" />
								<div class="btn btn-info btn-sm mkd-choose-image" data-image-url="cover_image" data-image-id="cover_image_id" data-image-preview="output_cover_image" data-view-width="250" data-view-height="250" data-boundary-width="500" data-boundary-height="500">Choose Image</div>
								<input type="hidden" id="cover_image" name="cover_image" value=""/>
								<input type="hidden" id="cover_image_id" name="cover_image_id" value=""/>	
							</div>		
						</div>
						<div class="form-row">
							<div class="form-group col-md-4">
							  	<label for="End Of Production">Release Date </label>
								<input type="date" class="form-control" id="form_release_date" name="release_date" value="<?php echo set_value('start_production_date', $this->_data['view_model']->get_start_production_date());?>"/>			
							</div>
							<div class="form-group col-md-4">
								<div class="form-group">
									<label for="Is for comp">Is for comp </label>
                    				<select id="form_is_for_comp" name="is_for_comp" class="form-control">
										<?php foreach ($view_model->is_for_comp_mapping() as $key => $value) {
											echo "<option value='{$key}' " . (($view_model->get_is_for_comp() == $key && $view_model->get_is_for_comp() != '') ? 'selected' : '') . "> {$value} </option>";
										}?>
									</select>
								</div>	
							</div>		
						</div>
						<div class="form-row">
                			<div class="form-group">
								<input type='hidden' name='filter_after' value="<?php echo set_value('filter_after', $this->_data['view_model']->get_filter_after());?>" >		
                    			<input type="submit" class="btn btn-primary" value="Submit">
                			</div>
						</div>
                	</form>
					<div class="row">
						<div class="col col-md-12 card pd-3">
							<div class="btn-group" role="group" aria-label="Basic example">
  								<a href="<?= base_url('admin/dvds/download_compliance_statement/'. $this->_data['view_model']->get_id())  ?>" class="btn btn-secondary m-1">Compliance Statement</a>
								<a href="<?= base_url('admin/dvds/download_cast/'. $this->_data['view_model']->get_id()) ?>"   class="btn btn-secondary m-1">Download cast</a>
								<a href="<?= base_url('admin/dvds/download_music_release/' . $this->_data['view_model']->get_id()) ?>" class="btn btn-secondary m-1">Music release</a> 
  							</div>			
						</div>				
					</div>
           	 	</div>
        	</div>
    	</div>
	</div>
</div>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
?>
<h2><?php echo $view_model->get_heading();?></h2>
<br>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header"><?php echo $view_model->get_heading();?> Search</h5>
            <div class="card-body">
                <?= form_open('', ['method' => 'get']) ?>
                    <div class='form-row'>
                        <div class="form-group col-md-4">
                            <label for="Title">Name </label>
							<input type="text" class="form-control" id="title" name="title"  value='<?php echo $view_model->get_title(); ?>'/>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="Title">ID </label>
							<input type="text" value='<?php echo $view_model->get_id(); ?>' class="form-control" id="id" name="id" />
                        </div>
                        <div class="form-group col-md-4">
                            <label for="Title">Set Id</label>
                            <select class='multiple-select-control' value='<?php  echo $view_model->get_set_id(); ?>' name='set_ids[]' id="example-filter-placeholder" multiple="multiple">
                                <?php foreach($this->_data["view_data"]['set_ids'] as $set_id ): ?>
                    				<option <?php if(in_array($set_id['set_id'],  $view_model->get_set_id())){ echo "selected"; } ?> value="<?php echo $set_id['set_id']; ?>"><?php echo $set_id['set_id']; ?></option>
                    			<?php endforeach;?>
							</select>
                        </div>   
                    </div>
                    <div class='form-row'>
                        <div class="form-group col-md-4">
                            <label for="Title">Status </label>
                            <select name="status" class="form-control"  value='<?php echo $view_model->get_status(); ?>'>
								<option value="">All</option>
								<?php foreach ($view_model->status_mapping() as $key => $value) {
									echo "<option value='{$key}' " . (($view_model->get_status() == $key && $view_model->get_status() != '') ? 'selected' : '') . "> {$value} </option>";
								}?>
							</select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="Title">Company </label>
                            <select name='company_id' id='company_id' value='<?php echo $view_model->get_company_id(); ?>' class='form-control' >
                               <option value=''>Choose ...</option>
                                <?php foreach($this->_data['view_data']['companies'] as $company):?>
                                    <option <?php if($view_model->get_company_id() === $company->id){echo 'selected'; }?> value='<?php echo $company->id; ?>'><?php echo $company->name;?></option>
                                <?php endforeach;?>
                            </select>       
                        </div>
                        <div class="form-group col-md-4">
                            <label for="Title">Studio </label>
                            <select name='studio_id'  value='<?php echo $view_model->get_studio_id(); ?>' id='studio_id' class='form-control' >
                                <option value=''>Choose ...</option>
                                <?php foreach($this->_data['view_data']['studios'] as $studio):?>
                                    <option <?php if($view_model->get_studio_id() === $studio->id){echo 'selected'; }?> value='<?php echo $studio->id; ?>'><?php echo $studio->name;?></option>
                                <?php endforeach;?>
                            </select> 
                        </div>
                    </div>
                     <div class='form-row'>
                        <div class="form-group col-md-3">
							<div class="form-group">
								<label for="Release date">Release From </label>
								<input type="date" value='<?php echo $view_model->get_release_start_date(); ?>' class="form-control" id="release_start_date" name="release_start_date" />
							</div>
                        </div>
                        <div class='form-group col-md-3'>
                            <div class="form-group">
								<label for="Release date">Release To </label>
								<input type="date" class="form-control" value='<?php echo $view_model->get_release_end_date(); ?>' id="release_end_date" name="release_end_date" />
							</div>  
                        </div>
                        <div class='form-group col-md-6'>
                            <div class="form-group">
                                <label>Sort</label>
                                <select name='sort' value='<?php echo $view_model->get_sort(); ?>' class='form-control'>
                                    <option value=''>Choose... </option>
                                    <option value='a-z' <?php if( $view_model->get_sort() === 'a-z'){echo 'selected'; }?>>Alphabetical Ascending </option>
                                    <option value='z-a' <?php if( $view_model->get_sort() === 'a-z'){echo 'selected'; }?>>Alphabetical Descending</option>
                                    <option  value='old-new' <?php if( $view_model->get_sort() === 'old-new'){echo 'selected'; }?> >Release Date (oldest to newest) </option>
                                    <option value='new-old' <?php if( $view_model->get_sort() === 'new-old'){echo 'selected'; }?> >Release Date (newest to oldest) </option>
                                </select>
                            </div>
                        </div>
                     </div>           
                     <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <input type='hidden' name='filter' value='1'/>
                            <div class="form-group">
                                <input type="submit" name='btn-filter' class="btn btn-primary" value="Search">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mt-3">
        <div class="card">
            <h5 class="card-header">
                <div class="float-left"><?php echo $view_model->get_heading();?></div>
                <div class="float-right"><a class="btn btn-primary btn-sm" target="__blank" href="/admin/dvds/add"><i class="fas fa-plus-circle"></i></a></div>
                <div class="float-right">&nbsp;<div class="mkd-upload-form-btn-wrapper">	    <button class="mkd-upload-btn">Import</button>	    <input type="file" name="file_import" id="file_import" onchange="onFileImport(event, 'dvds')" accept=".csv"/></div>&nbsp;</div>
                <div class="clearfix"></div>
            </h5>
            <div class="card-body">
                &nbsp;<div class="mkd-upload-form-btn-wrapper">	    <button class="mkd-upload-btn">Import</button>	    <input type="file" name="file_import" id="file_import" onchange="onFileImport(event, 'dvds')" accept=".csv"/></div>&nbsp;
                <div class="table-responsive">
                    <table class="table table-hover table-condensed table-striped">
                        <thead>
                            <tr>
                                <th>Dvd thumbnail</th>
                                <th>Name</th>
                                <th>Rights</th>
                                <th>Studio</th>
                                <th>Release date</th>
                                <th>comp only</th>
                                <th>Scenes</th>
                                <th>status</th>
                                <th>Action</th>
                            </tr>  
                        </thead>
                        <tbody>
                            <?php foreach ($view_model->get_list() as $data): ?>
                                <tr>
                                    <td></td>
                                    <td><?php echo $data->title; ?></td>
                                    <td>rights</td>
                                    <td><?php echo $data->studio_name; ?></td>
                                    <td><?php echo $data->release_date; ?></td>
                                    <td><?php echo $view_model->is_for_comp_mapping()[$data->is_for_comp];?></td>
                                    <td></td>
                                    <td><?php echo $view_model->status_mapping()[$data->status]; ?></td>
                                    <td>
                                        <a class="btn btn-primary btn-sm" target="__blank" href="<?php echo base_url('/admin/dvds/edit/' . $data->id ); ?>">Edit</a>&nbsp;
                                        <a class="btn btn-warning btn-sm" target="__blank" href="<?php echo base_url('/admin/dvds/view/' . $data->id ); ?>">View</a>
                                    </td>
                                </tr>
                            <?php endforeach;?>    
                        </tbody>
                    </table>
                    <p class="pagination_custom"><?php echo $view_model->get_links(); ?></p>
                </div>
            </div>
        </div>
    </div>
</div>
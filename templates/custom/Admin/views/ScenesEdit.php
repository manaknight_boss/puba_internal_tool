<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
?>
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="page-header">
			<h2 class="pageheader-title"><?php echo $view_model->get_heading();?> </h2>
			<div class="page-breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/admin/dashboard" class="breadcrumb-link">Dashboard</a></li>
						<li class="breadcrumb-item"><a href="/admin/scenes/0" class="breadcrumb-link"><?php echo $view_model->get_heading();?></a></li>
						<li class="breadcrumb-item active" aria-current="page">Edit</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</div>
<div class="row">
    <?php if (validation_errors()) : ?>
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?= validation_errors() ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (strlen($error) > 0) : ?>
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (strlen($success) > 0) : ?>
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    <?php endif; ?>
</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Edit <?php echo $view_model->get_heading();?></h5>
                <div class="card-body">
                <?= form_open() ?>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label>Title</label>
                            <input id="title" name="title"  value='<?php echo set_value('title', $this->_data['view_model']->get_title());?>'  type="text" class="form-control"  placeholder="Scene title">
                        </div>
                        <div class="form-group col-md-4">
                            <label>Type</label>
                            <select id="type" name='type'  value="<?php echo set_value('type', $this->_data['view_model']->get_type());?>"  class="form-control">
                                <?php foreach($this->_data["view_data"]['types'] as $type ): ?>
                                    <option value='<?php echo $type->id;   ?>'><?php echo $type->name; ?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Primary Producer</label>
                            <select id="primary_producer_id" name='primary_producer_id' value='<?php echo set_value('primary_producer_id', $this->_data['view_model']->get_primary_producer_id());?>' class="form-control">
                                <?php foreach($this->_data["view_data"]['producers'] as $producer ): ?>
                                    <option value="<?php echo  $producer->id; ?>"><?php echo $producer->name; ?></option>
                                <?php endforeach;?>
                            </select>
                        </div>                    
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label>Secondary Producers</label>
                            <select class='multiple-select-control' name='secondary_producers[]' id="example-filter-placeholder" multiple="multiple" placeholder="choose producer">
								<?php foreach($this->_data["view_data"]['producers'] as $producer ): ?>
									<?php if(is_numeric(array_search($producer->id,array_column($this->_data["view_data"]['scene_producers'], 'producer_id') ))):?>
										<option selected value="<?php echo $producer->id; ?>"><?php echo $producer->name; ?></option>
									<?php else:?>
										<option value="<?php echo $producer->id; ?>"><?php echo $producer->name; ?></option>
									<?php endif;?>	
                    			<?php endforeach;?>
							</select>     
                        </div>
						<div class='form-group col-md-4'>
							<label for="Status">Status </label>
							<select id="form_status" name="status" class="form-control">
                        		<?php foreach ($view_model->status_mapping() as $key => $value) {
                            		echo "<option value='{$key}' " . (($view_model->get_status() == $key && $view_model->get_status() != '') ? 'selected' : '') . "> {$value} </option>";
                        		}?>
                    		</select>
						</div>
                        <div class="form-group col-md-4">
                            <label for="Image">Image </label>
					        <img id="output_image" />
					        <div class="btn btn-info btn-sm mkd-choose-image" data-image-url="image" data-image-id="thumbnail" data-image-preview="output_image" data-view-width="250" data-view-height="250" data-boundary-width="500" data-boundary-height="500">Choose Image</div>
					        <input type="hidden" id="thumbnail" name="thumbnail" value=""/>
					        <input type="hidden" id="thumbnail" name="thumbnail" value=""/>
                         </div>   
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label >Group</label>
                            <select id="inputState" name='group_id' id='group_id' value="<?php echo set_value('group_id', $this->_data['view_model']->get_group_id());?>" class="form-control">
                                <?php foreach($this->_data["view_data"]['groups'] as $group ): ?>
                                    <option value="<?php echo $group->id; ?>"><?php echo $group->name; ?></option>
                                <?php endforeach;?>   
                            </select>
                        </div>  
                        <div class="form-group col-md-4">
                            <label>Date Released</label><br>
                            <div class="input-group " >
                                <input type="date" name="date_released"  value="<?php echo set_value('date_released', $this->_data['view_model']->get_date_released());?>" class="form-control">
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Release Paperwork</label>
                            <input type='hidden' name='release-paper-work' id='release-paper-work'>
                           <button class='btn btn-primary' data-toggle="modal" data-target="#multiple-file-upload" type="button" data-target-input-id='release-paper-work'>Upload paper work</button>
                         </div>  
                    </div>
                    <div class='form-row'>
                        <div class="form-group col-md-3">
                            <label>TubeClip Url</label>
                            <input type="text" name='tubeclip_url'  value="<?php echo set_value('tubeclip_url', $this->_data['view_model']->get_tubeclip_url());?>" id="tubeclip_url" class="form-control"  placeholder="TubeClip Url">
                        </div>
                        <div class="form-group col-md-3">
                            <label>TSS Url</label>
                            <input type="text" name='tss_url' id="tss_url"  value='<?php echo set_value('tss_url', $this->_data['view_model']->get_tss_url());?>' class="form-control"  placeholder="TSS Url">
                        </div>
                        <div class="form-group col-md-3">
                            <label>XVideos</label>
                            <input type="text" name='xvideos_url' id='xvideos_url' value='<?php echo set_value('xvideos_url', $this->_data['view_model']->get_xvideos_url());?>' class="form-control"  placeholder="XVideos">						
						</div>
                        <div class="form-group col-md-3">
                            <label>channel</label>
                            <input type="text" name='channel' id='channel' value="<?php echo set_value('channel', $this->_data['view_model']->get_channel());?>" class="form-control"  placeholder="channel">
                        </div>
                    </div>
                     <div class="form-row">
                        <div class="form-group form-group col-md-6">
                            <label>Set Id</label>
                            <input type="text" name="set_id" id="set_id" value="<?php echo set_value('set_id', $this->_data['view_model']->get_set_id());?>" class="form-control"  placeholder="Set Id ">
                        </div> 
                        <div class="form-group form-group col-md-4">
                            <label for="Status">Status </label>
							<select id="form_status" name="status" class="form-control">
                        		<?php foreach ($view_model->status_mapping() as $key => $value) {
                            		echo "<option value='{$key}' " . (($view_model->get_status() == $key && $this->_data['view_model']->get_status() != '') ? 'selected' : '') . "> {$value} </option>";
                        		}?>
                    		</select>        
                        </div>
                     </div>               
                    <div class='form-row'>
                        <div class="form-group col-md-4">
                            <label>Studio</label>
                            <select id="studio_id" name='studio_id'  value="<<?php echo set_value('studio_id', $this->_data['view_model']->get_studio_id());?>" class="form-control">
                                <?php foreach($this->_data["view_data"]['studios'] as $studio ): ?>
                                    <option  value='<?php echo $studio->id; ?>'><?php echo $studio->name; ?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                        <div class="form-group col-md-4 ui-widget">
                            <label>Categories</label>	
							<select class='multiple-select-control' name='scene_categories[]' id="example-filter-placeholder" multiple="multiple">
								<?php foreach($this->_data["view_data"]['categories'] as $cat ): ?>
									<?php if(is_numeric(array_search($cat->id,array_column($this->_data["view_data"]['scene_categories'], 'category_id') ))):?>
											<option selected value="<?php echo $cat->id; ?>"><?php echo $cat->name; ?></option>
										<?php else:?>
											<option value="<?php echo $cat->id; ?>"><?php echo $cat->name; ?></option>
									<?php endif;?>	
                    			<?php endforeach;?>
							</select>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Scene Actors</label>
                            <select class='multiple-select-control' name='actors[]' id="example-filter-placeholder" multiple="multiple">
								<?php foreach($this->_data["view_data"]['actors'] as $actor ): ?>
                    				<?php if(is_numeric(array_search( $actor->id,array_column($this->_data["view_data"]['scene_actors'], 'actor_id') ))):?>
										<option selected value="<?php echo $actor->id; ?>"><?php echo $actor->stage_name; ?></option>
									<?php else:?>
										<option value="<?php echo $actor->id; ?>"><?php echo $actor->stage_name; ?></option>
									<?php endif;?>
                    			<?php endforeach;?>
								
							</select> 
                        </div>
                    </div>
                    <div class='form-row'>
                        <div class="form-group col-md-4">
                            <label>Media Type</label>
                            <select  id="studio_id" name='media_type'  value="<<?php echo set_value('media_type', $this->_data['view_model']->get_media_type());?>"   class="form-control">
                                <option value='Both'   <?php if($this->_data['view_model']->get_media_type() === 'Both'){ echo 'selected'; }?>>Both</option>
                                <option value='Video'  <?php if($this->_data['view_model']->get_media_type() === 'Video'){ echo 'selected'; }?>>Video</option>
                                <option value='Photo'  <?php if($this->_data['view_model']->get_media_type() === 'Photo'){ echo 'selected'; }?>>Photo</option>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Next Studio</label>
                            <select id="next_studio_id" name='next_studio_id'  value="<<?php echo set_value('next_studio_id', $this->_data['view_model']->get_next_studio_id());?>" class="form-control">
                                <?php foreach($this->_data["view_data"]['studios'] as $studio ): ?>
                                    <option  value='<?php echo $studio->id; ?>'><?php echo $studio->name; ?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
						<label for="Is For Comp">Is For Comp </label>
							<select id="form_is_for_comp" name="is_for_comp" class="form-control">
								<?php foreach ($view_model->is_for_comp_mapping() as $key => $value) {
									echo "<option value='{$key}' " . (($view_model->get_is_for_comp() == $key && $view_model->get_is_for_comp() != '') ? 'selected' : '') . "> {$value} </option>";
								}?>
							</select>
                        </div>
                    </div>
                    <hr></hr>
                    <div class="row">
                        <div class='col-md-8'>
                            <label>Broadcast (PTV)</label>
                            <input type='hidden' name='broadcast' id="broadcast" value="<?= htmlentities(json_encode($this->_data["view_data"]['scene_broadcast_companies'])) ?>"  />
                            <table class='table'>
                                <thead class="thead-dark">
                                    <tr>
                                        <th>Company</th>
                                        <th>Rights</th>
                                        <th>Terms</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="broadcast_table" data-action='add'>
									<?php foreach($this->_data["view_data"]['scene_broadcast_companies'] as $company):?>
										<tr>
											<td><?php echo $company['name']; ?></td>
											<td><?php echo $company['right_id']; ?></td>
											<td><?php echo $company['terms']; ?></td>
											<td><a href='#' data-id='<?php echo $company['company_id']; ?>' class='btn-remove-broadcast-company btn btn-primary btn-link btn-sm'>remove</a></td>
										</tr>
									<?php endforeach;?>
								</tbody>
                            </table>
                            <a href='#' data-toggle="modal" data-target="#searchBroadCastCompanies" type="button" data-id='broadcast' class="btn btn-primary  btn-sm btn-add-broadcast">Add</a>
                        </div>             
                    </div>
                    <hr></hr>
                    <div class='row'>
                        <div class='col-md-4'>
                            <label>Networks</label>
                            <input type='hidden' name='networks' id="networks"  value='<?= implode(',', array_column((array) $this->_data["view_data"]['scene_networks'], 'company_id')) ?>' />
                            <table class='table'>
                                <thead class="thead-dark">
                                    <tr>
                                        <th>Network</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="networks_table"  data-action='add'>
									<?php foreach($this->_data["view_data"]['networks'] as $company):?>
										<?php if(is_numeric(array_search($company->id, array_column($this->_data["view_data"]['scene_networks'], 'company_id') ))):?>
											<tr>
												<td><?php echo $company->name; ?></td>
												<td><a href='#' data-type='networks' data-id='<?php echo $company->id; ?>' class='btn btn-link remove-company btn btn-sm btn-link btn-primary'>remove</a></td>
											</tr>
										<?php endif;?>
									<?php endforeach;?>
								</tbody>
                            </table>
                            <a href="#" data-toggle="modal" data-target="#searchNetworkCompanies" type="button" data-id="networks" class=" btn btn-primary btn-sm btn-add-company">Add</a>
                        </div>
                        <div class='col-md-4'>
                            <label>View Share</label>
                            <input type='hidden' name='view_share' id="view_share"  value='<?php echo implode(',', array_column((array) $this->_data["view_data"]['scene_view_share'], 'company_id')); ?>' />
                            <table class='table'>
                                <thead class="thead-dark">
                                    <tr>
                                        <th>Company</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id='view_share_table'  data-action='add'>
									<?php foreach($this->_data["view_data"]['companies'] as $company):?>
										<?php if(is_numeric(array_search( $company->id,array_column($this->_data["view_data"]['scene_view_share'], 'company_id') ))):?>
											<tr>
												<td><?php echo $company->name; ?></td>
												<td><a href='#' data-type='view_share' data-id='<?php echo $company->id; ?>' class='btn btn-link remove-company btn btn-sm btn-link btn-primary'>remove</a></td>
											</tr>
										<?php endif;?>
									<?php endforeach;?>
                                </tbody>
                            </table>
                            <a href='#' data-toggle="modal" data-target="#searchViewShareCompanies" type="button" data-id="view_share" class="btn btn-primary btn-sm btn-add-company">Add</a>
                        </div>
                        <div class='col-md-4'>
                            <label>Web Rights</label>
                            <input type='hidden' name='web_rights'  value='<?php echo implode(',', array_column((array) $this->_data["view_data"]['scene_web_rights'], 'company_id')); ?>' id="web_rights" />
                            <table class='table'>
                                <thead class="thead-dark">
                                    <tr>
                                        <th>Company</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody  id="web_rights_table"  data-action='add'>
									<?php foreach($this->_data["view_data"]['companies'] as $company):?>
										<?php if(is_numeric(array_search($company->id, array_column($this->_data["view_data"]['scene_web_rights'], 'company_id') ))):?>
											<tr>
												<td><?php echo $company->name; ?></td>
												<td><a href='#' data-type='web_rights' data-id='<?php echo $company->id; ?>' class='btn btn-link remove-company btn btn-sm btn-link btn-primary'>remove</a></td>
											</tr>
										<?php endif;?>
									<?php endforeach;?>		
                                </tbody>
                            </table>
                            <a href='#' data-toggle="modal" data-target="#searchCompanies" type="button" data-id='web_rights' class="btn btn-primary btn-sm btn-add-company">Add</a>
                        </div>
                    </div>
                    <hr></hr>
                    <div class='row'>
                        <div class='col-md-4'>
                            <label>VOD Rights</label>
                            <input type='hidden' value="<?= implode(',',array_column((array) $this->_data["view_data"]['scene_vod_rights'], 'company_id')) ?>" name='vod_rights' id="vod_rights" />
                            <table class='table'>
                                <thead class="thead-dark">
                                    <tr>
                                        <th>Company</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="vod_rights_table"  data-action='add'>
									<?php foreach($this->_data["view_data"]['companies'] as $company):?>
										<?php if(is_numeric(array_search( $company->id,array_column($this->_data["view_data"]['scene_vod_rights'], 'company_id') ))):?>
											<tr>
												<td><?= $company->name ?></td>
												<td><a href='#' data-type='vod_rights' data-id='<?= $company->id?>' class='btn btn-link remove-company btn btn-sm btn-link btn-primary'>remove</a></td>
											</tr>
										<?php endif;?>
									<?php endforeach;?>			
                                </tbody>
                            </table>
                            <a href="#" data-toggle="modal" data-target="#searchCompanies" type="button" data-id='vod_rights' class="btn btn-primary btn-sm btn-add-company">Add </a>
                        </div>
                        <div class='col-md-4'>
                            <label>DVD rights</label>
                            <input type='hidden' name='dvd_rights' value='<?= implode(',',array_column((array) $this->_data["view_data"]['scene_dvd_rights'], 'company_id')) ?>'  id="dvd_rights" />
                            <table class='table'>
                                <thead class="thead-dark">
                                    <tr>
                                        <th>Company</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="dvd_rights_table"  data-action='add'>
									<?php foreach($this->_data["view_data"]['companies'] as $company):?>
										<?php if(is_numeric(array_search( $company->id,array_column($this->_data["view_data"]['scene_dvd_rights'], 'company_id') ))):?>
											<tr>
												<td><?= $company->name ?></td>
												<td><a href='#' data-type='dvd_rights' data-id='<?= $company->id?>' class='btn btn-link remove-company btn btn-sm btn-link btn-primary'>remove</a></td>
											</tr>
										<?php endif;?>
									<?php endforeach;?>		
                                </tbody>
                            </table>
                            <a href='#' data-toggle="modal" data-target="#searchCompanies" type="button" data-id="dvd_rights" class="btn btn-primary btn-sm btn-add-company">Add</a>
                        </div>
                        <div class='col-md-4'>
                        </div>
                    </div>
                    <div class='form-row'>
                         <div style="width:100%; height:10px; float:none; clear:both;"></div>           
                        <input type='submit' class='btn btn-lg btn-primary' value='Save Scene'/>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="searchViewShareCompanies">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Search View Share Companies</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body"  style="height: 300px; overflow-y:scroll; overflow-x:hidden;">
                <table class='table display'>
                    <thead>
                        <tr>
                            <th>Company Name</th>
                            <th>Action</th>
                        <tr>
                    </thead>
                    <tbody>
                        <?php foreach($this->_data["view_data"]['view_share'] as $company ): ?>
                            <tr>
                                <td><?= $company->name ?></td>
                                <td><a href='#' class='btn btn-primary btn-sm btn-select-view-share-company' data-companyName='<?= $company->name ?>' data-companyId='<?= $company->id ?>'>Add company</a></td>
                            </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
  </div>
</div>
<div class="modal" id="searchNetworkCompanies">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Search Network Companies</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body"  style="height: 300px; overflow-y:scroll; overflow-x:hidden;">
                <table class='table display'>
                    <thead>
                        <tr>
                            <th>Company Name</th>
                            <th>Action</th>
                        <tr>
                    </thead>
                    <tbody>
                        <?php foreach($this->_data["view_data"]['networks'] as $company ): ?>
                            <tr>
                                <td><?= $company->name ?></td>
                                <td><a href='#' class='btn btn-primary btn-sm btn-select-network-company' data-companyName='<?= $company->name ?>' data-companyId='<?= $company->id ?>'>Add company</a></td>
                            </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
  </div>
</div>

<div class="modal" id="searchCompanies">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Search Companies</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body"  style="height: 300px; overflow-y:scroll; overflow-x:hidden;">
                <table class='table display'>
                    <thead>
                        <tr>
                            <th>Company Name</th>
                            <th>Action</th>
                        <tr>
                    </thead>
                    <tbody>
                        <?php foreach($this->_data["view_data"]['companies'] as $company ): ?>
                            <tr>
                                <td><?= $company->name ?></td>
                                <td><a href='#' class='btn btn-primary btn-sm btn-select-company' data-companyName='<?= $company->name ?>' data-companyId='<?= $company->id ?>'>Add company</a></td>
                            </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
  </div>
</div>
<div class="modal" id="multiple-file-upload">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                 <input type="hidden" id="upload-url" value="<?php echo base_url('admin/scenes/multiple_uploads'); ?>">           
                <h4 class="modal-title">Upload Files</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body"  style="height: 300px; overflow-y:scroll; overflow-x:hidden;">
            <div id="drop_file_zone" style='width:100%;' ondrop="upload_file(event)" ondragover="return false">
                <div id="drag_upload_file">
                    <p>Drop file here</p>
                        <p>or</p>
                    <p><input type="button" value="Select File" onclick="file_explorer();"></p>
                    <input type="file" id="selectfile" multiple>
                    </div>
                </div>
                <table class='table display'>
                    <thead>
                        <tr>
                            <th>Uploaded files</th>
                            <th>Action</th>
                        <tr>
                    </thead>
                    <tbody id='uploaded-files'>
                        
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
  </div>
</div>
<div class="modal" id="searchBroadCastCompanies">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Broadcast Companies</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body"  style="height: 300px; overflow-y:scroll; overflow-x:hidden;">
                <table class='table display'>
                    <thead>
                        <tr>
                            <th>Company Name</th>
                            <th>Broadcast right options</th>
                            <th>Terms</th>
                            <th>Action</th>
                        <tr>
                    </thead>
                    <tbody>
                        <?php foreach($this->_data["view_data"]['broadcasters'] as $company ): ?>
                            <tr>
                                <td><?= $company->name ?></td>
                                <td>
                                    <select class='form-control' id='broadcast_rights<?= $company->id ?>'>
                                        <option value='Exclusive Broadcast'>Exclusive Broadcast</option>
                                        <option value='Non-Exclusive Broadcast'>Non-Exclusive Broadcast</option>
                                        <option value='Non-Exclusive Mobile'>Non-Exclusive Mobile</option>
                                        <option value='Exclusive Broadcast VOD'>Exclusive Broadcast VOD</option>
                                        <option value='Non-Exclusive Broadcast VOD'>Non-Exclusive Broadcast VOD)</option>
                                    </select>
                                </td>
                                <td>
                                    <select class='form-control' id='terms<?=  $company->id ?>'>
                                        <option value="1">1 year</option>
                                        <option value="2">2 years</option>
                                        <option value="3">3 years</option>
                                        <option value="4">4 years</option>
                                        <option value="5">5 years</option>
                                        <option value="6">6 years</option>
                                        <option value="6">7 years</option>
                                        <option value="8">8 years</option>
                                        <option value="9">9 years</option>
                                        <option value="10">10 years</option>
                                    </select>
                                </td>
                                <td><a href='#' class='btn btn-primary btn-sm btn-select-broadcast-company' data-companyName='<?= $company->name ?>' data-companyId='<?= $company->id ?>'>Add</a></td>
                            </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
  </div>
</div>
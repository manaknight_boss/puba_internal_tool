<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Admin_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * User2257 Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Admin_user2257_controller extends Admin_controller
{
    protected $_model_file = 'user2257_model';
    public $_page_name = '2257 Users';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin_operation_model');

    }

	public function index($page)
	{
        $this->load->library('pagination');
        include_once __DIR__ . '/../../view_models/User2257_admin_list_paginate_view_model.php';
        $session = $this->get_session();
        $this->_data['view_model'] = new User2257_admin_list_paginate_view_model($this->user2257_model,$this->pagination,'/admin/users2257/0');
        $this->_data['view_model']->set_heading('2257 Users');
        $this->_data['view_model']->set_id(($this->input->get('id', TRUE) != NULL) ? $this->input->get('id', TRUE) : NULL);
		$this->_data['view_model']->set_email(($this->input->get('email', TRUE) != NULL) ? $this->input->get('email', TRUE) : NULL);
		$this->_data['view_model']->set_status(($this->input->get('status', TRUE) != NULL) ? $this->input->get('status', TRUE) : NULL);

        $where = [
            'id' => $this->_data['view_model']->get_id(),
			'email' => $this->_data['view_model']->get_email(),
			'status' => $this->_data['view_model']->get_status(),

        ];

        $this->_data['view_model']->set_total_rows($this->user2257_model->count($where));

        $this->_data['view_model']->set_per_page(10);
        $this->_data['view_model']->set_page($page);
		$this->_data['view_model']->set_list($this->user2257_model->get_paginated(
            $this->_data['view_model']->get_page(),
            $this->_data['view_model']->get_per_page(),
            $where));
        return $this->render('Admin/User2257', $this->_data);
	}

	public function add()
	{
        include_once __DIR__ . '/../../view_models/User2257_admin_add_view_model.php';
        $this->load->model('Company_user2257_model');
        $this->form_validation = $this->user2257_model->set_form_validation(
        $this->form_validation, $this->user2257_model->get_all_validation_rule());
        $this->_data['view_model'] = new User2257_admin_add_view_model($this->user2257_model);
        $this->_data['view_model']->set_heading('User2257');
        $this->_data['view_data'] = $this->_get_view_add_data();

		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/User2257Add', $this->_data);
        }

        $email = $this->input->post('email');
		$password = $this->input->post('password');
		$company_id = (int) $this->input->post('company_id');
        $result = $this->user2257_model->create([
            'email' => $email,
			'password' => $password,
			"password" => str_replace('$2y$', '$2b$', password_hash($password, PASSWORD_BCRYPT)),
        ]);

        if ($result)
        {
            if(is_int($company_id) && $company_id > 0)
            {
                $this->Company_user2257_model->create([
                    'user_id' => $result,
                    'company_id' => $company_id
                ]);
            }
            $this->success('Saved');
            $this->admin_operation_model->log_activity('add 2257 user', [
                'email' => $email,
                'password' => $password,
                'company_id' => $company_id
            ], $this->get_session()['user_id']);
            return $this->redirect('/admin/users2257/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Admin/User2257Add', $this->_data);
	}

	public function edit($id)
	{
        $model = $this->user2257_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/admin/users2257/0');
        }

        include_once __DIR__ . '/../../view_models/User2257_admin_edit_view_model.php';
        $this->form_validation = $this->user2257_model->set_form_validation(
        $this->form_validation, $this->user2257_model->get_all_edit_validation_rule());
        $this->_data['view_model'] = new User2257_admin_edit_view_model($this->user2257_model);
        $this->_data['view_model']->set_model($model);
        $this->_data['view_model']->set_heading('User2257');

		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/User2257Edit', $this->_data);
        }

        $email = $this->input->post('email');
		$password = $this->input->post('password');
		$status = $this->input->post('status');

        $result = $this->user2257_model->edit([
            'email' => $email,
			'password' => $password,
			'status' => $status,
			"password" => str_replace('$2y$', '$2b$', password_hash($password, PASSWORD_BCRYPT)),
        ], $id);

        if ($result)
        {

            return $this->redirect('/admin/users2257/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Admin/User2257Edit', $this->_data);
	}

	public function view($id)
	{
        $model = $this->user2257_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/admin/users2257/0');
		}


        include_once __DIR__ . '/../../view_models/User2257_admin_view_view_model.php';
		$this->_data['view_model'] = new User2257_admin_view_view_model($this->user2257_model);
		$this->_data['view_model']->set_heading('User2257');
        $this->_data['view_model']->set_model($model);
        return $this->render('Admin/User2257View', $this->_data);
	}

	public function delete($id)
	{
        $model = $this->user2257_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/admin/users2257/0');
        }

        $result = $this->user2257_model->real_delete($id);

        if ($result)
        {

            return $this->redirect('/admin/users2257/0', 'refresh');
        }

        $this->error('Error');
        return redirect('/admin/users2257/0');
    }

    public function _get_view_add_data()
    {
        $this->load->model('Company_model');
        $view_data = [
            'companies' => $this->Company_model->get_all()
        ];
        return $view_data;
    }

}
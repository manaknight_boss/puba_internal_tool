<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Admin_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Company Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Admin_company_controller extends Admin_controller
{
    protected $_model_file = 'company_model';
    public $_page_name = 'Companies';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin_operation_model');
    }

	public function index($page)
	{
        $this->load->library('pagination');
        include_once __DIR__ . '/../../view_models/Company_admin_list_paginate_view_model.php';
        $session = $this->get_session();
        $this->_data['view_model'] = new Company_admin_list_paginate_view_model($this->company_model,$this->pagination,'/admin/company/0');
        $this->_data['view_model']->set_heading('Companies');
        $this->_data['view_model']->set_id(($this->input->get('id', TRUE) != NULL) ? $this->input->get('id', TRUE) : NULL);
		$this->_data['view_model']->set_name(($this->input->get('name', TRUE) != NULL) ? $this->input->get('name', TRUE) : NULL);
		$this->_data['view_model']->set_status(($this->input->get('status', TRUE) != NULL) ? $this->input->get('status', TRUE) : NULL);

        $where = [
            'id' => $this->_data['view_model']->get_id(),
			'name' => $this->_data['view_model']->get_name(),
			'status' => $this->_data['view_model']->get_status(),

        ];

        $this->_data['view_model']->set_total_rows($this->company_model->count($where));

        $this->_data['view_model']->set_per_page(10);
        $this->_data['view_model']->set_page($page);
		$this->_data['view_model']->set_list($this->company_model->get_paginated($this->_data['view_model']->get_page(),$this->_data['view_model']->get_per_page(),$where));
        return $this->render('Admin/Company', $this->_data);
	}

	public function add()
	{
        include_once __DIR__ . '/../../view_models/Company_admin_add_view_model.php';
        $this->load->model('Company_user_model');
        $this->load->model('Company_studios_model');
        $this->form_validation = $this->company_model->set_form_validation(
        $this->form_validation, $this->company_model->get_all_validation_rule());
        $this->_data['view_model'] = new Company_admin_add_view_model($this->company_model);
        $this->_data['view_model']->set_heading('Company');
        $this->_data['view_data'] = $this->_get_view_add_data();

        if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/CompanyAdd', $this->_data);
        }

        $name = $this->input->post('name');
		$address = $this->input->post('address');
        $address_2257 = $this->input->post('address_2257');
        $company_users = $this->input->post('company_users');
        $company_studios = $this->input->post('company_studios');
        $payload = [
            'name' => $name,
			'address' => $address,
			'address_2257' => $address_2257,

        ];
        $result = $this->company_model->create($payload);

        if ($result)
        {
            if(is_array($company_users) && !empty($company_users))
            {
                $param = [];

                for($i = 0; $i < count($company_users); $i ++)
                {
                    $param[] = [
                        'company_id' => $result,
                        'user_id' => $company_users[$i]
                    ];
                }

                $this->Company_user_model->batch_insert($param);
                $payload['users'] = $param;
            }
            if(is_array($company_studios) && !empty($company_studios))
            {
                $param = [];

                for($i = 0; $i < count($company_studios); $i ++)
                {
                    $param[] = [
                        'company_id' => $result,
                        'studio_id' => $company_studios[$i]
                    ];
                }

                $this->Company_studios_model->batch_insert($param);
                $payload['studios'] = $param;
            }
            $this->success('Saved');
			$this->admin_operation_model->log_activity('add company', $payload, $this->get_session()['user_id']);
           return $this->redirect('/admin/company/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Admin/CompanyAdd', $this->_data);
	}

	public function edit($id)
	{
        $model = $this->company_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/admin/company/0');
        }

        include_once __DIR__ . '/../../view_models/Company_admin_edit_view_model.php';
        $this->load->model('Company_user_model');
        $this->load->model('Company_studios_model');

        $this->form_validation = $this->company_model->set_form_validation(
        $this->form_validation, $this->company_model->get_all_edit_validation_rule());
        $this->_data['view_model'] = new Company_admin_edit_view_model($this->company_model);
        $this->_data['view_model']->set_model($model);
        $this->_data['view_model']->set_heading('Company');
        $this->_data['view_data'] = $this->_get_view_edit_data($id);

		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/CompanyEdit', $this->_data);
        }

        $name = $this->input->post('name');
		$address = $this->input->post('address');
		$address_2257 = $this->input->post('address_2257');
        $status = $this->input->post('status');
        $company_users = $this->input->post('company_users');
        $company_studios = $this->input->post('company_studios');


        $result = $this->company_model->edit([
            'name' => $name,
			'address' => $address,
			'address_2257' => $address_2257,
			'status' => $status,

        ], $id);

        if ($result)
        {
            if(is_array($company_users) && !empty($company_users))
            {
                if($this->Company_user_model->delete_by_company($id))
                {
                    $param = [];

                    for($i = 0; $i < count($company_users); $i ++)
                    {
                        $param[] = [
                        'company_id' => $id,
                        'user_id' => $company_users[$i]
                        ];
                    }

                    $this->Company_user_model->batch_insert($param);
                }
            }

            if(is_array($company_studios) && !empty($company_studios))
            {
               if($this->Company_studios_model->delete_by_company($id))
               {
                    $param = [];

                    for($i = 0; $i < count($company_studios); $i ++)
                    {
                        $param[] = [
                            'company_id' => $id,
                            'studio_id' => $company_studios[$i]
                        ];
                    }

                    $this->Company_studios_model->batch_insert($param);
               }
            }

            return $this->redirect('/admin/company/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Admin/CompanyEdit', $this->_data);
	}

	public function view($id)
	{
        $model = $this->company_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/admin/company/0');
		}


        include_once __DIR__ . '/../../view_models/Company_admin_view_view_model.php';
		$this->_data['view_model'] = new Company_admin_view_view_model($this->company_model);
		$this->_data['view_model']->set_heading('Company');
        $this->_data['view_model']->set_model($model);
        return $this->render('Admin/CompanyView', $this->_data);
    }

    private function _get_view_add_data()
    {
        $this->load->model('User_model');
        $this->load->model('Studio_model');
        $view_data = [
            'users' => $this->User_model->get_staff_2257_user(),
            'studios' => $this->Studio_model->get_all(),
        ];
        return $view_data;
    }

    private function _get_view_edit_data($id = 0)
    {
        $this->load->model('User_model');
        $this->load->model('Studio_model');
        $this->load->model('Company_user_model');
        $this->load->model('Company_studios_model');

        $view_data = [
            'users' => $this->User_model->get_staff_2257_user(),
            'studios' => $this->Studio_model->get_all(),
            'company_users' => $this->Company_user_model->get_by_company($id),
            'company_studios' => $this->Company_studios_model->get_by_company($id)
        ];
        return $view_data;

    }



}
<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once __DIR__ . '/../../services/User_service.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Reset Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Auditor_reset_controller extends Manaknight_Controller
{
	public function index ($reset_token)
	{
        $this->load->model('user_model');
        $this->load->model('token_model');

        $service = new User_service($this->user_model);
        $service->set_token_model($this->token_model);

        $valid_user = $service->valid_reset_token($reset_token);

        if (!$valid_user)
        {
            $this->error('Email does not exist in our system.');
            return $this->redirect('/auditor/login');
        }

        $this->_data['reset_token'] = $reset_token;

        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|matches[password]');

        if ($this->form_validation->run() === FALSE)
        {
            echo $this->load->view('Auditor/Reset', $this->_data, TRUE);
            exit;
        }

        $password = $this->input->post('password');
        $password_reseted = $service->reset_password($valid_user->id, $password);

        if ($password_reseted)
        {
            $service->invalidate_token($reset_token, $valid_user->id);
            $this->success('Your password was reset. Try to login now');
            return $this->redirect('/auditor/login');
        }

        $this->error('Invalid reset token to reset password.');
        return $this->redirect('/auditor/login');
	}
}
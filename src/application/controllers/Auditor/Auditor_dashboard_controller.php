<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Auditor_controller.php';

/**
 * Auditor Dashboard Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Auditor_dashboard_controller extends Auditor_controller
{
    public $_page_name = 'Dashboard';

    public function __construct()
    {
        parent::__construct();
    }

    public function index ()
    {
        return $this->render('Auditor/Dashboard', $this->_data);
    }
}
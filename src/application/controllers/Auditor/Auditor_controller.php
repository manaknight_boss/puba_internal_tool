<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Auditor Controller
 *
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Auditor_controller extends Manaknight_Controller
{

    public $_page_name ='dashboard';

    public $_valid_roles = [3];

    public function __construct()
    {
        parent::__construct();

        $this->_data['page_name'] = $this->_page_name;
        $this->_data['setting'] = $this->_setting;
        $this->_run_middlewares();
    }

    protected function _middleware()
    {
        return [
            'auth', 'acl', 'maintenance'
        ];
    }

    public function render($template, $data)
    {
        return (!$this->_test_mode) ? $this->_render($template, $data) : $this->_render_test($template, $data);
    }

    protected function _render_test($template, $data)
    {
        return [
            'header' => $this->load->view('Layout/AuditorHeader', $data, TRUE),
            'body' => $this->load->view($template, $data, TRUE),
            'footer' => $this->load->view('Layout/AuditorFooter', $data, TRUE),
            'data' => $data,
        ];
    }

    protected function _render($template, $data)
    {
        $this->load->view('Layout/AuditorHeader', $data);
        $this->load->view($template, $data);
        $this->load->view('Layout/AuditorFooter');
    }
}
<?php 
include_once dirname(__FILE__) . '/../../core/CLI_Controller.php';
 
class Data_seed_cronjob_controller extends CLI_Controller
{
   
    
    public function __construct()
    {
        parent::__construct();
    }
   
    public function comp_scene_to_next_studio()
    {
        $this->load->database();
        $this->load->model('Scenes_model', 'scenes');
        
    }

    public function seed_categories()
    {
        ini_set('memory_limit','1024M');
        $this->load->database();
        $this->load->model('Category_model');
        $this->load->model('Data_migration_mapping_model');
        $sql = "SELECT * FROM cms_categories";
        $connection = $this->_get_connection();
        $statement =  $connection->prepare($sql);
        $statement->execute();
        $count = $statement->rowCount();
        $per_page = 100;
        $num_pages = ceil($count / $per_page);
        echo "$count $per_page $num_pages\n";

        for($i=0; $i < $num_pages; $i++)
        {
            $offset = $i * $per_page;
            $sql = "SELECT * FROM cms_categories  LIMIT {$per_page} OFFSET {$offset} ";
            $statement =  $connection->prepare($sql);
            $statement->execute();
            $results = $statement->fetchAll(PDO::FETCH_ASSOC);
            $mapping_array = [];

            foreach($results as $result)
            {
                $temp = [
                    'name' => $result['name'],
                    'status' => 1
                ];
                $insert_id = $this->Category_model->create( $temp);
                
                if($insert_id)
                {
                    $mapping_array[] = [
                        'old_id' => $result['catid'],
                        'new_id' => $insert_id,
                        'source_table' => 'cms_categories',
                        'destination_table' => 'category'
                    ];
                }
            }
           $this->Data_migration_mapping_model->batch_insert($mapping_array);
           echo (($i * 100) ) .  " of $count inserted \n";
        }
    }

    public function seed_actors()
    {
        ini_set('memory_limit','1024M');
        $this->load->database();
        $this->load->model('Actor_model', 'actors');
        $this->load->model('Data_migration_mapping_model');
        $sql = "SELECT * FROM cms_actors";
        $connection = $this->_get_connection();
        $statement =  $connection->prepare($sql);
        $statement->execute();
        $count = $statement->rowCount();
        $per_page = 100;
        $num_pages = ceil($count / $per_page);
        echo "$count $per_page $num_pages\n";
        
        for($i=0; $i < $num_pages; $i++)
        {
            $offset = $i * $per_page;
            $sql = "SELECT * FROM cms_actors  LIMIT {$per_page} OFFSET {$offset} ";
            $statement =  $connection->prepare($sql);
            $statement->execute();
            $results = $statement->fetchAll(PDO::FETCH_ASSOC);
            $mapping_array = [];
            
            foreach($results as $result)
            {
                 $temp = [
                    'name' => $result['real_name'],
                    'stage_name' => $result['name'],
                    'additional_name' => $result['additional_name'] ?? " ",
                    'gender' => $this->_map_gender($result['gender']),
                    'image' =>'',
                    'image_id' => 0,
                    'dob' => null,
                ];
                $insert_id = $this->actors->create( $temp);
                
                if($insert_id)
                {
                    $mapping_array[] = [
                        'old_id' => $result['actorid'],
                        'new_id' => $insert_id,
                        'source_table' => 'cms_actors',
                        'destination_table' => 'actor'
                    ];
                }
            }
            $this->Data_migration_mapping_model->batch_insert($mapping_array);
            echo ($i * 100) .  " of $count inserted \n";
        }
    }

    public function seed_scenes()
    {
        ini_set('memory_limit','1024M');
        $this->load->database();
        $this->load->model('Scenes_model', 'scenes');
        $this->load->model('Data_migration_mapping_model');
        $sql = "SELECT * FROM cms_gals";
        $connection = $this->_get_connection();
        $statement =  $connection->prepare($sql);
        $statement->execute();
        $count = $statement->rowCount();
        $per_page = 100;
        $num_pages = ceil($count / $per_page);
        $comp_date = date("Y-m-d", strtotime("+6 month"));
        echo "$count $per_page $num_pages\n";
        

        for($i=0; $i < $num_pages; $i++)
        {
            $offset = $i * $per_page;
            $sql = "SELECT * FROM cms_gals  LIMIT {$per_page} OFFSET {$offset} ";
            $statement =  $connection->prepare($sql);
            $statement->execute();
            $results = $statement->fetchAll(PDO::FETCH_ASSOC);
            $mapping_array = [];

            foreach($results as $result)
            {
                $temp = [
                    'title' => $result['caption'],
			        'tubeclip_url' => " ",
			        'xvideos_url' => " ",
			        'type' => 0,
			        'primary_producer_id' => 1,
			        'group_id' => 0,
			        'channel' => " ",
			        'set_id' => $result['setid'],
			        'tss_url' => " ",
			        'media_type' => $result['type'],
			        'studio_id' => 0,
			        'is_for_comp' => 1,
			        'date_released' => $result['added'],
			        'next_studio_id' => 0,
			        'comp_after_date' => $comp_date
                ];
                $insert_id = $this->scenes->create( $temp);

                if($insert_id)
                {
                    $mapping_array[] = [ 
                        'old_id' => $result['galid'],
                        'new_id' => $insert_id,
                        'source_table' => 'cms_gals',
                        'destination_table' => 'scenes'
                    ];
                }
            }
            
            $this->Data_migration_mapping_model->batch_insert($mapping_array);
            echo ($i * 100) .  " of $count inserted \n";

        }
    }


    private function _get_connection()
    {
        $conn = new PDO("mysql:host=localhost;dbname=carma", 'root', '');
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $conn;
    }

    private function _map_gender($gender)
    {
        if($gender === 'm')
        {
            return 1;
        }
        
        if($gender === 'f' )
        {
            return 0;
        }

        return 3;
    }
    
}
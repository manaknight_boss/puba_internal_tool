<?php
include_once dirname(__FILE__) . '/../../core/CLI_Controller.php';

class Actor_expiry_date_cronjob_controller extends CLI_Controller{

    public function __construct()
    {
        parent::__construct();
    }
   
    /**
     * @todo create query selects expiry date less than today
     */
    public function update_actor_status()
    {
        ini_set('memory_limit','1024M');
        $this->load->database();
        $this->load->model('Actor_model');
        $count = $this->Actor_model->count([]);
        $per_page = 100;
        $num_pages = ceil($count / $per_page);
        echo "$count $per_page $num_pages";
        $current_date =  date("Y-m-d", strtotime( date("Y-m-d")));
        for ($i=0; $i < 4245; $i++)
        {
            $results = $this->Actor_model->get_paginated($i * $per_page,$per_page,[]);
            echo "Page: $i\n";
            foreach ($results as $result)
            {
               if($result->expiry_date !== null && $result->expiry_date < $current_date )
               {
                    $this->Actor_model->edit(['status' => 0 ],$result->id);
               }
            }
        }
        echo 'Cron job complete';
    }

}
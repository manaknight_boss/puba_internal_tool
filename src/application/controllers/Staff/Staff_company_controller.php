<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Staff_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Company Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Staff_company_controller extends Staff_controller
{
    protected $_model_file = 'company_model';
    public $_page_name = 'Companies';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('staff_operation_model');

    }

	public function index($page)
	{
        $this->load->library('pagination');
        include_once __DIR__ . '/../../view_models/Company_staff_list_paginate_view_model.php';
        $session = $this->get_session();
        $this->_data['view_model'] = new Company_staff_list_paginate_view_model(
            $this->company_model,
            $this->pagination,
            '/staff/company/0');
        $this->_data['view_model']->set_heading('Companies');
        $this->_data['view_model']->set_id(($this->input->get('id', TRUE) != NULL) ? $this->input->get('id', TRUE) : NULL);
		$this->_data['view_model']->set_name(($this->input->get('name', TRUE) != NULL) ? $this->input->get('name', TRUE) : NULL);
		$this->_data['view_model']->set_status(($this->input->get('status', TRUE) != NULL) ? $this->input->get('status', TRUE) : NULL);
		
        $where = [
            'id' => $this->_data['view_model']->get_id(),
			'name' => $this->_data['view_model']->get_name(),
			'status' => $this->_data['view_model']->get_status(),
			
        ];

        $this->_data['view_model']->set_total_rows($this->company_model->count($where));

        $this->_data['view_model']->set_per_page(10);
        $this->_data['view_model']->set_page($page);
		$this->_data['view_model']->set_list($this->company_model->get_paginated(
            $this->_data['view_model']->get_page(),
            $this->_data['view_model']->get_per_page(),
            $where));
        return $this->render('Staff/Company', $this->_data);
	}

	public function add()
	{
        include_once __DIR__ . '/../../view_models/Company_staff_add_view_model.php';
        $this->form_validation = $this->company_model->set_form_validation(
        $this->form_validation, $this->company_model->get_all_validation_rule());
        $this->_data['view_model'] = new Company_staff_add_view_model($this->company_model);
        $this->_data['view_model']->set_heading('Companies');

		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Staff/CompanyAdd', $this->_data);
        }

        $name = $this->input->post('name');
		$address = $this->input->post('address');
		$address_2257 = $this->input->post('address_2257');
		
        $result = $this->company_model->create([
            'name' => $name,
			'address' => $address,
			'address_2257' => $address_2257,
			
        ]);

        if ($result)
        {
            $this->success('Saved');
			$this->staff_operation_model->log_activity('add company', $this->company_model->get($result), $this->get_session()['user_id']);
            return $this->redirect('/staff/company/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Staff/CompanyAdd', $this->_data);
	}

	public function edit($id)
	{
        $model = $this->company_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/staff/company/0');
        }

        include_once __DIR__ . '/../../view_models/Company_staff_edit_view_model.php';
        $this->form_validation = $this->company_model->set_form_validation(
        $this->form_validation, $this->company_model->get_all_edit_validation_rule());
        $this->_data['view_model'] = new Company_staff_edit_view_model($this->company_model);
        $this->_data['view_model']->set_model($model);
        $this->_data['view_model']->set_heading('Companies');
        
		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Staff/CompanyEdit', $this->_data);
        }

        $name = $this->input->post('name');
		$address = $this->input->post('address');
		$address_2257 = $this->input->post('address_2257');
		$status = $this->input->post('status');
		
        $result = $this->company_model->edit([
            'name' => $name,
			'address' => $address,
			'address_2257' => $address_2257,
			'status' => $status,
			
        ], $id);

        if ($result)
        {
            
            return $this->redirect('/staff/company/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Staff/CompanyEdit', $this->_data);
	}

	public function view($id)
	{
        $model = $this->company_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/staff/company/0');
		}


        include_once __DIR__ . '/../../view_models/Company_staff_view_view_model.php';
		$this->_data['view_model'] = new Company_staff_view_view_model($this->company_model);
		$this->_data['view_model']->set_heading('Companies');
        $this->_data['view_model']->set_model($model);
        return $this->render('Staff/CompanyView', $this->_data);
	}




}
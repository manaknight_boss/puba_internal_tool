<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Staff_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Actor Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Staff_actor_controller extends Staff_controller
{
    protected $_model_file = 'actor_model';
    public $_page_name = 'Actors';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin_operation_model');

    }

	public function index($page)
	{
        $this->load->library('pagination');
        include_once __DIR__ . '/../../view_models/Actor_staff_list_paginate_view_model.php';
        $session = $this->get_session();
        $this->_data['view_model'] = new Actor_staff_list_paginate_view_model(
            $this->actor_model,
            $this->pagination,
            '/staff/actor/0');
        $this->_data['view_model']->set_heading('Actors');
        $this->_data['view_model']->set_name(($this->input->get('name', TRUE) != NULL) ? $this->input->get('name', TRUE) : NULL);
		$this->_data['view_model']->set_stage_name(($this->input->get('stage_name', TRUE) != NULL) ? $this->input->get('stage_name', TRUE) : NULL);
		$this->_data['view_model']->set_gender(($this->input->get('gender', TRUE) != NULL) ? $this->input->get('gender', TRUE) : NULL);
		$this->_data['view_model']->set_dob(($this->input->get('dob', TRUE) != NULL) ? $this->input->get('dob', TRUE) : NULL);
		$this->_data['view_model']->set_expiry_date(($this->input->get('expiry_date', TRUE) != NULL) ? $this->input->get('expiry_date', TRUE) : NULL);
		
        $where = [
            'name' => $this->_data['view_model']->get_name(),
			'stage_name' => $this->_data['view_model']->get_stage_name(),
			'gender' => $this->_data['view_model']->get_gender(),
			'dob' => $this->_data['view_model']->get_dob(),
			'expiry_date' => $this->_data['view_model']->get_expiry_date(),
			
        ];

        $this->_data['view_model']->set_total_rows($this->actor_model->count($where));

        $this->_data['view_model']->set_per_page(10);
        $this->_data['view_model']->set_page($page);
		$this->_data['view_model']->set_list($this->actor_model->get_paginated(
            $this->_data['view_model']->get_page(),
            $this->_data['view_model']->get_per_page(),
            $where));
        return $this->render('Staff/Actor', $this->_data);
	}

	public function add()
	{
        include_once __DIR__ . '/../../view_models/Actor_staff_add_view_model.php';
        $this->form_validation = $this->actor_model->set_form_validation(
        $this->form_validation, $this->actor_model->get_all_validation_rule());
        $this->_data['view_model'] = new Actor_staff_add_view_model($this->actor_model);
        $this->_data['view_model']->set_heading('Actors');

		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Staff/ActorAdd', $this->_data);
        }

        $name = $this->input->post('name');
		$stage_name = $this->input->post('stage_name');
		$gender = $this->input->post('gender');
		$image = $this->input->post('image');
		$image_id = $this->input->post('image_id');
		$dob = $this->input->post('dob');
		$expiry_date = $this->input->post('expiry_date');
		
        $result = $this->actor_model->create([
            'name' => $name,
			'stage_name' => $stage_name,
			'gender' => $gender,
			'image' => $image,
			'image_id' => $image_id,
			'dob' => $dob,
			'expiry_date' => $expiry_date,
			
        ]);

        if ($result)
        {
            $this->success('Saved');
			$this->staff_operation_model->log_activity('add actor', $this->actor_model->get($result), $this->get_session()['user_id']);
            return $this->redirect('/staff/actor/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Staff/ActorAdd', $this->_data);
	}

	public function edit($id)
	{
        $model = $this->actor_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/staff/actor/0');
        }

        include_once __DIR__ . '/../../view_models/Actor_staff_edit_view_model.php';
        $this->form_validation = $this->actor_model->set_form_validation(
        $this->form_validation, $this->actor_model->get_all_edit_validation_rule());
        $this->_data['view_model'] = new Actor_staff_edit_view_model($this->actor_model);
        $this->_data['view_model']->set_model($model);
        $this->_data['view_model']->set_heading('Actors');
        
		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Staff/ActorEdit', $this->_data);
        }

        $name = $this->input->post('name');
		$stage_name = $this->input->post('stage_name');
		$gender = $this->input->post('gender');
		$image = $this->input->post('image');
		$image_id = $this->input->post('image_id');
		$dob = $this->input->post('dob');
		$expiry_date = $this->input->post('expiry_date');
		$status = $this->input->post('status');
		
        $result = $this->actor_model->edit([
            'name' => $name,
			'stage_name' => $stage_name,
			'gender' => $gender,
			'image' => $image,
			'image_id' => $image_id,
			'dob' => $dob,
			'expiry_date' => $expiry_date,
			'status' => $status,
			
        ], $id);

        if ($result)
        {
            
            return $this->redirect('/staff/actor/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Staff/ActorEdit', $this->_data);
	}

	public function view($id)
	{
        $model = $this->actor_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/staff/actor/0');
		}


        include_once __DIR__ . '/../../view_models/Actor_staff_view_view_model.php';
		$this->_data['view_model'] = new Actor_staff_view_view_model($this->actor_model);
		$this->_data['view_model']->set_heading('Actors');
        $this->_data['view_model']->set_model($model);
        return $this->render('Staff/ActorView', $this->_data);
	}




}
<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Staff_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Scenes Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Staff_scene_controller extends Staff_controller
{
    protected $_model_file = 'scenes_model';
    public $_page_name = 'Scenes';

    public function __construct()
    {
        parent::__construct();

    }

	public function index($page)
	{
        $this->load->library('pagination');
		include_once __DIR__ . '/../../view_models/Scenes_staff_list_paginate_view_model.php';
		$this->load->model('Company_user_model');
		$this->load->model('Studio_model');
        $session = $this->get_session();
        $this->_data['view_model'] = new Scenes_staff_list_paginate_view_model($this->scenes_model,$this->pagination,'/staff/scenes/0');
        $this->_data['view_model']->set_heading('Scenes');
        $this->_data['view_model']->set_title(($this->input->get('title', TRUE) != NULL) ? $this->input->get('title', TRUE) : NULL);
		$this->_data['view_model']->set_thumbnail(($this->input->get('thumbnail', TRUE) != NULL) ? $this->input->get('thumbnail', TRUE) : NULL);
		$this->_data['view_model']->set_status(($this->input->get('status', TRUE) != NULL) ? $this->input->get('status', TRUE) : NULL);
		$this->_data['view_model']->set_group_id(($this->input->get('group_id', TRUE) != NULL) ? $this->input->get('group_id', TRUE) : NULL);
		$this->_data['view_model']->set_last_name(($this->input->get('last_name', TRUE) != NULL) ? $this->input->get('last_name', TRUE) : NULL);
		$this->_data['view_model']->set_type(($this->input->get('type', TRUE) != NULL) ? $this->input->get('type', TRUE) : NULL);
		$user_companies = array_column($this->Company_user_model->get_user_companies($this->session->userdata('user_id')), 'company_id');

		$where = [
			'title' => $this->_data['view_model']->get_title(),
			'status' => $this->_data['view_model']->get_status(),
			'group_id' => $this->_data['view_model']->get_group_id(),
			'type_id' => $this->_data['view_model']->get_type(),
			'categories' => $this->_data['view_model']->get_categories(),
			'not_categories' => $this->_data['view_model']->get_not_categories(),
			'date_released' => $this->_data['view_model']->get_date_released(),
			'permitted_studios' => $this->Studio_model->get_studios_by_company_ids($user_companies)
		];

        $this->_data['view_model']->set_total_rows($this->scenes_model->count($where));
        $this->_data['view_model']->set_per_page(10);
        $this->_data['view_model']->set_page($page);
		$this->_data['view_model']->set_list($this->scenes_model->get_paginated($this->_data['view_model']->get_page(),$this->_data['view_model']->get_per_page(),$where));
		$this->_data['view_data'] = $this->_get_view_listing_data();
		return $this->render('Staff/Scenes', $this->_data);
	}

	public function add()
	{
		include_once __DIR__ . '/../../view_models/Scenes_staff_add_view_model.php';
		$this->load->model('Scene_broadcast_model');
		$this->load->model('Scene_network_model');
		$this->load->model('Scene_dvd_right_model');
		$this->load->model('Scene_vod_right_model');
		$this->load->model('Scene_viewshare_right_model');
		$this->load->model('Scene_web_right_model');
		$this->load->model('Scene_actors_model');
		$this->load->model('Scene_secondary_producers_model');
		$this->load->model('Scene_categories_model');
		$this->load->model('Scene_release_paper_work_model');
		$this->load->model('Image_model');
        $this->form_validation = $this->scenes_model->set_form_validation(
        $this->form_validation, $this->scenes_model->get_all_validation_rule());
        $this->_data['view_model'] = new Scenes_staff_add_view_model($this->scenes_model);
        $this->_data['view_model']->set_heading('Scenes');
		$this->_data['view_data'] = $this->_get_view_add_data();
		$status = 0;	
		
		if ($this->form_validation->run() === FALSE)
		{
			if(!isset($_POST['btn_save_scene']))
			{
				return $this->render('Staff/ScenesAdd', $this->_data);
			}
			//set status to pending
			$status = 2;
        }

		
		$release_paper_work = $this->input->post('release_paper_work');
		$title = $this->input->post('title') ?? ' ';
		$tubeclip_url = $this->input->post('tubeclip_url') ?? ' ';
		$dob = $this->input->post('dob');
		$xvideos_url = $this->input->post('xvideos_url') ?? ' ';
		$type = $this->input->post('type') ?? 0;
		$date_released = $this->input->post('date_released');
		$primary_producer_id = $this->input->post('primary_producer_id') ?? 0;
		$group_id = $this->input->post('group_id') ?? 0;
		$next_studio_id = $this->input->post('next_studio_id') ?? 0;
		$channel = $this->input->post('channel') ?? ' ';
		$set_id = $this->input->post('set_id') ?? ' ';
		$tss_url = $this->input->post('tss_url') ?? ' ';
		$media_type = $this->input->post('media_type') ?? ' ' ;
		$studio_id = $this->input->post('studio_id') ?? ' ';
		$is_for_comp = $this->input->post('is_for_comp') ?? 0;
		// collect relational scene data
		$broadcast = $this->input->post('broadcast');
		$networks = $this->input->post('networks');
		$view_share = $this->input->post('view_share');
		$web_rights = $this->input->post('web_rights');
		$vod_rights = $this->input->post('vod_rights');
		$dvd_rights = $this->input->post('dvd_rights');
		$scene_categories = $this->input->post('scene_categories');
		$release_paper_work = $this->input->post('release_paper_work');
		$broadcast_obj = $broadcast  === "" ? [] : json_decode($broadcast);
		//actors and producers
		$actors = $this->input->post('actors');
		$secondary_producers = $this->input->post('secondary_producers');
		
		$result = $this->scenes_model->create([
            'title' => $title,
			'tubeclip_url' => $tubeclip_url,
			'dob' => $dob,
			'xvideos_url' => $xvideos_url,
			'type' => $type,
			'primary_producer_id' => $primary_producer_id,
			'group_id' => $group_id,
			'channel' => $channel,
			'set_id' => $set_id,
			'tss_url' => $tss_url,
			'status' => $status,
			'media_type' => $media_type,
			'studio_id' => $studio_id,
			'is_for_comp' => $is_for_comp,
			'date_released' => $date_released,
			'next_studio_id' =>$next_studio_id,
			'comp_after_date' => date("Y-m-d", strtotime("+6 month"))
        ]);
		
        if ($result)
        {    
			$relational_fields = [
				'Scene_network_model' => $networks === '' ? [] : explode (',', $networks),
				'Scene_dvd_right_model' => $dvd_rights === '' ? [] : explode (',', $dvd_rights),
				'Scene_vod_right_model' => $vod_rights === '' ? [] : explode (',', $vod_rights),
				'Scene_viewshare_right_model' => $view_share === '' ? [] : explode (',', $view_share),
				'Scene_web_right_model' => $web_rights ==='' ? [] : explode (',', $web_rights)
			];
			$models  = array_keys($relational_fields);
			
			foreach($models as $model)
			{
				if(!empty($relational_fields[$model]))
				{
					$params = [];
					
					for($i = 0; $i < count($relational_fields[$model]); $i ++)
					{
						$params[] = [
							'scene_id' => $result,
							'company_id' => $relational_fields[$model][$i],
						];
					}
					
					$this->$model->batch_insert($params);
					$params = [];
				}
			}

			$broadcast_obj = $broadcast  === "" ? [] : json_decode($broadcast);
			
			if(!empty($broadcast_obj))
			{
				$params = [];
				
				foreach($broadcast_obj as $company)
				{
					$temp = [
						'scene_id' => $result,
						'company_id'=> $company->company_id,
						'right_id' => $company->right_id,
						'terms' => $company->terms
					];
					array_push($params, $temp);
					$temp = [];
				}
				$this->Scene_broadcast_model->batch_insert($params);
			}

			if(is_array($actors) && !empty($actors))
			{
				$params = [];
				
				for($i = 0; $i < count($actors); $i++ )
				{
					$params[] = [
						'scene_id' => $result,
						'actor_id' => $actors[$i]
					];		
				}

				$this->Scene_actors_model->batch_insert($params);
			}
			
			if(is_array($secondary_producers) && !empty($secondary_producers))
			{
				$params = [];
				
				for($i = 0; $i < count($secondary_producers); $i++ )
				{
					$params[] = [
						'scene_id' => $result,
						'producer_id' => $secondary_producers[$i]
					];
				}
				
				$this->Scene_secondary_producers_model->batch_insert($params);
			}

			if(is_array($scene_categories) && !empty($scene_categories))
			{
				$params = [];
				
				for($i = 0; $i < count($scene_categories); $i++ )
				{
					$params[] = [
						'scene_id' => $result,
						'category_id' => $scene_categories[$i]
					];
				}
				
				$this->Scene_categories_model->batch_insert($params);
			}
			
			$release_paper_work_array = $release_paper_work  === '' ? [] : json_decode($release_paper_work);
			
			if(!empty($release_paper_work_array) )
			{
				$params = [];
				
				for($i = 0; $i < count($release_paper_work_array); $i++ )
				{
					$image_param = [
						'caption' => '',
						'user_id' => 0,
						'width' => 0,
						'height'=> 0,
						'type' => 0,
						'url' => "/uploads{$release_paper_work_array[$i]}"	
					];
					$image_id = $this->Image_model->create($image_param);
					
					if($image_id)
					{
						$params[] = [
							'scene_id' => $result,
							'image_id' => $image_id,
							'file_url' => "/uploads{$release_paper_work_array[$i]}"
						];
					}
				}
				$this->Scene_release_paper_work_model->batch_insert($params);
			}
			return $this->redirect('/staff/scenes/0', 'refresh');
		}
		
        $this->_data['error'] = 'Error';
        return $this->render('Staff/ScenesAdd', $this->_data);
	}

	public function edit($id)
	{
		
		
		$model = $this->scenes_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/staff/scenes/0');
        }

		include_once __DIR__ . '/../../view_models/Scenes_staff_edit_view_model.php';
		$this->load->model('Scene_broadcast_model');
		$this->load->model('Scene_network_model');
		$this->load->model('Scene_dvd_right_model');
		$this->load->model('Scene_vod_right_model');
		$this->load->model('Scene_viewshare_right_model');
		$this->load->model('Scene_web_right_model');
		$this->load->model('Scene_actors_model');
		$this->load->model('Scene_secondary_producers_model');
		$this->load->model('Scene_categories_model');
		$this->load->model('Scene_release_paper_work_model');
		$this->load->model('Image_model');
		
		$this->form_validation = $this->scenes_model->set_form_validation(
        $this->form_validation, $this->scenes_model->get_all_edit_validation_rule());
        $this->_data['view_model'] = new Scenes_staff_edit_view_model($this->scenes_model);
        $this->_data['view_model']->set_model($model);
        $this->_data['view_model']->set_heading('Scenes');
		$this->_data['view_data'] = $this->_get_view_edit_data($id);

		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Staff/ScenesEdit', $this->_data);
        }

        $title = $this->input->post('title');
		$date_released = $this->input->post('date_released');
		$tubeclip_url = $this->input->post('tubeclip_url');
		$next_studio_id = $this->input->post('next_studio_id');
		$xvideos_url = $this->input->post('xvideos_url');
		$type = $this->input->post('type');
		$primary_producer_id = $this->input->post('primary_producer_id');
		$group_id = $this->input->post('group_id');
		$channel = $this->input->post('channel');
		$set_id = $this->input->post('set_id');
		$tss_url = $this->input->post('tss_url');
		$status = $this->input->post('status');
		$media_type = $this->input->post('media_type');
		$studio_id = $this->input->post('studio_id');
		$is_for_comp = $this->input->post('is_for_comp');
		// collect relational scene data
		$broadcast = $this->input->post('broadcast');
		$networks = $this->input->post('networks');
		$view_share = $this->input->post('view_share');
		$web_rights = $this->input->post('web_rights');
		$vod_rights = $this->input->post('vod_rights');
		$dvd_rights = $this->input->post('dvd_rights');
		$release_paper_work = $this->input->post('release_paper_work');
		//actors and producers
		$actors = $this->input->post('actors');
		$secondary_producers = $this->input->post('secondary_producers');
		$scene_categories = $this->input->post('scene_categories');
		$broadcast_obj = $broadcast  === "" ? [] : json_decode($broadcast);
		
        $result = $this->scenes_model->edit([
            'title' => $title,
			'date_released' => $date_released,
			'tubeclip_url' => $tubeclip_url,
			'next_studio_id' => $next_studio_id,
			'xvideos_url' => $xvideos_url,
			'type' => $type,
			'primary_producer_id' => $primary_producer_id,
			'group_id' => $group_id,
			'channel' => $channel,
			'set_id' => $set_id,
			'tss_url' => $tss_url,
			'status' => $status,
			'media_type' => $media_type,
			'studio_id' => $studio_id,
			'is_for_comp' => $is_for_comp,
			
        ], $id);

        if ($result)
        {
            
			$relational_fields = [
				"Scene_network_model" => $networks === "" ? [] : explode (",", $networks),
				"Scene_dvd_right_model" => $dvd_rights === "" ? [] : explode (",", $dvd_rights),
				"Scene_vod_right_model" => $vod_rights === "" ? [] : explode (",", $vod_rights),
				"Scene_viewshare_right_model" => $view_share === "" ? [] : explode (",",$view_share),
				"Scene_web_right_model" => $web_rights === "" ? [] : explode (",", $web_rights)
			];
			$models  = array_keys($relational_fields);
			$broadcast_obj = $broadcast  === "" ? [] : json_decode($broadcast);
			
			if(!empty($broadcast_obj))
			{
				if($this->Scene_broadcast_model->delete_by_scene($id))
				{
					$params = [];
					
					foreach($broadcast_obj as $company)
					{
						$params[] = [
							'scene_id' => $id,
							'company_id'=> $company->company_id,
							'right_id' => $company->right_id,
							'terms' => $company->terms
						];
					}
					
					$this->Scene_broadcast_model->batch_insert($params);
				}
			}
			
			foreach($models as $model)
			{
				if(!empty($relational_fields[$model]))
				{
					if($this->$model->delete_by_scene($id))
					{
						$params = [];
						
						for($i = 0; $i < count($relational_fields[$model]); $i ++)
						{
							$params[] = [
								'scene_id' => $id,
								'company_id' => $relational_fields[$model][$i],
							];
						}
						$this->$model->batch_insert($params);
						$params = [];
					}
				}
			}
			
			if(is_array($actors) && !empty($actors))
			{
				if($this->Scene_actors_model->delete_by_scene($id))
				{
					$params = [];
					for($i = 0; $i < count($actors); $i++ )
					{
						$params[] = [
							'scene_id' => $id,
							'actor_id' => $actors[$i]
						];
					}

					$this->Scene_actors_model->batch_insert($params);
				}
			}
			
			if(is_array($secondary_producers) &&!empty($secondary_producers))
			{
				if($this->Scene_secondary_producers_model->delete_by_scene($id))
				{
					$params = [];
					
					for($i = 0; $i < count($secondary_producers); $i++ )
					{
						$params[] = [
							'scene_id' => $id,
							'producer_id' => $secondary_producers[$i]
						];
					}

					$this->Scene_secondary_producers_model->batch_insert($params);
				}
			}
			
			if(is_array($scene_categories) && !empty($scene_categories))
			{
				if($this->Scene_categories_model->delete_by_scene($id))
				{
					$params = [];
					
					for($i = 0; $i < count($scene_categories); $i++ )
					{
						$params[] = [
							'scene_id' => $id,
							'category_id' => $scene_categories[$i]
						];	
					}
					
					$this->Scene_categories_model->batch_insert($params);
				}
			}

			$release_paper_work_array = $release_paper_work  === '' ? [] : json_decode($release_paper_work);
			
			if(!empty($release_paper_work_array) )
			{
				$params = [];
				
				for($i = 0; $i < count($release_paper_work_array); $i++ )
				{
					$image_param = [
						'caption' => '',
						'user_id' => 0,
						'width' => 0,
						'height'=> 0,
						'type' => 0,
						'url' => "/uploads{$release_paper_work_array[$i]}"	
					];
					$image_id = $this->Image_model->create($image_param);
					
					if($image_id)
					{
						$params[] = [
							'scene_id' => $id,
							'image_id' => $image_id,
							'file_url' => "/uploads{$release_paper_work_array[$i]}"
						];	
					}
				}

			}
            return $this->redirect('/staff/scenes/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Staff/ScenesEdit', $this->_data);
	}

	public function view($id)
	{
        $model = $this->scenes_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/staff/scenes/0');
			
		}

        include_once __DIR__ . '/../../view_models/Scenes_staff_view_view_model.php';
		$this->_data['view_model'] = new Scenes_staff_view_view_model($this->scenes_model);
		$this->_data['view_model']->set_heading('Scenes');
        $this->_data['view_model']->set_model($model);
        return $this->render('Staff/ScenesView', $this->_data);
	}


	private function _get_view_add_data()
	{	
		$this->load->model('Company_model', 'companies');
		$this->load->model('Broadcast_company_model', 'broadcast');
		$this->load->model('Viewshare_site_model', 'view_share');
		$this->load->model('Networks_model', 'networks');
		$this->load->model('Group_model', 'groups');
		$this->load->model('Producer_model', 'producers');
		$this->load->model('Studio_model', 'studios');
		$this->load->model('Types_model', 'types');
		$this->load->model('Actor_model', 'actors');
		$this->load->model('Category_model', 'categories');
		$view_data = [
			'companies' => $this->companies->get_all(),
			'view_share' => $this->view_share->get_all(),
			'networks'=> $this->networks->get_all(),
			'broadcasters' => $this->broadcast->get_all(),
			'groups' => $this->groups->get_all(),
			'producers' => $this->producers->get_all(),
			'studios' => $this->studios->get_all(),
			'types' => $this->types->get_all(),
			'actors' => $this->actors->get_all(),
			'categories' => $this->categories->get_all()
		];
		return $view_data;
	}

	private function _get_view_listing_data()
	{
		$this->load->model('Studio_model', 'studios');
		$this->load->model('Group_model', 'groups');
		$this->load->model('Types_model', 'types');
		$this->load->model('Category_model', 'categories');
		$view_data = [
			'groups' => $this->groups->get_all(),
			'studios' => $this->studios->get_all(),
			'types' => $this->types->get_all(),
			'categories' => $this->categories->get_all()
		];
		return $view_data;
	}

	private function _get_view_edit_data($id = 0)
	{
		// pre fetch for drop downs and modals
		$this->load->model('Company_model', 'companies');
		$this->load->model('Broadcast_company_model', 'broadcast');
		$this->load->model('Viewshare_site_model', 'view_share');
		$this->load->model('Networks_model', 'networks');
		$this->load->model('Group_model', 'groups');
		$this->load->model('Producer_model', 'producers');
		$this->load->model('Studio_model', 'studios');
		$this->load->model('Types_model', 'types');
		$this->load->model('Actor_model', 'actors');
		$this->load->model('Category_model', 'categories');
		// used to pre populate table rows
		$this->load->model('Scene_actors_model', 'scene_actor');
		$this->load->model('Scene_broadcast_model', 'scene_broadcast');
		$this->load->model('Scene_categories_model', 'scene_cat');
		$this->load->model('Scene_dvd_right_model', 'dvd_right');
		$this->load->model('Scene_network_model', 'scene_networks');
		$this->load->model('Scene_secondary_producers_model', 'secondary_producer');
		$this->load->model('Scene_viewshare_right_model', 'scene_view_share');
		$this->load->model('Scene_vod_right_model', 'vod_rights');
		$this->load->model('Scene_web_right_model', 'web_rights');

		$view_data = [
			'companies' => $this->companies->get_all(),
			'view_share' => $this->view_share->get_all(),
			'networks'=> $this->networks->get_all(),
			'broadcasters' => $this->broadcast->get_all(),
			'groups' => $this->groups->get_all(),
			'producers' => $this->producers->get_all(),
			'studios' => $this->studios->get_all(),
			'types' => $this->types->get_all(),
			'actors' => $this->actors->get_all(),
			'categories' => $this->categories->get_all(),
			'scene_actors' => $this->scene_actor->get_by_scene($id),
			'scene_broadcast_companies' => $this->scene_broadcast->get_by_scene($id),
			'scene_categories' =>  $this->scene_cat->get_by_scene($id),
			'scene_dvd_rights' => $this->dvd_right->get_by_scene($id),
			'scene_networks' => $this->scene_networks->get_by_scene($id),
			'scene_producers' => $this->secondary_producer->get_by_scene($id),
			'scene_view_share' => $this->scene_view_share->get_by_scene($id),
			'scene_vod_rights' => $this->vod_rights->get_by_scene($id),
			'scene_web_rights' => $this->web_rights->get_by_scene($id)
		];
		return $view_data;
	}
}
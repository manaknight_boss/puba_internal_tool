<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Staff_controller.php';

/**
 * Staff Dashboard Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Staff_dashboard_controller extends Staff_controller
{
    public $_page_name = 'Dashboard';

    public function __construct()
    {
        parent::__construct();
    }

    public function index ()
    {
        return $this->render('Staff/Dashboard', $this->_data);
    }
}
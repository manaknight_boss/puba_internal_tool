<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Staff_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Broadcastcompanies Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Staff_broadcastcompany_controller extends Staff_controller
{
    protected $_model_file = 'broadcast_company_model';
    public $_page_name = 'Broadcast Companies';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('staff_operation_model');

    }

	public function index($page)
	{
        $this->load->library('pagination');
        include_once __DIR__ . '/../../view_models/Broadcastcompanies_staff_list_paginate_view_model.php';
        $session = $this->get_session();
        $this->_data['view_model'] = new Broadcastcompanies_staff_list_paginate_view_model(
            $this->broadcast_company_model,
            $this->pagination,
            '/staff/broadcastcompany/0');
        $this->_data['view_model']->set_heading('Broadcast Companies');
        $this->_data['view_model']->set_name(($this->input->get('name', TRUE) != NULL) ? $this->input->get('name', TRUE) : NULL);
		$this->_data['view_model']->set_status(($this->input->get('status', TRUE) != NULL) ? $this->input->get('status', TRUE) : NULL);
		
        $where = [
            'name' => $this->_data['view_model']->get_name(),
			'status' => $this->_data['view_model']->get_status(),
			
        ];

        $this->_data['view_model']->set_total_rows($this->broadcast_company_model->count($where));

        $this->_data['view_model']->set_per_page(10);
        $this->_data['view_model']->set_page($page);
		$this->_data['view_model']->set_list($this->broadcast_company_model->get_paginated(
            $this->_data['view_model']->get_page(),
            $this->_data['view_model']->get_per_page(),
            $where));
        return $this->render('Staff/Broadcastcompanies', $this->_data);
	}

	public function add()
	{
        include_once __DIR__ . '/../../view_models/Broadcastcompanies_staff_add_view_model.php';
        $this->form_validation = $this->broadcast_company_model->set_form_validation(
        $this->form_validation, $this->broadcast_company_model->get_all_validation_rule());
        $this->_data['view_model'] = new Broadcastcompanies_staff_add_view_model($this->broadcast_company_model);
        $this->_data['view_model']->set_heading('Broadcast Companies');

		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Staff/BroadcastcompaniesAdd', $this->_data);
        }

        $name = $this->input->post('name');
		
        $result = $this->broadcast_company_model->create([
            'name' => $name,
			
        ]);

        if ($result)
        {
            $this->success('Saved');
			$this->staff_operation_model->log_activity('add broadcast company', $this->broadcast_company_model->get($result), $this->get_session()['user_id']);
            return $this->redirect('/staff/broadcastcompany/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Staff/BroadcastcompaniesAdd', $this->_data);
	}

	public function edit($id)
	{
        $model = $this->broadcast_company_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/staff/broadcastcompany/0');
        }

        include_once __DIR__ . '/../../view_models/Broadcastcompanies_staff_edit_view_model.php';
        $this->form_validation = $this->broadcast_company_model->set_form_validation(
        $this->form_validation, $this->broadcast_company_model->get_all_edit_validation_rule());
        $this->_data['view_model'] = new Broadcastcompanies_staff_edit_view_model($this->broadcast_company_model);
        $this->_data['view_model']->set_model($model);
        $this->_data['view_model']->set_heading('Broadcast Companies');
        
		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Staff/BroadcastcompaniesEdit', $this->_data);
        }

        $name = $this->input->post('name');
		$status = $this->input->post('status');
		
        $result = $this->broadcast_company_model->edit([
            'name' => $name,
			'status' => $status,
			
        ], $id);

        if ($result)
        {
            
            return $this->redirect('/staff/broadcastcompany/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Staff/BroadcastcompaniesEdit', $this->_data);
	}






}
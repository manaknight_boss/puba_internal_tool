<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Staff_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Viewsharesites Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Staff_viewsharesite_controller extends Staff_controller
{
    protected $_model_file = 'viewshare_site_model';
    public $_page_name = 'Viewshare Sites';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('staff_operation_model');

    }

	public function index($page)
	{
        $this->load->library('pagination');
        include_once __DIR__ . '/../../view_models/Viewsharesites_staff_list_paginate_view_model.php';
        $session = $this->get_session();
        $this->_data['view_model'] = new Viewsharesites_staff_list_paginate_view_model(
            $this->viewshare_site_model,
            $this->pagination,
            '/staff/viewsharesite/0');
        $this->_data['view_model']->set_heading('Viewshare Sites');
        
        $where = [
            
        ];

        $this->_data['view_model']->set_total_rows($this->viewshare_site_model->count($where));

        $this->_data['view_model']->set_per_page(10);
        $this->_data['view_model']->set_page($page);
		$this->_data['view_model']->set_list($this->viewshare_site_model->get_paginated(
            $this->_data['view_model']->get_page(),
            $this->_data['view_model']->get_per_page(),
            $where));
        return $this->render('Staff/Viewsharesites', $this->_data);
	}

	public function add()
	{
        include_once __DIR__ . '/../../view_models/Viewsharesites_staff_add_view_model.php';
        $this->form_validation = $this->viewshare_site_model->set_form_validation(
        $this->form_validation, $this->viewshare_site_model->get_all_validation_rule());
        $this->_data['view_model'] = new Viewsharesites_staff_add_view_model($this->viewshare_site_model);
        $this->_data['view_model']->set_heading('Viewshare Sites');

		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Staff/ViewsharesitesAdd', $this->_data);
        }

        $name = $this->input->post('name');
		
        $result = $this->viewshare_site_model->create([
            'name' => $name,
			
        ]);

        if ($result)
        {
            $this->success('Saved');
			$this->staff_operation_model->log_activity('add viewshare site', $this->viewshare_site_model->get($result), $this->get_session()['user_id']);
            return $this->redirect('/staff/viewsharesite/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Staff/ViewsharesitesAdd', $this->_data);
	}

	public function edit($id)
	{
        $model = $this->viewshare_site_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/staff/viewsharesite/0');
        }

        include_once __DIR__ . '/../../view_models/Viewsharesites_staff_edit_view_model.php';
        $this->form_validation = $this->viewshare_site_model->set_form_validation(
        $this->form_validation, $this->viewshare_site_model->get_all_edit_validation_rule());
        $this->_data['view_model'] = new Viewsharesites_staff_edit_view_model($this->viewshare_site_model);
        $this->_data['view_model']->set_model($model);
        $this->_data['view_model']->set_heading('Viewshare Sites');
        
		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Staff/ViewsharesitesEdit', $this->_data);
        }

        $name = $this->input->post('name');
		
        $result = $this->viewshare_site_model->edit([
            'name' => $name,
			
        ], $id);

        if ($result)
        {
            
            return $this->redirect('/staff/viewsharesite/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Staff/ViewsharesitesEdit', $this->_data);
	}






}
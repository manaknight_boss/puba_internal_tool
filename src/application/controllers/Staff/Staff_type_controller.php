<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Staff_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Types Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Staff_type_controller extends Staff_controller
{
    protected $_model_file = 'types_model';
    public $_page_name = 'Types';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('staff_operation_model');

    }

	public function index($page)
	{
        $this->load->library('pagination');
        include_once __DIR__ . '/../../view_models/Types_staff_list_paginate_view_model.php';
        $session = $this->get_session();
        $this->_data['view_model'] = new Types_staff_list_paginate_view_model(
            $this->types_model,
            $this->pagination,
            '/staff/type/0');
        $this->_data['view_model']->set_heading('Types');
        $this->_data['view_model']->set_name(($this->input->get('name', TRUE) != NULL) ? $this->input->get('name', TRUE) : NULL);
		$this->_data['view_model']->set_status(($this->input->get('status', TRUE) != NULL) ? $this->input->get('status', TRUE) : NULL);
		
        $where = [
            'name' => $this->_data['view_model']->get_name(),
			'status' => $this->_data['view_model']->get_status(),
			
        ];

        $this->_data['view_model']->set_total_rows($this->types_model->count($where));

        $this->_data['view_model']->set_per_page(10);
        $this->_data['view_model']->set_page($page);
		$this->_data['view_model']->set_list($this->types_model->get_paginated(
            $this->_data['view_model']->get_page(),
            $this->_data['view_model']->get_per_page(),
            $where));
        return $this->render('Staff/Types', $this->_data);
	}

	public function add()
	{
        include_once __DIR__ . '/../../view_models/Types_staff_add_view_model.php';
        $this->form_validation = $this->types_model->set_form_validation(
        $this->form_validation, $this->types_model->get_all_validation_rule());
        $this->_data['view_model'] = new Types_staff_add_view_model($this->types_model);
        $this->_data['view_model']->set_heading('Types');

		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Staff/TypesAdd', $this->_data);
        }

        $name = $this->input->post('name');
		
        $result = $this->types_model->create([
            'name' => $name,
			
        ]);

        if ($result)
        {
            $this->success('Saved');
			$this->staff_operation_model->log_activity('add type', $this->type_model->get($result), $this->get_session()['user_id']);
            return $this->redirect('/staff/type/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Staff/TypesAdd', $this->_data);
	}

	public function edit($id)
	{
        $model = $this->types_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/staff/type/0');
        }

        include_once __DIR__ . '/../../view_models/Types_staff_edit_view_model.php';
        $this->form_validation = $this->types_model->set_form_validation(
        $this->form_validation, $this->types_model->get_all_edit_validation_rule());
        $this->_data['view_model'] = new Types_staff_edit_view_model($this->types_model);
        $this->_data['view_model']->set_model($model);
        $this->_data['view_model']->set_heading('Types');
        
		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Staff/TypesEdit', $this->_data);
        }

        $name = $this->input->post('name');
		$status = $this->input->post('status');
		
        $result = $this->types_model->edit([
            'name' => $name,
			'status' => $status,
			
        ], $id);

        if ($result)
        {
            
            return $this->redirect('/staff/type/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Staff/TypesEdit', $this->_data);
	}






}
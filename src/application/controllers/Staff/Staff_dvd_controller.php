<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Staff_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Dvds Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Staff_dvd_controller extends Staff_controller
{
    protected $_model_file = 'dvds_model';
    public $_page_name = 'dvds';

    public function __construct()
    {
        parent::__construct();

    }

	public function index($page)
	{
        $this->load->library('pagination');
        include_once __DIR__ . '/../../view_models/Dvds_staff_list_paginate_view_model.php';
		$this->load->model('Company_user_model');
		$this->load->model('Studio_model');
		$session = $this->get_session();
        $this->_data['view_model'] = new Dvds_staff_list_paginate_view_model($this->dvds_model,$this->pagination,'/staff/dvds/0');
        $this->_data['view_model']->set_heading('dvds');
        $this->_data['view_model']->set_title(($this->input->get('title', TRUE) != NULL) ? $this->input->get('title', TRUE) : NULL);
		$this->_data['view_model']->set_status(($this->input->get('status', TRUE) != NULL) ? $this->input->get('status', TRUE) : NULL);
		$this->_data['view_model']->set_studio_id(($this->input->get('studio_id', TRUE) != NULL) ? $this->input->get('studio_id', TRUE) : NULL);
		$this->_data['view_model']->set_company_id(($this->input->get('company_id', TRUE) != NULL) ? $this->input->get('company_id', TRUE) : NULL);
		$this->_data['view_model']->set_set_id(($this->input->get('set_ids', TRUE) != NULL) ? $this->input->get('set_ids', TRUE) : NULL);
		$this->_data['view_model']->set_id(($this->input->get('id', TRUE) != NULL) ? $this->input->get('id', TRUE) : NULL);
		$this->_data['view_model']->set_sort(($this->input->get('sort', TRUE) != NULL) ? $this->input->get('sort', TRUE) : NULL);
		$this->_data['view_model']->set_release_start_date(($this->input->get('release_start_date', TRUE) != NULL) ? $this->input->get('release_start_date', TRUE) : NULL);
		$this->_data['view_model']->set_release_end_date(($this->input->get('release_end_date', TRUE) != NULL) ? $this->input->get('release_end_date', TRUE) : NULL);
		//$where = [];
		
		$user_companies = array_column($this->Company_user_model->get_user_companies($this->session->userdata('user_id')), 'company_id');
		
		$where = [
			'title' => $this->_data['view_model']->get_title(),
			'status' => $this->_data['view_model']->get_status(),
			'sort' =>  $this->_data['view_model']->get_sort() ?? '', 
			'id' =>  $this->_data['view_model']->get_id() ?? 0, 
			'studio_id' => $this->_data['view_model']->get_studio_id(),
			'release_date' => $this->_data['view_model']->get_release_date(),
			'company_id' => $this->_data['view_model']->get_company_id(),
			'set_id' => $this->_data['view_model']->get_set_id(),
			'release_start_date' => $this->_data['view_model']->get_release_start_date() ?? '',
			'release_end_date' => $this->_data['view_model']->get_release_end_date() ?? '',
			'permitted_studios' => $this->Studio_model->get_studios_by_company_ids($user_companies)
		];	

        $this->_data['view_model']->set_total_rows($this->dvds_model->count($where));
		$this->_data['view_model']->set_per_page(10);
        $this->_data['view_model']->set_page($page);
		$this->_data['view_model']->set_list($this->dvds_model->get_paginated($this->_data['view_model']->get_page(),$this->_data['view_model']->get_per_page(),$where));
		$this->_data['view_data'] =$this->_get_view_filter_data();
		return $this->render('Staff/Dvds', $this->_data);
	}

	public function update_dvd()
	{
		$id = $this->uri->segment(4) ?? 0;
		
		if(isset($_POST['btn_update_dvd']))
		{
		   $this->load->model('Scenes_model','dvd_update_scenes');
		   $this->load->model('Dvds_model','dvds');
		   $title = $this->input->post('title');
		   $release_date = $this->input->post('release_date');
		   $is_for_comp = $this->input->post('is_for_comp');
		   $dvd_cover_scene = $this->input->post('dvd_cover_scene');
		   $updated_scenes = $this->input->post('updated_scenes');
		   
		   if(empty($title) || empty($release_date))
		   {
			  redirect("staff/dvds/view/{$id}?error=true");
		   }
		   
		   $result = $this->dvds->edit([
            	'title' => $title,
				'release_date' => $release_date,
				'is_for_comp' => $is_for_comp,
			], $id);
			
			if($result)
			{
				if($updated_scenes !== '')
				{
					$updated_scenes_obj = json_decode($updated_scenes);
					foreach($updated_scenes_obj as $scene)
					{
						$param = [];
						if($scene->comp_after_studio !== 0)
						{
							$param = [
								'next_studio_id' =>  $scene->comp_after_studio,
								'comp_after_date' => $scene->comp_after_date
							];
						}
						else
						{
							$param = [
								'comp_after_date' => $scene->comp_after_date
							];
						}
						var_dump($param);
						$this->dvd_update_scenes->update_scene($scene->scene_id,$param);	
					}
				}
			}
			
			redirect("staff/dvds/view/{$id}?error=false");
			exit();
		}
	}

	public function add()
	{
		include_once __DIR__ . '/../../view_models/Dvds_staff_add_view_model.php';
		$this->load->model('Dvd_categories_model');
		$this->load->model('Dvd_scenes_model');
        $this->form_validation = $this->dvds_model->set_form_validation(
        $this->form_validation, $this->dvds_model->get_all_validation_rule());
        $this->_data['view_model'] = new Dvds_staff_add_view_model($this->dvds_model);
		$this->_data['view_model']->set_heading('Dvds');
		$this->_data['view_data'] = $this->_get_view_data();

		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Staff/DvdsAdd', $this->_data);
		}
		
		//relational fields 
		$dvd_categories = $this->input->post('dvd_categories');
		$dvd_scenes = $this->input->post('dvd_scenes');
		$dvd_cover_scene = $this->input->post('dvd_cover_scene');
		
        $title = $this->input->post('title');
		$status = $this->input->post('status');
		$studio_id = $this->input->post('studio_id');
		$release_date = $this->input->post('release_date');
		$company_id = $this->input->post('company_id');
		$type_id = $this->input->post('type_id');
		$filter_after = $this->input->post('filter_after');
		$start_production_date = $this->input->post('start_production_date');
		$end_production_date = $this->input->post('end_production_date');
		$cover_image = $this->input->post('cover_image');
		$cover_image_id = $this->input->post('cover_image_id');
		
        $result = $this->dvds_model->create([
            'title' => $title,
			'status' => $status,
			'studio_id' => $studio_id,
			'release_date' => $release_date,
			'company_id' => $company_id,
			'type_id' => $type_id,
			'filter_after' => $filter_after,
			'start_production_date' => $start_production_date,
			'end_production_date' => $end_production_date,
			'cover_image' => $cover_image,
			'cover_image_id' => $cover_image_id,
        ]);

        if ($result)
        {
            if(!empty($dvd_categories))
			{
				$param = [];
				
				for($i = 0; $i < count($dvd_categories); $i ++)
				{
					$param[] = [
						'dvd_id' => $result,
						'category_id' => $dvd_categories[$i]
					];
				}
				
				$this->Dvd_categories_model->batch_insert($param);
			}
	
			$dvd_scenes_array  = ($dvd_scenes === ''  ? [] : explode (',', $dvd_scenes));
			
			if(!empty($dvd_scenes_array))
			{
				$param = [];
				
				for($i = 0; $i < count($dvd_scenes_array); $i ++ )
				{
					$param[] = [
						'dvd_id' => $result,
						'scene_id' => $dvd_scenes_array[$i],
						'queue_position' => $i + 1,
						'is_cover_scene' => ($dvd_scenes_array[$i] === $dvd_cover_scene ? 1 : 0)
					];
				}
				
				$this->Dvd_scenes_model->batch_insert($param);
			}
			
			return $this->redirect('/staff/dvds/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Staff/DvdsAdd', $this->_data);
	}

	public function edit($id)
	{
        $model = $this->dvds_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/staff/dvds/0');
        }

		include_once __DIR__ . '/../../view_models/Dvds_staff_edit_view_model.php';
		$this->load->model('Dvd_categories_model');
		$this->load->model('Dvd_scenes_model');
        $this->form_validation = $this->dvds_model->set_form_validation(
        $this->form_validation, $this->dvds_model->get_all_edit_validation_rule());
        $this->_data['view_model'] = new Dvds_staff_edit_view_model($this->dvds_model);
        $this->_data['view_model']->set_model($model);
		$this->_data['view_model']->set_heading('Dvds');
		$this->_data['view_data'] = $this->_get_view_edit_data($id);
        
		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Staff/DvdsEdit', $this->_data);
        }

        $title = $this->input->post('title');
		$studio_id = $this->input->post('studio_id');
		$filter_after = $this->input->post('filter_after');
		$status = $this->input->post('status');
		$release_date = $this->input->post('release_date');
		$company_id = $this->input->post('company_id');
		$type_id = $this->input->post('type_id');
		$start_production_date = $this->input->post('start_production_date');
		$end_production_date = $this->input->post('end_production_date');
		$cover_image = $this->input->post('cover_image');
		$cover_image_id = $this->input->post('cover_image_id');
		$is_for_comp = $this->input->post('is_for_comp');

		$dvd_categories = $this->input->post('dvd_categories');
		$dvd_scenes = $this->input->post('dvd_scenes');
		$dvd_cover_scene = $this->input->post('dvd_cover_scene');
		
        $result = $this->dvds_model->edit([
            'title' => $title,
			'studio_id' => $studio_id,
			'filter_after' => $filter_after,
			'status' => $status,
			'release_date' => $release_date,
			'company_id' => $company_id,
			'type_id' => $type_id,
			'start_production_date' => $start_production_date,
			'end_production_date' => $end_production_date,
			'cover_image' => $cover_image,
			'cover_image_id' => $cover_image_id,
			'is_for_comp' => $is_for_comp,
			
        ], $id);

        if ($result)
        {
            if(!empty($dvd_categories))
			{
				// first delete all the categories in database and reinsert as new categories
				// @todo find a more efficient way to achieve batch update 
				if($this->Dvd_categories_model->delete_all_dvd_categories($id) === TRUE)
				{
					$param = [];
					
					for($i = 0; $i < count($dvd_categories); $i ++)
					{
						$param[] = [
							'dvd_id' => $id,
							'category_id' => $dvd_categories[$i]
						];
					}
					
					$this->Dvd_categories_model->batch_insert($param);
				}
			}

			$dvd_scenes_array  = ($dvd_scenes === ''  ? [] : explode (',', $dvd_scenes));
			
			if(!empty($dvd_scenes_array))
			{
				$this->load->model('Dvd_scenes_model');
				
				if($this->Dvd_scenes_model->delete_all_dvd_scenes($id) === TRUE)
				{
					$param = [];
					
					for($i = 0; $i < count($dvd_scenes_array); $i ++ )
					{
						$param[] = [
							'dvd_id' => $id,
							'scene_id' => $dvd_scenes_array[$i],
							'queue_position' => $i + 1,
							'is_cover_scene' => ($dvd_scenes_array[$i] === $dvd_cover_scene ? 1 : 0)
						];
					}
					
					$this->Dvd_scenes_model->batch_insert($param);
				}
			} 
			
			return $this->redirect('/staff/dvds/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Staff/DvdsEdit', $this->_data);
	}

	public function view($id)
	{
        $model = $this->dvds_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/staff/dvds/0');
		}


        include_once __DIR__ . '/../../view_models/Dvds_staff_view_view_model.php';
		$this->_data['view_model'] = new Dvds_staff_view_view_model($this->dvds_model);
		$this->_data['view_model']->set_heading('Dvds');
        $this->_data['view_model']->set_model($model);
        return $this->render('Staff/DvdsView', $this->_data);
	}

	public function download_compliance_statement()
	{
		$dvd_id = (int) $this->uri->segment(4) ?? 0;
		$this->load->library('pdf');
		$this->load->model('Dvds_model', 'dvd');
		$data['dvd_details'] = $this->dvd->get_dvd_details($dvd_id);
		$html = $this->load->view('pdfs/Compliance_statement', $data, true);
        $this->pdf->create_pdf($html, $data['dvd_details']['title'].'_compliance_statement', true);
		exit();
	}

	public function download_cast()
	{
		$dvd_id = (int) $this->uri->segment(4) ?? 0;
		$this->load->library('pdf');
		$this->load->model('Dvd_scenes_model', 'dvd_scenes');
		$this->load->model('Scene_actors_model', 'actors');
		$this->load->model('Dvds_model', 'dvd');
		$data['dvd_details'] = $this->dvd->get_dvd_details($dvd_id);
		$data['dvd_scenes'] = [];
		$dvd_scenes_array = $this->dvd_scenes->get_dvd_scenes($dvd_id);
		foreach($dvd_scenes_array as $scene)
		{
			$temp = [
				'scene'  => $scene,
				'scene_actors' => $this->actors->get_scene_actors($scene->scene_id)
			];
			array_push($data['dvd_scenes'],$temp);	
		}
		$html = $this->load->view('pdfs/Dvd_cast', $data, true);
        $this->pdf->create_pdf($html, $data['dvd_details']['title'].'cast', true);
		exit();
	}

	public function download_music_release()
	{
		$dvd_id = (int) $this->uri->segment(4) ?? 0;
		$this->load->library('pdf');
		$this->load->model('Dvd_scenes_model', 'dvd_scenes');
		$this->load->model('Studio_model', 'studios');
		$this->load->model('Dvds_model', 'dvd');
		$data['dvd_details'] = $this->dvd->get_dvd_details($dvd_id);
		$data['dvd_scenes'] = [];
		$dvd_scenes_array = $this->dvd_scenes->get_dvd_scenes($dvd_id);
		foreach($dvd_scenes_array as $scene)
		{
			$temp = [
				'scene'  => $scene,
				'scene_studio' => $this->studios->get_studio_company($scene->studio_id)
			];
			array_push($data['dvd_scenes'],$temp);	
		}
		$html =  $this->load->view('pdfs/Music_release', $data, true);
		$this->pdf->create_pdf($html, $data['dvd_details']['title'].'music_release', true);
		exit();
	}

	public function filter_dvd_scenes()
	{
		$category = (int) $this->input->get("category_id") ?? 0;
		$filter_after = (int) $this->input->get("filter_after") ?? 0;
		$this->load->model('Scenes_model', 'scenes');
		$response = $this->scenes->filter_scenes($category, $filter_after);
		echo json_encode($response);
		exit();
	}

	public function filter_company_studios()
	{
		$company_id = (int) $this->input->get('company_id') ?? 0;
		$this->load->model('Studio_model');
		$response = $this->Studio_model->get_company_studios_list($company_id); 
		echo json_encode($response);
		exit();
	}

	public function remove_dvd_scene()
	{
		$dvd_scene_id = (int) $this->input->get("dvd_scene_id") ?? 0;
		$this->load->model('Dvd_scenes_model', 'dvd_scenes');
		if($this->dvd_scenes->delete_dvd_scene($dvd_scene_id))
		{
			$response = ['error' => false, 'message' => 'removed scene from dvd '];
		}
		else
		{
			$response = ['error' => true, 'message' => 'error removing scene from dvd '];	
		}
		echo json_encode($response);
		exit;
	}

	private function _get_view_data()
	{	
		$this->load->model('Company_user_model', 'companies');
		$this->load->model('Studio_model', 'studios');
		$this->load->model('Types_model', 'types');
		$this->load->model('Category_model', 'categories');
		$this->load->model('Scenes_model', 'scenes');
		$view_data = [
			'companies' => $this->companies->get_user_companies($this->session->userdata('user_id')),
			'studios' => $this->studios->get_all(),
			'types' => $this->types->get_all(),
			'categories' => $this->categories->get_scene_categories_count(),
			'scenes' => $this->scenes->get_all()
		];
		return $view_data;
	}

	private function _get_view_edit_data($id= 0)
	{
		$this->load->model('Company_user_model', 'companies');
		$this->load->model('Studio_model', 'studios');
		$this->load->model('Types_model', 'types');
		$this->load->model('Category_model', 'categories');
		$this->load->model('Dvd_scenes_model', 'dvd_scenes');
		$this->load->model('Dvd_categories_model', 'dvd_category');
		$view_data = [
			'companies' => 	$this->companies->get_user_companies($this->session->userdata('user_id')),
			'studios' => $this->studios->get_all(),
			'types' => $this->types->get_all(),
			'categories' => $this->categories->get_scene_categories_count(),
			'dvd_category'=> $this->dvd_category->get_by_dvd_id($id),
			'dvd_scenes' => $this->dvd_scenes->get_dvd_scenes($id),
			'dvd_cover_scene' => $this->dvd_scenes->get_dvd_cover_scene($id)
		];
		return $view_data;
	}


	private function _get_view_filter_data()
	{
		$this->load->model('Company_user_model', 'companies');
		$this->load->model('Studio_model', 'studios');
		$this->load->model('Scenes_model', 'scenes');
		$view_data = [
			'set_ids' => $this->scenes->get_unique_scene_ids(),
			'studios' =>  $this->studios->get_all(),
			'companies' => 	$this->companies->get_user_companies($this->session->userdata('user_id'))
		];
		return $view_data;

	}

	private function _get_view_list_item_data($id= 0, $error= FALSE)
	{
		$this->load->model('Dvd_scenes_model', 'dvd_scenes');
		$this->load->model('Studio_model', 'studios');
		$view_data = [
			'dvd_scenes' => $this->dvd_scenes->get_dvd_scenes($id),
			'studios' => $this->studios->get_all(),
			'dvd_cover_scene' => $this->dvd_scenes->get_dvd_cover_scene($id),
			'error' =>  $error
		];
		return $view_data;
	}



}
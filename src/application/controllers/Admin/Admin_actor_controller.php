<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Admin_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Actor Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Admin_actor_controller extends Admin_controller
{
    protected $_model_file = 'actor_model';
    public $_page_name = 'Actors';

    public function __construct()
    {
				parent::__construct();
				$this->load->model('admin_operation_model');

    }

	public function index($page)
	{
		$this->load->library('pagination');
		include_once __DIR__ . '/../../view_models/Actor_admin_list_paginate_view_model.php';
		$session = $this->get_session();
		$this->_data['view_model'] = new Actor_admin_list_paginate_view_model(
				$this->actor_model,
				$this->pagination,
				'/admin/actor/0');
		$this->_data['view_model']->set_heading('Actors');
		$this->_data['view_model']->set_name(($this->input->get('name', TRUE) != NULL) ? $this->input->get('name', TRUE) : NULL);
		$this->_data['view_model']->set_stage_name(($this->input->get('stage_name', TRUE) != NULL) ? $this->input->get('stage_name', TRUE) : NULL);
		$this->_data['view_model']->set_gender(($this->input->get('gender', TRUE) != NULL) ? $this->input->get('gender', TRUE) : NULL);
		$this->_data['view_model']->set_dob(($this->input->get('dob', TRUE) != NULL) ? $this->input->get('dob', TRUE) : NULL);
		$this->_data['view_model']->set_expiry_date(($this->input->get('expiry_date', TRUE) != NULL) ? $this->input->get('expiry_date', TRUE) : NULL);

		$where = [
			'name' => $this->_data['view_model']->get_name(),
			'stage_name' => $this->_data['view_model']->get_stage_name(),
			'gender' => $this->_data['view_model']->get_gender(),
			'dob' => $this->_data['view_model']->get_dob(),
			'expiry_date' => $this->_data['view_model']->get_expiry_date(),
		];
		$this->load->model('actor_additional_name_model');
		$results = $this->actor_model->get_paginated($this->_data['view_model']->get_page(),$this->_data['view_model']->get_per_page(),$where);

		foreach ($results as $key => $value)
		{
			$name_list = $this->actor_additional_name_model->get_all(['actor_id' => $value->id]);
			$names = [];

			foreach ($name_list as $name_key => $person)
			{
				$names[] = $person->additional_name;
			}

			$results[$key]->additional_name = implode('<br/>', $names);
		}

		$this->_data['view_model']->set_total_rows($this->actor_model->count($where));

		$this->_data['view_model']->set_per_page(10);
		$this->_data['view_model']->set_page($page);
		$this->_data['view_model']->set_list($results);
		return $this->render('Admin/Actor', $this->_data);
	}

	public function add()
	{
		include_once __DIR__ . '/../../view_models/Actor_admin_add_view_model.php';
		$this->load->model('Actor_additional_name_model');
		$this->form_validation = $this->actor_model->set_form_validation(
		$this->form_validation, $this->actor_model->get_all_validation_rule());
		$this->_data['view_model'] = new Actor_admin_add_view_model($this->actor_model);
		$this->_data['view_model']->set_heading('Actor');

		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/ActorAdd', $this->_data);
		}

		$name = $this->input->post('name');
		$stage_name = $this->input->post('stage_name');
		$gender = $this->input->post('gender');
		$image = $this->input->post('image');
		$image_id = $this->input->post('image_id');
		$dob = $this->input->post('dob');
		$expiry_date = $this->input->post('expiry_date');
		$additional_names = $this->input->post('additional_names');

		$result = $this->actor_model->create([
			'name' => $name,
			'stage_name' => $stage_name,
			'gender' => $gender,
			'image' => $image,
			'image_id' => $image_id,
			'dob' => $dob,
			'expiry_date' => $expiry_date,
		]);

		if ($result)
		{
			$additional_names_array  = ($additional_names === ''  ? [] : explode (',', $additional_names));
			$payload = [
				'name' => $name,
				'stage_name' => $stage_name,
				'gender' => $gender,
				'image' => $image,
				'image_id' => $image_id,
				'dob' => $dob,
				'expiry_date' => $expiry_date
			];
			if(!empty($additional_names_array))
			{
				$param = [];

				for($i = 0; $i < count($additional_names_array); $i ++ )
				{
					$param[] = [
						'actor_id' => $result,
						'additional_name' => $additional_names_array[$i],
					];
				}

				$this->Actor_additional_name_model->batch_insert($param);
				$payload['additional_name'] = $additional_names_array;
			}
			$this->success('Saved');
			$this->admin_operation_model->log_activity('add actor', $payload, $this->get_session()['user_id']);
			return $this->redirect('/admin/actor/0', 'refresh');
		}

		$this->_data['error'] = 'Error';
		return $this->render('Admin/ActorAdd', $this->_data);
	}

	public function edit($id)
	{
    $model = $this->actor_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/admin/actor/0');
		}

		include_once __DIR__ . '/../../view_models/Actor_admin_edit_view_model.php';
		$this->load->model('Actor_additional_name_model');
		$this->form_validation = $this->actor_model->set_form_validation(
		$this->form_validation, $this->actor_model->get_all_edit_validation_rule());
		$this->_data['view_model'] = new Actor_admin_edit_view_model($this->actor_model);
		$this->_data['view_model']->set_model($model);
		$this->_data['view_model']->set_heading('Actor');
		$this->_data['view_data'] = $this->_get_view_edit_data($id);

		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/ActorEdit', $this->_data);
		}

		$name = $this->input->post('name');
		$stage_name = $this->input->post('stage_name');
		$additional_name = $this->input->post('additional_name');
		$gender = $this->input->post('gender');
		$image = $this->input->post('image');
		$image_id = $this->input->post('image_id');
		$dob = $this->input->post('dob');
		$expiry_date = $this->input->post('expiry_date');
		$status = $this->input->post('status');
		$additional_names = $this->input->post('additional_names');

		$result = $this->actor_model->edit([
			'name' => $name,
			'stage_name' => $stage_name,
			'additional_name' => $additional_name,
			'gender' => $gender,
			'image' => $image,
			'image_id' => $image_id,
			'dob' => $dob,
			'expiry_date' => $expiry_date,
			'status' => $status,
		], $id);

		if ($result)
		{
			$additional_names_array  = ($additional_names === ''  ? [] : explode (',', $additional_names));

			if(!empty($additional_names_array))
			{
				$param = [];

				for($i = 0; $i < count($additional_names_array); $i ++ )
				{
					$param[] = [
						'actor_id' => $id,
						'additional_name' => $additional_names_array[$i],
					];
				}

				$this->Actor_additional_name_model->batch_insert($param);
			}
			$this->success('Saved');
			return $this->redirect('/admin/actor/0', 'refresh');
		}

		$this->_data['error'] = 'Error';
		return $this->render('Admin/ActorEdit', $this->_data);
	}


	public function update_actor_name()
	{
		$this->load->model('Actor_additional_name_model');
		$name_id = (int) $this->input->get('name_id');
		$name = $this->input->get('name');

		if(!empty($name))
		{
			$result = $this->Actor_additional_name_model->edit(['additional_name' => $name], $name_id);

			if($result)
			{
				$response = ['error' => FALSE, 'message' => 'name updated'];
				echo json_encode($response);
				exit();
			}

		}

		$response = ['error' => TRUE, 'message' => 'error updating name'];
		echo json_encode($response);
		exit();
	}


	public function view($id)
	{
		$model = $this->actor_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/admin/actor/0');
		}

		$this->load->model('actor_additional_name_model');

		$name_list = $this->actor_additional_name_model->get_all(['actor_id' => $id]);
		$names = [];

		foreach ($name_list as $name_key => $person)
		{
			$names[] = $person->additional_name;
		}

		$model->additional_name = implode('<br/>', $names);

		include_once __DIR__ . '/../../view_models/Actor_admin_view_view_model.php';
		$this->_data['view_model'] = new Actor_admin_view_view_model($this->actor_model);
		$this->_data['view_model']->set_heading('Actor');
		$this->_data['view_model']->set_model($model);
		return $this->render('Admin/ActorView', $this->_data);
	}

	public function _get_view_edit_data($id)
	{
		$this->load->model('Actor_additional_name_model');
		$view_data = [
			'actor_names' => $this->Actor_additional_name_model->get_by_actor($id)
		];
		return $view_data;
	}

}
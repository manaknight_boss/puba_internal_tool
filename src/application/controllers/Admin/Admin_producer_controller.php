<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Admin_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Producers Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Admin_producer_controller extends Admin_controller
{
    protected $_model_file = 'producer_model';
    public $_page_name = 'Producers';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin_operation_model');

    }

	public function index($page)
	{
        $this->load->library('pagination');
        include_once __DIR__ . '/../../view_models/Producers_admin_list_paginate_view_model.php';
        $session = $this->get_session();
        $this->_data['view_model'] = new Producers_admin_list_paginate_view_model(
            $this->producer_model,
            $this->pagination,
            '/admin/producer/0');
        $this->_data['view_model']->set_heading('Producers');
        $this->_data['view_model']->set_id(($this->input->get('id', TRUE) != NULL) ? $this->input->get('id', TRUE) : NULL);
		$this->_data['view_model']->set_name(($this->input->get('name', TRUE) != NULL) ? $this->input->get('name', TRUE) : NULL);
		$this->_data['view_model']->set_phone(($this->input->get('phone', TRUE) != NULL) ? $this->input->get('phone', TRUE) : NULL);
		$this->_data['view_model']->set_email(($this->input->get('email', TRUE) != NULL) ? $this->input->get('email', TRUE) : NULL);
		$this->_data['view_model']->set_status(($this->input->get('status', TRUE) != NULL) ? $this->input->get('status', TRUE) : NULL);
		
        $where = [
            'id' => $this->_data['view_model']->get_id(),
			'name' => $this->_data['view_model']->get_name(),
			'phone' => $this->_data['view_model']->get_phone(),
			'email' => $this->_data['view_model']->get_email(),
			'status' => $this->_data['view_model']->get_status(),
			
        ];

        $this->_data['view_model']->set_total_rows($this->producer_model->count($where));

        $this->_data['view_model']->set_per_page(10);
        $this->_data['view_model']->set_page($page);
		$this->_data['view_model']->set_list($this->producer_model->get_paginated(
            $this->_data['view_model']->get_page(),
            $this->_data['view_model']->get_per_page(),
            $where));
        return $this->render('Admin/Producers', $this->_data);
	}

	public function add()
	{
        include_once __DIR__ . '/../../view_models/Producers_admin_add_view_model.php';
        $this->form_validation = $this->producer_model->set_form_validation(
        $this->form_validation, $this->producer_model->get_all_validation_rule());
        $this->_data['view_model'] = new Producers_admin_add_view_model($this->producer_model);
        $this->_data['view_model']->set_heading('Producers');

		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/ProducersAdd', $this->_data);
        }

        $name = $this->input->post('name');
		$address = $this->input->post('address');
		$phone = $this->input->post('phone');
		$email = $this->input->post('email');
		$url = $this->input->post('url');
		$contact_name = $this->input->post('contact_name');
		$note = $this->input->post('note');
		
        $result = $this->producer_model->create([
            'name' => $name,
			'address' => $address,
			'phone' => $phone,
			'email' => $email,
			'url' => $url,
			'contact_name' => $contact_name,
			'note' => $note,
			
        ]);

        if ($result)
        {
            $this->success('Saved');

            return $this->redirect('/admin/producer/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Admin/ProducersAdd', $this->_data);
	}

	public function edit($id)
	{
        $model = $this->producer_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/admin/producer/0');
        }

        include_once __DIR__ . '/../../view_models/Producers_admin_edit_view_model.php';
        $this->form_validation = $this->producer_model->set_form_validation(
        $this->form_validation, $this->producer_model->get_all_edit_validation_rule());
        $this->_data['view_model'] = new Producers_admin_edit_view_model($this->producer_model);
        $this->_data['view_model']->set_model($model);
        $this->_data['view_model']->set_heading('Producers');
        
		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/ProducersEdit', $this->_data);
        }

        $name = $this->input->post('name');
		$address = $this->input->post('address');
		$phone = $this->input->post('phone');
		$email = $this->input->post('email');
		$url = $this->input->post('url');
		$contact_name = $this->input->post('contact_name');
		$status = $this->input->post('status');
		$note = $this->input->post('note');
		
        $result = $this->producer_model->edit([
            'name' => $name,
			'address' => $address,
			'phone' => $phone,
			'email' => $email,
			'url' => $url,
			'contact_name' => $contact_name,
			'status' => $status,
			'note' => $note,
			
        ], $id);

        if ($result)
        {
            
            return $this->redirect('/admin/producer/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Admin/ProducersEdit', $this->_data);
	}






}
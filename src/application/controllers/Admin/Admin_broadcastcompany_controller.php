<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Admin_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Broadcastcompanies Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Admin_broadcastcompany_controller extends Admin_controller
{
    protected $_model_file = 'broadcast_company_model';
    public $_page_name = 'Broadcast Companies';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin_operation_model');

    }

	public function index($page)
	{
        $this->load->library('pagination');
        include_once __DIR__ . '/../../view_models/Broadcastcompanies_admin_list_paginate_view_model.php';

        $session = $this->get_session();
        $where = [];
        $this->_data['view_model'] = new Broadcastcompanies_admin_list_paginate_view_model(
            $this->broadcast_company_model,
            $this->pagination,
            '/admin/broadcastcompany/0');
        $this->_data['view_model']->set_heading('Broadcast Companies');
        $this->_data['view_model']->set_total_rows($this->broadcast_company_model->count($where));

        $this->_data['view_model']->set_per_page(10);
        $this->_data['view_model']->set_page($page);
		$this->_data['view_model']->set_list($this->broadcast_company_model->get_paginated(
            $this->_data['view_model']->get_page(),
            $this->_data['view_model']->get_per_page(),
            $where));
        return $this->render('Admin/Broadcastcompanies', $this->_data);
	}

	public function add()
	{
        include_once __DIR__ . '/../../view_models/Broadcastcompanies_admin_add_view_model.php';
        $this->form_validation = $this->broadcast_company_model->set_form_validation(
        $this->form_validation, $this->broadcast_company_model->get_all_validation_rule());
        $this->_data['view_model'] = new Broadcastcompanies_admin_add_view_model($this->broadcast_company_model);
        $this->_data['view_model']->set_heading('Broadcast Companies');

		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/BroadcastcompaniesAdd', $this->_data);
        }

        $name = $this->input->post('name');
		
        $result = $this->broadcast_company_model->create([
            'name' => $name,
			
        ]);

        if ($result)
        {
            $this->success('Saved');
			$this->admin_operation_model->log_activity('add broadcast', $this->broadcast_company_model->get($result), $this->get_session()['user_id']);
            return $this->redirect('/admin/broadcastcompany/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Admin/BroadcastcompaniesAdd', $this->_data);
	}

	public function edit($id)
	{
        $model = $this->broadcast_company_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/admin/broadcastcompany/0');
        }

        include_once __DIR__ . '/../../view_models/Broadcastcompanies_admin_edit_view_model.php';
        $this->form_validation = $this->broadcast_company_model->set_form_validation(
        $this->form_validation, $this->broadcast_company_model->get_all_edit_validation_rule());
        $this->_data['view_model'] = new Broadcastcompanies_admin_edit_view_model($this->broadcast_company_model);
        $this->_data['view_model']->set_model($model);
        $this->_data['view_model']->set_heading('Broadcast Companies');
        
		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/BroadcastcompaniesEdit', $this->_data);
        }

        $name = $this->input->post('name');
		$status = $this->input->post('status');
		
        $result = $this->broadcast_company_model->edit([
            'name' => $name,
			'status' => $status,
			
        ], $id);

        if ($result)
        {
            
            return $this->redirect('/admin/broadcastcompany/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Admin/BroadcastcompaniesEdit', $this->_data);
	}






}
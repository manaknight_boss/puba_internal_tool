<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Admin_controller.php';

/**
 * Admin Dashboard Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Admin_dashboard_controller extends Admin_controller
{
    public $_page_name = 'Dashboard';

    public function __construct()
    {
        parent::__construct();
    }

    public function index ()
    {
        return $this->render('Admin/Dashboard', $this->_data);
    }
}
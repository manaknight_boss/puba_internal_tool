<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Admin_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Musicrelease Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Admin_musicrelease_controller extends Admin_controller
{
    protected $_model_file = 'music_release_model';
    public $_page_name = 'Music Release';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin_operation_model');

    }





	public function edit($id)
	{
        $model = $this->music_release_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/admin/musicrelease');
        }

        include_once __DIR__ . '/../../view_models/Musicrelease_admin_edit_view_model.php';
        $this->form_validation = $this->music_release_model->set_form_validation(
        $this->form_validation, $this->music_release_model->get_all_edit_validation_rule());
        $this->_data['view_model'] = new Musicrelease_admin_edit_view_model($this->music_release_model);
        $this->_data['view_model']->set_model($model);
        $this->_data['view_model']->set_heading('Music Release');
        
		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/MusicreleaseEdit', $this->_data);
        }

        $legal_text = $this->input->post('legal_text');
		$image = $this->input->post('image');
		$image_id = $this->input->post('image_id');
		$signer_name = $this->input->post('signer_name');
		$file_name = $this->input->post('file_name');
		$file_name_id = $this->input->post('file_name_id');
		
        $result = $this->music_release_model->edit([
            'legal_text' => $legal_text,
			'image' => $image,
			'image_id' => $image_id,
			'signer_name' => $signer_name,
			'file_name' => $file_name,
			'file_name_id' => $file_name_id,
			
        ], $id);

        if ($result)
        {
            
            return $this->redirect('/admin/musicrelease', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Admin/MusicreleaseEdit', $this->_data);
	}






}
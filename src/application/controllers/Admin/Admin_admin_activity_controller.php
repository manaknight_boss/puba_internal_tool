<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Admin_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Admin_operation Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Admin_admin_activity_controller extends Admin_controller
{
    protected $_model_file = 'admin_operation_model';
    public $_page_name = 'Admin Activity Log';

    public function __construct()
    {
        parent::__construct();
        

    }

	public function index($page)
	{
        $this->load->library('pagination');
        include_once __DIR__ . '/../../view_models/Admin_operation_admin_list_paginate_view_model.php';
        $session = $this->get_session();
        $this->_data['view_model'] = new Admin_operation_admin_list_paginate_view_model(
            $this->admin_operation_model,
            $this->pagination,
            '/admin/activitylog/admin/0');
        $this->_data['view_model']->set_heading('Admin Activity Log');
        $this->_data['view_model']->set_user_id(($this->input->get('user_id', TRUE) != NULL) ? $this->input->get('user_id', TRUE) : NULL);
		$this->_data['view_model']->set_updated_at(($this->input->get('updated_at', TRUE) != NULL) ? $this->input->get('updated_at', TRUE) : NULL);
		$this->_data['view_model']->set_last_ip(($this->input->get('last_ip', TRUE) != NULL) ? $this->input->get('last_ip', TRUE) : NULL);
		
        $where = [
            'user_id' => $this->_data['view_model']->get_user_id(),
			'updated_at' => $this->_data['view_model']->get_updated_at(),
			'last_ip' => $this->_data['view_model']->get_last_ip(),
			
        ];

        $this->_data['view_model']->set_total_rows($this->admin_operation_model->count($where));

        $this->_data['view_model']->set_per_page(10);
        $this->_data['view_model']->set_page($page);
		$this->_data['view_model']->set_list($this->admin_operation_model->get_paginated(
            $this->_data['view_model']->get_page(),
            $this->_data['view_model']->get_per_page(),
            $where));
        return $this->render('Admin/Admin_operation', $this->_data);
	}










}
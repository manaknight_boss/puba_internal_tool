<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Admin_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Tubesites Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Admin_tubesite_controller extends Admin_controller
{
    protected $_model_file = 'tube_site_model';
    public $_page_name = 'Tube Sites';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin_operation_model');

    }

	public function index($page)
	{
        $this->load->library('pagination');
        include_once __DIR__ . '/../../view_models/Tubesites_admin_list_paginate_view_model.php';

        $session = $this->get_session();
        $where = [];
        $this->_data['view_model'] = new Tubesites_admin_list_paginate_view_model(
            $this->tube_site_model,
            $this->pagination,
            '/admin/tubesite/0');
        $this->_data['view_model']->set_heading('Tube Sites');
        $this->_data['view_model']->set_total_rows($this->tube_site_model->count($where));

        $this->_data['view_model']->set_per_page(10);
        $this->_data['view_model']->set_page($page);
		$this->_data['view_model']->set_list($this->tube_site_model->get_paginated(
            $this->_data['view_model']->get_page(),
            $this->_data['view_model']->get_per_page(),
            $where));
        return $this->render('Admin/Tubesites', $this->_data);
	}

	public function add()
	{
        include_once __DIR__ . '/../../view_models/Tubesites_admin_add_view_model.php';
        $this->form_validation = $this->tube_site_model->set_form_validation(
        $this->form_validation, $this->tube_site_model->get_all_validation_rule());
        $this->_data['view_model'] = new Tubesites_admin_add_view_model($this->tube_site_model);
        $this->_data['view_model']->set_heading('Tube Sites');

		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/TubesitesAdd', $this->_data);
        }

        $name = $this->input->post('name');
		$url = $this->input->post('url');
		
        $result = $this->tube_site_model->create([
            'name' => $name,
			'url' => $url,
			
        ]);

        if ($result)
        {
            $this->success('Saved');

            return $this->redirect('/admin/tubesite/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Admin/TubesitesAdd', $this->_data);
	}

	public function edit($id)
	{
        $model = $this->tube_site_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/admin/tubesite/0');
        }

        include_once __DIR__ . '/../../view_models/Tubesites_admin_edit_view_model.php';
        $this->form_validation = $this->tube_site_model->set_form_validation(
        $this->form_validation, $this->tube_site_model->get_all_edit_validation_rule());
        $this->_data['view_model'] = new Tubesites_admin_edit_view_model($this->tube_site_model);
        $this->_data['view_model']->set_model($model);
        $this->_data['view_model']->set_heading('Tube Sites');
        
		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/TubesitesEdit', $this->_data);
        }

        $name = $this->input->post('name');
		$url = $this->input->post('url');
		
        $result = $this->tube_site_model->edit([
            'name' => $name,
			'url' => $url,
			
        ], $id);

        if ($result)
        {
            
            return $this->redirect('/admin/tubesite/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Admin/TubesitesEdit', $this->_data);
	}






}
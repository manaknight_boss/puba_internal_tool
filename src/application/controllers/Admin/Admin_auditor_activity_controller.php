<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Admin_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Auditor_operation Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Admin_auditor_activity_controller extends Admin_controller
{
    protected $_model_file = 'auditor_operation_model';
    public $_page_name = 'Auditor Activity Log';

    public function __construct()
    {
        parent::__construct();
        

    }

	public function index($page)
	{
        $this->load->library('pagination');
        include_once __DIR__ . '/../../view_models/Auditor_operation_admin_list_paginate_view_model.php';
        $session = $this->get_session();
        $this->_data['view_model'] = new Auditor_operation_admin_list_paginate_view_model(
            $this->auditor_operation_model,
            $this->pagination,
            '/admin/activitylog/auditor/0');
        $this->_data['view_model']->set_heading('Auditor Activity Log');
        
        $where = [
            
        ];

        $this->_data['view_model']->set_total_rows($this->auditor_operation_model->count($where));

        $this->_data['view_model']->set_per_page(10);
        $this->_data['view_model']->set_page($page);
		$this->_data['view_model']->set_list($this->auditor_operation_model->get_paginated(
            $this->_data['view_model']->get_page(),
            $this->_data['view_model']->get_per_page(),
            $where));
        return $this->render('Admin/Auditor_operation', $this->_data);
	}










}
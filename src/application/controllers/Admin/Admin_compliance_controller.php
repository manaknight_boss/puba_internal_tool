<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Admin_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Compliance Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Admin_compliance_controller extends Admin_controller
{
    protected $_model_file = 'compliance_model';
    public $_page_name = 'Compliance Statement';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin_operation_model');

    }





	public function edit($id)
	{
        $model = $this->compliance_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/admin/compliance');
        }

        include_once __DIR__ . '/../../view_models/Compliance_admin_edit_view_model.php';
        $this->form_validation = $this->compliance_model->set_form_validation(
        $this->form_validation, $this->compliance_model->get_all_edit_validation_rule());
        $this->_data['view_model'] = new Compliance_admin_edit_view_model($this->compliance_model);
        $this->_data['view_model']->set_model($model);
        $this->_data['view_model']->set_heading('Compliance Statement');
        
		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/ComplianceEdit', $this->_data);
        }

        $legal_text = $this->input->post('legal_text');
		$image = $this->input->post('image');
		$image_id = $this->input->post('image_id');
		$signer_name = $this->input->post('signer_name');
		$file_name = $this->input->post('file_name');
		$file_name_id = $this->input->post('file_name_id');
		
        $result = $this->compliance_model->edit([
            'legal_text' => $legal_text,
			'image' => $image,
			'image_id' => $image_id,
			'signer_name' => $signer_name,
			'file_name' => $file_name,
			'file_name_id' => $file_name_id,
			
        ], $id);

        if ($result)
        {
            
            return $this->redirect('/admin/compliance', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Admin/ComplianceEdit', $this->_data);
	}






}
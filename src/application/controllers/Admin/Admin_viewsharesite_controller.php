<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Admin_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Viewsharesites Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Admin_viewsharesite_controller extends Admin_controller
{
    protected $_model_file = 'viewshare_site_model';
    public $_page_name = 'Viewshare Sites';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin_operation_model');

    }

	public function index($page)
	{
        $this->load->library('pagination');
        include_once __DIR__ . '/../../view_models/Viewsharesites_admin_list_paginate_view_model.php';

        $session = $this->get_session();
        $where = [];
        $this->_data['view_model'] = new Viewsharesites_admin_list_paginate_view_model(
            $this->viewshare_site_model,
            $this->pagination,
            '/admin/viewsharesite/0');
        $this->_data['view_model']->set_heading('Viewshare Sites');
        $this->_data['view_model']->set_total_rows($this->viewshare_site_model->count($where));

        $this->_data['view_model']->set_per_page(10);
        $this->_data['view_model']->set_page($page);
		$this->_data['view_model']->set_list($this->viewshare_site_model->get_paginated(
            $this->_data['view_model']->get_page(),
            $this->_data['view_model']->get_per_page(),
            $where));
        return $this->render('Admin/Viewsharesites', $this->_data);
	}

	public function add()
	{
        include_once __DIR__ . '/../../view_models/Viewsharesites_admin_add_view_model.php';
        $this->form_validation = $this->viewshare_site_model->set_form_validation(
        $this->form_validation, $this->viewshare_site_model->get_all_validation_rule());
        $this->_data['view_model'] = new Viewsharesites_admin_add_view_model($this->viewshare_site_model);
        $this->_data['view_model']->set_heading('Viewshare Sites');

		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/ViewsharesitesAdd', $this->_data);
        }

        $name = $this->input->post('name');
		
        $result = $this->viewshare_site_model->create([
            'name' => $name,
			
        ]);

        if ($result)
        {
            $this->success('Saved');

            return $this->redirect('/admin/viewsharesite/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Admin/ViewsharesitesAdd', $this->_data);
	}

	public function edit($id)
	{
        $model = $this->viewshare_site_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/admin/viewsharesite/0');
        }

        include_once __DIR__ . '/../../view_models/Viewsharesites_admin_edit_view_model.php';
        $this->form_validation = $this->viewshare_site_model->set_form_validation(
        $this->form_validation, $this->viewshare_site_model->get_all_edit_validation_rule());
        $this->_data['view_model'] = new Viewsharesites_admin_edit_view_model($this->viewshare_site_model);
        $this->_data['view_model']->set_model($model);
        $this->_data['view_model']->set_heading('Viewshare Sites');
        
		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/ViewsharesitesEdit', $this->_data);
        }

        $name = $this->input->post('name');
		
        $result = $this->viewshare_site_model->edit([
            'name' => $name,
			
        ], $id);

        if ($result)
        {
            
            return $this->redirect('/admin/viewsharesite/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Admin/ViewsharesitesEdit', $this->_data);
	}






}
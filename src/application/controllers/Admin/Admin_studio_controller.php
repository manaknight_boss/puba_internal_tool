<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Admin_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Studios Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Admin_studio_controller extends Admin_controller
{
    protected $_model_file = 'studio_model';
    public $_page_name = 'Studios';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin_operation_model');

    }

	public function index($page)
	{
        $this->load->library('pagination');
        include_once __DIR__ . '/../../view_models/Studios_admin_list_paginate_view_model.php';
        $session = $this->get_session();
        $this->_data['view_model'] = new Studios_admin_list_paginate_view_model(
            $this->studio_model,
            $this->pagination,
            '/admin/studio/0');
        $this->_data['view_model']->set_heading('Studios');
        $this->_data['view_model']->set_name(($this->input->get('name', TRUE) != NULL) ? $this->input->get('name', TRUE) : NULL);
		$this->_data['view_model']->set_company_id(($this->input->get('company_id', TRUE) != NULL) ? $this->input->get('company_id', TRUE) : NULL);
		$this->_data['view_model']->set_status(($this->input->get('status', TRUE) != NULL) ? $this->input->get('status', TRUE) : NULL);
		
        $where = [
            'name' => $this->_data['view_model']->get_name(),
			'company_id' => $this->_data['view_model']->get_company_id(),
			'status' => $this->_data['view_model']->get_status(),
			
        ];

        $this->_data['view_model']->set_total_rows($this->studio_model->count($where));

        $this->_data['view_model']->set_per_page(10);
        $this->_data['view_model']->set_page($page);
		$this->_data['view_model']->set_list($this->studio_model->get_paginated(
            $this->_data['view_model']->get_page(),
            $this->_data['view_model']->get_per_page(),
            $where));
        return $this->render('Admin/Studios', $this->_data);
	}

	public function add()
	{
        include_once __DIR__ . '/../../view_models/Studios_admin_add_view_model.php';
        $this->form_validation = $this->studio_model->set_form_validation(
        $this->form_validation, $this->studio_model->get_all_validation_rule());
        $this->_data['view_model'] = new Studios_admin_add_view_model($this->studio_model);
        $this->_data['view_model']->set_heading('Studios');

		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/StudiosAdd', $this->_data);
        }

        $name = $this->input->post('name');
		$company_id = $this->input->post('company_id');
		
        $result = $this->studio_model->create([
            'name' => $name,
			'company_id' => $company_id,
			
        ]);

        if ($result)
        {
            $this->success('Saved');
			$this->admin_operation_model->log_activity('add studio', $this->studio_model->get($result), $this->get_session()['user_id']);
            return $this->redirect('/admin/studio/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Admin/StudiosAdd', $this->_data);
	}

	public function edit($id)
	{
        $model = $this->studio_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/admin/studio/0');
        }

        include_once __DIR__ . '/../../view_models/Studios_admin_edit_view_model.php';
        $this->form_validation = $this->studio_model->set_form_validation(
        $this->form_validation, $this->studio_model->get_all_edit_validation_rule());
        $this->_data['view_model'] = new Studios_admin_edit_view_model($this->studio_model);
        $this->_data['view_model']->set_model($model);
        $this->_data['view_model']->set_heading('Studios');
        
		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/StudiosEdit', $this->_data);
        }

        $name = $this->input->post('name');
		$company_id = $this->input->post('company_id');
		$status = $this->input->post('status');
		
        $result = $this->studio_model->edit([
            'name' => $name,
			'company_id' => $company_id,
			'status' => $status,
			
        ], $id);

        if ($result)
        {
            
            return $this->redirect('/admin/studio/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Admin/StudiosEdit', $this->_data);
	}






}
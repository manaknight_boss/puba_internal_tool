<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Admin_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Loginlog Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Admin_loginlog_controller extends Admin_controller
{
    protected $_model_file = 'login_log_model';
    public $_page_name = 'Login Log';

    public function __construct()
    {
        parent::__construct();
        

    }

	public function index($page)
	{
        $this->load->library('pagination');
        include_once __DIR__ . '/../../view_models/Loginlog_admin_list_paginate_view_model.php';
        $session = $this->get_session();
        $this->_data['view_model'] = new Loginlog_admin_list_paginate_view_model(
            $this->login_log_model,
            $this->pagination,
            '/admin/loginlog/0');
        $this->_data['view_model']->set_heading('Login Log');
        $this->_data['view_model']->set_ip(($this->input->get('ip', TRUE) != NULL) ? $this->input->get('ip', TRUE) : NULL);
		$this->_data['view_model']->set_user_id(($this->input->get('user_id', TRUE) != NULL) ? $this->input->get('user_id', TRUE) : NULL);
		$this->_data['view_model']->set_created_at(($this->input->get('created_at', TRUE) != NULL) ? $this->input->get('created_at', TRUE) : NULL);
		
        $where = [
            'ip' => $this->_data['view_model']->get_ip(),
			'user_id' => $this->_data['view_model']->get_user_id(),
			'created_at' => $this->_data['view_model']->get_created_at(),
			
        ];

        $this->_data['view_model']->set_total_rows($this->login_log_model->count($where));

        $this->_data['view_model']->set_per_page(10);
        $this->_data['view_model']->set_page($page);
		$this->_data['view_model']->set_list($this->login_log_model->get_paginated(
            $this->_data['view_model']->get_page(),
            $this->_data['view_model']->get_per_page(),
            $where));
        return $this->render('Admin/Loginlog', $this->_data);
	}










}
<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Admin_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Groups Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Admin_group_controller extends Admin_controller
{
    protected $_model_file = 'group_model';
    public $_page_name = 'Groups';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin_operation_model');

    }

	public function index($page)
	{
        $this->load->library('pagination');
        include_once __DIR__ . '/../../view_models/Groups_admin_list_paginate_view_model.php';
        $session = $this->get_session();
        $this->_data['view_model'] = new Groups_admin_list_paginate_view_model(
            $this->group_model,
            $this->pagination,
            '/admin/group/0');
        $this->_data['view_model']->set_heading('Groups');
        $this->_data['view_model']->set_name(($this->input->get('name', TRUE) != NULL) ? $this->input->get('name', TRUE) : NULL);
		$this->_data['view_model']->set_status(($this->input->get('status', TRUE) != NULL) ? $this->input->get('status', TRUE) : NULL);
		
        $where = [
            'name' => $this->_data['view_model']->get_name(),
			'status' => $this->_data['view_model']->get_status(),
			
        ];

        $this->_data['view_model']->set_total_rows($this->group_model->count($where));

        $this->_data['view_model']->set_per_page(10);
        $this->_data['view_model']->set_page($page);
		$this->_data['view_model']->set_list($this->group_model->get_paginated(
            $this->_data['view_model']->get_page(),
            $this->_data['view_model']->get_per_page(),
            $where));
        return $this->render('Admin/Groups', $this->_data);
	}

	public function add()
	{
        include_once __DIR__ . '/../../view_models/Groups_admin_add_view_model.php';
        $this->form_validation = $this->group_model->set_form_validation(
        $this->form_validation, $this->group_model->get_all_validation_rule());
        $this->_data['view_model'] = new Groups_admin_add_view_model($this->group_model);
        $this->_data['view_model']->set_heading('Groups');

		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/GroupsAdd', $this->_data);
        }

        $name = $this->input->post('name');
		
        $result = $this->group_model->create([
            'name' => $name,
			
        ]);

        if ($result)
        {
            $this->success('Saved');
			$this->admin_operation_model->log_activity('add group', $this->group_model->get($result), $this->get_session()['user_id']);
            return $this->redirect('/admin/group/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Admin/GroupsAdd', $this->_data);
	}

	public function edit($id)
	{
        $model = $this->group_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/admin/group/0');
        }

        include_once __DIR__ . '/../../view_models/Groups_admin_edit_view_model.php';
        $this->form_validation = $this->group_model->set_form_validation(
        $this->form_validation, $this->group_model->get_all_edit_validation_rule());
        $this->_data['view_model'] = new Groups_admin_edit_view_model($this->group_model);
        $this->_data['view_model']->set_model($model);
        $this->_data['view_model']->set_heading('Groups');
        
		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/GroupsEdit', $this->_data);
        }

        $name = $this->input->post('name');
		$status = $this->input->post('status');
		
        $result = $this->group_model->edit([
            'name' => $name,
			'status' => $status,
			
        ], $id);

        if ($result)
        {
            
            return $this->redirect('/admin/group/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Admin/GroupsEdit', $this->_data);
	}






}
<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Admin_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Networks Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Admin_network_controller extends Admin_controller
{
    protected $_model_file = 'networks_model';
    public $_page_name = 'Networks';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin_operation_model');

    }

	public function index($page)
	{
        $this->load->library('pagination');
        include_once __DIR__ . '/../../view_models/Networks_admin_list_paginate_view_model.php';

        $session = $this->get_session();
        $where = [];
        $this->_data['view_model'] = new Networks_admin_list_paginate_view_model(
            $this->networks_model,
            $this->pagination,
            '/admin/network/0');
        $this->_data['view_model']->set_heading('Networks');
        $this->_data['view_model']->set_total_rows($this->networks_model->count($where));

        $this->_data['view_model']->set_per_page(10);
        $this->_data['view_model']->set_page($page);
		$this->_data['view_model']->set_list($this->networks_model->get_paginated(
            $this->_data['view_model']->get_page(),
            $this->_data['view_model']->get_per_page(),
            $where));
        return $this->render('Admin/Networks', $this->_data);
	}

	public function add()
	{
        include_once __DIR__ . '/../../view_models/Networks_admin_add_view_model.php';
        $this->form_validation = $this->networks_model->set_form_validation(
        $this->form_validation, $this->networks_model->get_all_validation_rule());
        $this->_data['view_model'] = new Networks_admin_add_view_model($this->networks_model);
        $this->_data['view_model']->set_heading('Networks');

		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/NetworksAdd', $this->_data);
        }

        $name = $this->input->post('name');
		
        $result = $this->networks_model->create([
            'name' => $name,
			
        ]);

        if ($result)
        {
            $this->success('Saved');
			$this->admin_operation_model->log_activity('add network', $this->networks_model->get($result), $this->get_session()['user_id']);
            return $this->redirect('/admin/network/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Admin/NetworksAdd', $this->_data);
	}

	public function edit($id)
	{
        $model = $this->networks_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/admin/network/0');
        }

        include_once __DIR__ . '/../../view_models/Networks_admin_edit_view_model.php';
        $this->form_validation = $this->networks_model->set_form_validation(
        $this->form_validation, $this->networks_model->get_all_edit_validation_rule());
        $this->_data['view_model'] = new Networks_admin_edit_view_model($this->networks_model);
        $this->_data['view_model']->set_model($model);
        $this->_data['view_model']->set_heading('Networks');
        
		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/NetworksEdit', $this->_data);
        }

        $name = $this->input->post('name');
		$status = $this->input->post('status');
		
        $result = $this->networks_model->edit([
            'name' => $name,
			'status' => $status,
			
        ], $id);

        if ($result)
        {
            
            return $this->redirect('/admin/network/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Admin/NetworksEdit', $this->_data);
	}






}
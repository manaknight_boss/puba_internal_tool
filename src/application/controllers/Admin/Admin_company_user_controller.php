<?php defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Admin_controller.php';
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Company user Controller
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Admin_company_user_controller extends Admin_controller
{
    protected $_model_file = 'company_user_model';
    public $_page_name = 'Company Users';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin_operation_model');

    }

	public function index($page)
	{
        $this->load->library('pagination');
        include_once __DIR__ . '/../../view_models/Company user_admin_list_paginate_view_model.php';
        $session = $this->get_session();
        $this->_data['view_model'] = new Company user_admin_list_paginate_view_model(
            $this->company_user_model,
            $this->pagination,
            '/admin/company_user/0');
        $this->_data['view_model']->set_heading('Company Users');
        $this->_data['view_model']->set_company_id(($this->input->get('company_id', TRUE) != NULL) ? $this->input->get('company_id', TRUE) : NULL);
		
        $where = [
            'company_id' => $this->_data['view_model']->get_company_id(),
			
        ];

        $this->_data['view_model']->set_total_rows($this->company_user_model->count($where));

        $this->_data['view_model']->set_per_page(10);
        $this->_data['view_model']->set_page($page);
		$this->_data['view_model']->set_list($this->company_user_model->get_paginated(
            $this->_data['view_model']->get_page(),
            $this->_data['view_model']->get_per_page(),
            $where));
        return $this->render('Admin/Company user', $this->_data);
	}

	public function add()
	{
        include_once __DIR__ . '/../../view_models/Company user_admin_add_view_model.php';
        $this->form_validation = $this->company_user_model->set_form_validation(
        $this->form_validation, $this->company_user_model->get_all_validation_rule());
        $this->_data['view_model'] = new Company user_admin_add_view_model($this->company_user_model);
        $this->_data['view_model']->set_heading('Company Users');

		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/Company userAdd', $this->_data);
        }

        $company_id = $this->input->post('company_id');
		$user_id = $this->input->post('user_id');
		
        $result = $this->company_user_model->create([
            'company_id' => $company_id,
			'user_id' => $user_id,
			
        ]);

        if ($result)
        {
            $this->success('Saved');

            return $this->redirect('/admin/company_user/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Admin/Company userAdd', $this->_data);
	}

	public function edit($id)
	{
        $model = $this->company_user_model->get($id);

		if (!$model)
		{
			$this->error('Error');
			return redirect('/admin/company_user/0');
        }

        include_once __DIR__ . '/../../view_models/Company user_admin_edit_view_model.php';
        $this->form_validation = $this->company_user_model->set_form_validation(
        $this->form_validation, $this->company_user_model->get_all_edit_validation_rule());
        $this->_data['view_model'] = new Company user_admin_edit_view_model($this->company_user_model);
        $this->_data['view_model']->set_model($model);
        $this->_data['view_model']->set_heading('Company Users');
        
		if ($this->form_validation->run() === FALSE)
		{
			return $this->render('Admin/Company userEdit', $this->_data);
        }

        $company_id = $this->input->post('company_id');
		
        $result = $this->company_user_model->edit([
            'company_id' => $company_id,
			
        ], $id);

        if ($result)
        {
            
            return $this->redirect('/admin/company_user/0', 'refresh');
        }

        $this->_data['error'] = 'Error';
        return $this->render('Admin/Company userEdit', $this->_data);
	}






}
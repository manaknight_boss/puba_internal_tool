<?php defined('BASEPATH') or exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['dummy'] = 'Admin/dummy';
$route['admin/compliance'] = 'Admin/Admin_compliance_controller/edit/1';
$route['admin/musicrelease'] = 'Admin/Admin_musicrelease_controller/edit/1';
$route['admin/dvds/filter_dvd_scenes'] = 'Admin/Admin_dvd_controller/filter_dvd_scenes';
$route['admin/dvds/remove_dvd_scene'] = 'Admin/Admin_dvd_controller/remove_dvd_scene';
$route['admin/dvds/download_cast/(:num)'] = 'Admin/Admin_dvd_controller/download_cast/$1';
$route['admin/dvds/download_music_release/(:num)'] = 'Admin/Admin_dvd_controller/download_music_release/$1';
$route['admin/dvds/download_compliance_statement/(:num)'] = 'Admin/Admin_dvd_controller/download_compliance_statement/$1';
$route['admin/scenes/multiple_uploads'] = 'Admin/Admin_scene_controller/upload_release_paper_work';
$route['admin/dvds/update_dvd/(:num)'] = 'Admin/Admin_dvd_controller/update_dvd/$1';
$route['admin/actors/update_additional_name'] = 'Admin/Admin_actor_controller/update_actor_name';
$route['staff/dvds/filter_dvd_scenes'] = 'Staff/Staff_dvd_controller/filter_dvd_scenes';
$route['staff/dvds/filter_company_studios'] = 'Staff/Staff_dvd_controller/filter_company_studios';
$route['staff/dvds/remove_dvd_scene'] = 'Staff/Staff_dvd_controller/remove_dvd_scene';
$route['staff/dvds/download_cast/(:num)'] = 'Staff/Staff_dvd_controller/download_cast/$1';
$route['staff/dvds/download_music_release/(:num)'] = 'Staff/Staff_dvd_controller/download_music_release/$1';
$route['staff/dvds/download_compliance_statement/(:num)'] = 'Staff/Staff_dvd_controller/download_compliance_statement/$1';
$route['cli/scene_comp'] = 'Cli/Scene_comp_cronjob_controller/comp_scene_to_next_studio';
$route['cli/seed_categories'] = 'Cli/Data_seed_cronjob_controller/seed_categories';
$route['cli/seed_actors'] = 'Cli/Data_seed_cronjob_controller/seed_actors';
$route['cli/seed_scenes'] = 'Cli/Data_seed_cronjob_controller/seed_scenes';
$route['cli/actor_status'] = 'Cli/Actor_expiry_date_cronjob_controller/update_actor_status';
$route['guest/upload_multiple_files'] = 'Guest/File_upload_controller/upload_multiple_files';
$route['health_check'] = 'Health_check_controller/index';
$route['admin/settings'] = 'Admin/Admin_setting_controller/index';
$route['v1/api/admin/settings/edit/(:num)'] = 'Admin/Admin_setting_controller/edit/$1';
$route['v1/api/image/upload'] = 'Guest/Image_controller';
$route['v1/api/file/upload'] = 'Guest/Image_controller/file_upload';
$route['v1/api/file/import/(:any)'] = 'Guest/Image_controller/file_import/$1';
$route['v1/api/assets'] = 'Guest/Image_controller/paginate/0';
$route['v1/api/assets/(:num)'] = 'Guest/Image_controller/paginate/$1';
$route['admin/dashboard'] = 'Admin/Admin_dashboard_controller';
$route['admin/profile'] = 'Admin/Admin_profile_controller';
$route['admin/login'] = 'Admin/Admin_login_controller';
$route['admin/logout'] = 'Admin/Admin_login_controller/logout';
$route['staff/dashboard'] = 'Staff/Staff_dashboard_controller';
$route['staff/profile'] = 'Staff/Staff_profile_controller';
$route['staff/login'] = 'Staff/Staff_login_controller';
$route['staff/logout'] = 'Staff/Staff_login_controller/logout';
$route['staff/forgot'] = 'Staff/Staff_forgot_controller';
$route['staff/reset/(:num)'] = 'Staff/Staff_reset_controller/index/$1';
$route['auditor/dashboard'] = 'Auditor/Auditor_dashboard_controller';
$route['auditor/profile'] = 'Auditor/Auditor_profile_controller';
$route['auditor/login'] = 'Auditor/Auditor_login_controller';
$route['auditor/logout'] = 'Auditor/Auditor_login_controller/logout';
$route['auditor/forgot'] = 'Auditor/Auditor_forgot_controller';
$route['auditor/reset/(:num)'] = 'Auditor/Auditor_reset_controller/index/$1';
$route['admin/users/(:num)'] = 'Admin/Admin_user_controller/index/$1';
$route['admin/users'] = 'Admin/Admin_user_controller/index/0';
$route['admin/users/add'] = 'Admin/Admin_user_controller/add';
$route['admin/users/edit/(:num)'] = 'Admin/Admin_user_controller/edit/$1';
$route['admin/users/view/(:num)'] = 'Admin/Admin_user_controller/view/$1';
$route['admin/users2257/(:num)'] = 'Admin/Admin_user2257_controller/index/$1';
$route['admin/users2257'] = 'Admin/Admin_user2257_controller/index/0';
$route['admin/users2257/add'] = 'Admin/Admin_user2257_controller/add';
$route['admin/users2257/edit/(:num)'] = 'Admin/Admin_user2257_controller/edit/$1';
$route['admin/users2257/view/(:num)'] = 'Admin/Admin_user2257_controller/view/$1';
$route['admin/users2257/delete/(:num)'] = 'Admin/Admin_user2257_controller/delete/$1';
$route['admin/emails/(:num)'] = 'Admin/Admin_email_controller/index/$1';
$route['admin/emails'] = 'Admin/Admin_email_controller/index/0';
$route['admin/emails/add'] = 'Admin/Admin_email_controller/add';
$route['admin/emails/edit/(:num)'] = 'Admin/Admin_email_controller/edit/$1';
$route['admin/emails/view/(:num)'] = 'Admin/Admin_email_controller/view/$1';
$route['admin/image/(:num)'] = 'Admin/Admin_image_controller/index/$1';
$route['admin/image'] = 'Admin/Admin_image_controller/index/0';
$route['admin/image/view/(:num)'] = 'Admin/Admin_image_controller/view/$1';
$route['admin/image/delete/(:num)'] = 'Admin/Admin_image_controller/delete/$1';
$route['admin/actor/(:num)'] = 'Admin/Admin_actor_controller/index/$1';
$route['admin/actor'] = 'Admin/Admin_actor_controller/index/0';
$route['admin/actor/add'] = 'Admin/Admin_actor_controller/add';
$route['admin/actor/edit/(:num)'] = 'Admin/Admin_actor_controller/edit/$1';
$route['admin/actor/view/(:num)'] = 'Admin/Admin_actor_controller/view/$1';
$route['admin/actor/delete/(:num)'] = 'Admin/Admin_actor_controller/delete/$1';
$route['v1/api/admin/actor/(:num)'] = 'Admin/Admin_actor_api_controller/index/$1';
$route['v1/api/admin/actor'] = 'Admin/Admin_actor_api_controller/index/0';
$route['v1/api/admin/actor/add'] = 'Admin/Admin_actor_api_controller/add';
$route['v1/api/admin/actor/edit/(:num)'] = 'Admin/Admin_actor_api_controller/edit/$1';
$route['v1/api/admin/actor/view/(:num)'] = 'Admin/Admin_actor_api_controller/view/$1';
$route['v1/api/admin/actor/delete/(:num)'] = 'Admin/Admin_actor_api_controller/delete/$1';
$route['admin/category/(:num)'] = 'Admin/Admin_category_controller/index/$1';
$route['admin/category'] = 'Admin/Admin_category_controller/index/0';
$route['admin/category/add'] = 'Admin/Admin_category_controller/add';
$route['admin/category/edit/(:num)'] = 'Admin/Admin_category_controller/edit/$1';
$route['admin/studio/(:num)'] = 'Admin/Admin_studio_controller/index/$1';
$route['admin/studio'] = 'Admin/Admin_studio_controller/index/0';
$route['admin/studio/add'] = 'Admin/Admin_studio_controller/add';
$route['admin/studio/edit/(:num)'] = 'Admin/Admin_studio_controller/edit/$1';
$route['admin/type/(:num)'] = 'Admin/Admin_type_controller/index/$1';
$route['admin/type'] = 'Admin/Admin_type_controller/index/0';
$route['admin/type/add'] = 'Admin/Admin_type_controller/add';
$route['admin/type/edit/(:num)'] = 'Admin/Admin_type_controller/edit/$1';
$route['admin/group/(:num)'] = 'Admin/Admin_group_controller/index/$1';
$route['admin/group'] = 'Admin/Admin_group_controller/index/0';
$route['admin/group/add'] = 'Admin/Admin_group_controller/add';
$route['admin/group/edit/(:num)'] = 'Admin/Admin_group_controller/edit/$1';
$route['admin/network/(:num)'] = 'Admin/Admin_network_controller/index/$1';
$route['admin/network'] = 'Admin/Admin_network_controller/index/0';
$route['admin/network/add'] = 'Admin/Admin_network_controller/add';
$route['admin/network/edit/(:num)'] = 'Admin/Admin_network_controller/edit/$1';
$route['admin/company/(:num)'] = 'Admin/Admin_company_controller/index/$1';
$route['admin/company'] = 'Admin/Admin_company_controller/index/0';
$route['admin/company/add'] = 'Admin/Admin_company_controller/add';
$route['admin/company/edit/(:num)'] = 'Admin/Admin_company_controller/edit/$1';
$route['admin/company/view/(:num)'] = 'Admin/Admin_company_controller/view/$1';
$route['admin/vodcompany/(:num)'] = 'Admin/Admin_vodcompany_controller/index/$1';
$route['admin/vodcompany'] = 'Admin/Admin_vodcompany_controller/index/0';
$route['admin/vodcompany/add'] = 'Admin/Admin_vodcompany_controller/add';
$route['admin/vodcompany/edit/(:num)'] = 'Admin/Admin_vodcompany_controller/edit/$1';
$route['admin/broadcastcompany/(:num)'] = 'Admin/Admin_broadcastcompany_controller/index/$1';
$route['admin/broadcastcompany'] = 'Admin/Admin_broadcastcompany_controller/index/0';
$route['admin/broadcastcompany/add'] = 'Admin/Admin_broadcastcompany_controller/add';
$route['admin/broadcastcompany/edit/(:num)'] = 'Admin/Admin_broadcastcompany_controller/edit/$1';
$route['admin/tubesite/(:num)'] = 'Admin/Admin_tubesite_controller/index/$1';
$route['admin/tubesite'] = 'Admin/Admin_tubesite_controller/index/0';
$route['admin/tubesite/add'] = 'Admin/Admin_tubesite_controller/add';
$route['admin/tubesite/edit/(:num)'] = 'Admin/Admin_tubesite_controller/edit/$1';
$route['admin/viewsharesite/(:num)'] = 'Admin/Admin_viewsharesite_controller/index/$1';
$route['admin/viewsharesite'] = 'Admin/Admin_viewsharesite_controller/index/0';
$route['admin/viewsharesite/add'] = 'Admin/Admin_viewsharesite_controller/add';
$route['admin/viewsharesite/edit/(:num)'] = 'Admin/Admin_viewsharesite_controller/edit/$1';
$route['admin/producer/(:num)'] = 'Admin/Admin_producer_controller/index/$1';
$route['admin/producer'] = 'Admin/Admin_producer_controller/index/0';
$route['admin/producer/add'] = 'Admin/Admin_producer_controller/add';
$route['admin/producer/edit/(:num)'] = 'Admin/Admin_producer_controller/edit/$1';
$route['admin/compliance/edit/(:num)'] = 'Admin/Admin_compliance_controller/edit/$1';
$route['admin/musicrelease/edit/(:num)'] = 'Admin/Admin_musicrelease_controller/edit/$1';
$route['admin/loginlog/(:num)'] = 'Admin/Admin_loginlog_controller/index/$1';
$route['admin/loginlog'] = 'Admin/Admin_loginlog_controller/index/0';
$route['admin/activitylog/staff/(:num)'] = 'Admin/Admin_staff_activity_controller/index/$1';
$route['admin/activitylog/staff'] = 'Admin/Admin_staff_activity_controller/index/0';
$route['admin/activitylog/admin/(:num)'] = 'Admin/Admin_admin_activity_controller/index/$1';
$route['admin/activitylog/admin'] = 'Admin/Admin_admin_activity_controller/index/0';
$route['admin/activitylog/auditor/(:num)'] = 'Admin/Admin_auditor_activity_controller/index/$1';
$route['admin/activitylog/auditor'] = 'Admin/Admin_auditor_activity_controller/index/0';
$route['admin/company_user/(:num)'] = 'Admin/Admin_company_user_controller/index/$1';
$route['admin/company_user'] = 'Admin/Admin_company_user_controller/index/0';
$route['admin/company_user/add'] = 'Admin/Admin_company_user_controller/add';
$route['admin/company_user/edit/(:num)'] = 'Admin/Admin_company_user_controller/edit/$1';
$route['admin/scenes/(:num)'] = 'Admin/Admin_scene_controller/index/$1';
$route['admin/scenes'] = 'Admin/Admin_scene_controller/index/0';
$route['admin/scenes/add'] = 'Admin/Admin_scene_controller/add';
$route['admin/scenes/edit/(:num)'] = 'Admin/Admin_scene_controller/edit/$1';
$route['admin/scenes/view/(:num)'] = 'Admin/Admin_scene_controller/view/$1';
$route['admin/dvds/(:num)'] = 'Admin/Admin_dvd_controller/index/$1';
$route['admin/dvds'] = 'Admin/Admin_dvd_controller/index/0';
$route['admin/dvds/add'] = 'Admin/Admin_dvd_controller/add';
$route['admin/dvds/edit/(:num)'] = 'Admin/Admin_dvd_controller/edit/$1';
$route['admin/dvds/view/(:num)'] = 'Admin/Admin_dvd_controller/view/$1';
$route['staff/actor/(:num)'] = 'Staff/Staff_actor_controller/index/$1';
$route['staff/actor'] = 'Staff/Staff_actor_controller/index/0';
$route['staff/actor/add'] = 'Staff/Staff_actor_controller/add';
$route['staff/actor/edit/(:num)'] = 'Staff/Staff_actor_controller/edit/$1';
$route['staff/actor/view/(:num)'] = 'Staff/Staff_actor_controller/view/$1';
$route['staff/actor/delete/(:num)'] = 'Staff/Staff_actor_controller/delete/$1';
$route['v1/api/staff/actor/(:num)'] = 'Staff/Staff_actor_api_controller/index/$1';
$route['v1/api/staff/actor'] = 'Staff/Staff_actor_api_controller/index/0';
$route['v1/api/staff/actor/add'] = 'Staff/Staff_actor_api_controller/add';
$route['v1/api/staff/actor/edit/(:num)'] = 'Staff/Staff_actor_api_controller/edit/$1';
$route['v1/api/staff/actor/view/(:num)'] = 'Staff/Staff_actor_api_controller/view/$1';
$route['v1/api/staff/actor/delete/(:num)'] = 'Staff/Staff_actor_api_controller/delete/$1';
$route['staff/category/(:num)'] = 'Staff/Staff_category_controller/index/$1';
$route['staff/category'] = 'Staff/Staff_category_controller/index/0';
$route['staff/category/add'] = 'Staff/Staff_category_controller/add';
$route['staff/category/edit/(:num)'] = 'Staff/Staff_category_controller/edit/$1';
$route['staff/studio/(:num)'] = 'Staff/Staff_studio_controller/index/$1';
$route['staff/studio'] = 'Staff/Staff_studio_controller/index/0';
$route['staff/studio/add'] = 'Staff/Staff_studio_controller/add';
$route['staff/studio/edit/(:num)'] = 'Staff/Staff_studio_controller/edit/$1';
$route['staff/type/(:num)'] = 'Staff/Staff_type_controller/index/$1';
$route['staff/type'] = 'Staff/Staff_type_controller/index/0';
$route['staff/type/add'] = 'Staff/Staff_type_controller/add';
$route['staff/type/edit/(:num)'] = 'Staff/Staff_type_controller/edit/$1';
$route['staff/group/(:num)'] = 'Staff/Staff_group_controller/index/$1';
$route['staff/group'] = 'Staff/Staff_group_controller/index/0';
$route['staff/group/add'] = 'Staff/Staff_group_controller/add';
$route['staff/group/edit/(:num)'] = 'Staff/Staff_group_controller/edit/$1';
$route['staff/network/(:num)'] = 'Staff/Staff_network_controller/index/$1';
$route['staff/network'] = 'Staff/Staff_network_controller/index/0';
$route['staff/network/add'] = 'Staff/Staff_network_controller/add';
$route['staff/network/edit/(:num)'] = 'Staff/Staff_network_controller/edit/$1';
$route['staff/company/(:num)'] = 'Staff/Staff_company_controller/index/$1';
$route['staff/company'] = 'Staff/Staff_company_controller/index/0';
$route['staff/company/add'] = 'Staff/Staff_company_controller/add';
$route['staff/company/edit/(:num)'] = 'Staff/Staff_company_controller/edit/$1';
$route['staff/company/view/(:num)'] = 'Staff/Staff_company_controller/view/$1';
$route['staff/vodcompany/(:num)'] = 'Staff/Staff_vodcompany_controller/index/$1';
$route['staff/vodcompany'] = 'Staff/Staff_vodcompany_controller/index/0';
$route['staff/vodcompany/add'] = 'Staff/Staff_vodcompany_controller/add';
$route['staff/vodcompany/edit/(:num)'] = 'Staff/Staff_vodcompany_controller/edit/$1';
$route['staff/broadcastcompany/(:num)'] = 'Staff/Staff_broadcastcompany_controller/index/$1';
$route['staff/broadcastcompany'] = 'Staff/Staff_broadcastcompany_controller/index/0';
$route['staff/broadcastcompany/add'] = 'Staff/Staff_broadcastcompany_controller/add';
$route['staff/broadcastcompany/edit/(:num)'] = 'Staff/Staff_broadcastcompany_controller/edit/$1';
$route['staff/tubesite/(:num)'] = 'Staff/Staff_tubesite_controller/index/$1';
$route['staff/tubesite'] = 'Staff/Staff_tubesite_controller/index/0';
$route['staff/tubesite/add'] = 'Staff/Staff_tubesite_controller/add';
$route['staff/tubesite/edit/(:num)'] = 'Staff/Staff_tubesite_controller/edit/$1';
$route['staff/viewsharesite/(:num)'] = 'Staff/Staff_viewsharesite_controller/index/$1';
$route['staff/viewsharesite'] = 'Staff/Staff_viewsharesite_controller/index/0';
$route['staff/viewsharesite/add'] = 'Staff/Staff_viewsharesite_controller/add';
$route['staff/viewsharesite/edit/(:num)'] = 'Staff/Staff_viewsharesite_controller/edit/$1';
$route['staff/producer/(:num)'] = 'Staff/Staff_producer_controller/index/$1';
$route['staff/producer'] = 'Staff/Staff_producer_controller/index/0';
$route['staff/producer/add'] = 'Staff/Staff_producer_controller/add';
$route['staff/producer/edit/(:num)'] = 'Staff/Staff_producer_controller/edit/$1';
$route['staff/scenes/(:num)'] = 'Staff/Staff_scene_controller/index/$1';
$route['staff/scenes'] = 'Staff/Staff_scene_controller/index/0';
$route['staff/scenes/add'] = 'Staff/Staff_scene_controller/add';
$route['staff/scenes/edit/(:num)'] = 'Staff/Staff_scene_controller/edit/$1';
$route['staff/scenes/view/(:num)'] = 'Staff/Staff_scene_controller/view/$1';
$route['staff/dvds/(:num)'] = 'Staff/Staff_dvd_controller/index/$1';
$route['staff/dvds'] = 'Staff/Staff_dvd_controller/index/0';
$route['staff/dvds/add'] = 'Staff/Staff_dvd_controller/add';
$route['staff/dvds/edit/(:num)'] = 'Staff/Staff_dvd_controller/edit/$1';
$route['staff/dvds/view/(:num)'] = 'Staff/Staff_dvd_controller/view/$1';

$route['404_override'] = '';
$route['translate_uri_dashes'] = false;
$route['default_controller'] = 'Welcome/index';

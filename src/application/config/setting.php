<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/*
| -------------------------------------------------------------------
| MIME TYPES
| -------------------------------------------------------------------
| This file contains an array of mime types.  It is used by the
| Upload class to help identify allowed file types.
|
*/
$config['setting'] = array(
	'site_name' => 'Puba LLC',
	'site_logo' => 'https://manaknightdigital.com/assets/img/logo.png',
	'maintenance' => '0',
	'version' => '0.0.1',
	'copyright' => 'Copyright © 2019 Puba LLC. All rights reserved.',
	'license_key' => '4097fbd4f340955de76ca555c201b185cf9d6921d977301b05cdddeae4af54f924f0508cd0f7ca66',

);
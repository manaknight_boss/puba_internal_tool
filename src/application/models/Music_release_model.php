<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Music_release_model Model
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Music_release_model extends Manaknight_Model
{
	protected $_table = 'music_release';
	protected $_primary_key = 'id';
	protected $_return_type = 'array';
	protected $_allowed_fields = [
    'signer_name',
		'file_name',
		'image',
		'image_id',
		'dynamic_tags',
		'legal_text',
		
    ];
	protected $_label_fields = [
    'Signer Name','File Name','Signature','Image ID','Dynamic Tags','Legal Text',
    ];
	protected $_use_timestamps = TRUE;
	protected $_created_field = 'created_at';
	protected $_updated_field = 'updated_at';
	protected $_validation_rules = [
    ['signer_name', 'Signer Name', 'required'],
		['file_name', 'File Name', 'required'],
		['image', 'Signature', 'required'],
		['image_id', 'Image ID', ''],
		['dynamic_tags', 'Dynamic Tags', 'required'],
		['legal_text', 'Legal Text', 'required'],
		
    ];
	protected $_validation_edit_rules = [
    ['signer_name', 'Signer Name', 'required'],
		['file_name', 'File Name', 'required'],
		['image', 'Signature', 'required'],
		['image_id', 'Image ID', ''],
		['dynamic_tags', 'Dynamic Tags', ''],
		['legal_text', 'Legal Text', 'required'],
		
    ];
	protected $_validation_messages = [

    ];

	public function __construct()
	{
		parent::__construct();
	}

    /**
     * If you need to modify payload before create, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _pre_create_processing($data)
    {
        
        return $data;
    }

    /**
     * If you need to modify payload before edit, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _post_edit_processing($data)
    {
        if(isset($data['slug']))
		{
			unset($data['slug']);
		}

        return $data;
    }

    /**
     * Allow user to add extra counting condition so user don't have to change main function
     *
     * @param mixed $parameters
     * @return $db
     */
    protected function _custom_counting_conditions(&$db)
    {
        
        return $db;
    }


	public function get_template($slug,$data)
	{
		$this->db->from('email');
		$this->db->where('slug',$slug,TRUE);
		$template=$this->db->get()->row();
		if(!$template)
		{
			return FALSE;
		}
		$tags_raw=$template->tag;
		$tags=explode(',',$tags_raw);
		$template->subject=$this->inject_substitute($template->subject,$tags,$data);
		$template->html=$this->inject_substitute($template->html,$tags,$data);
		return $template;
	}

	public function inject_substitute($raw, $tags, $data) 
	{
		foreach ($data as $key => $value) 
		{
			if (in_array($key, $tags))
			{
				$raw = str_replace('{{{' . $key . '}}}', $value, $raw);
			}
		}
		return $raw;
	}


}
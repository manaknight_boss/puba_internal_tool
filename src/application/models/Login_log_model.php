<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Login_log_model Model
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Login_log_model extends Manaknight_Model
{
	protected $_table = 'login_log';
	protected $_primary_key = 'id';
	protected $_return_type = 'array';
	protected $_allowed_fields = [
    'user_id',
		'ip',
		'user_agent',
		
    ];
	protected $_label_fields = [
    'User','IP','User Agent',
    ];
	protected $_use_timestamps = TRUE;
	protected $_created_field = 'created_at';
	protected $_updated_field = 'updated_at';
	protected $_validation_rules = [
    ['user_id', 'User', 'required|integer'],
		['ip', 'IP', ''],
		['user_agent', 'User Agent', 'required'],
		
    ];
	protected $_validation_edit_rules = [
    ['user_id', 'User', ''],
		['ip', 'IP', ''],
		['user_agent', 'User Agent', ''],
		
    ];
	protected $_validation_messages = [

    ];

	public function __construct()
	{
		parent::__construct();
	}

    /**
     * If you need to modify payload before create, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _pre_create_processing($data)
    {
        $data['last_ip'] = $this->get_ip();
		$data['last_ip'] = $this->get_user_agent();

        return $data;
    }

    /**
     * If you need to modify payload before edit, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _post_edit_processing($data)
    {
        
        return $data;
    }

    /**
     * Allow user to add extra counting condition so user don't have to change main function
     *
     * @param mixed $parameters
     * @return $db
     */
    protected function _custom_counting_conditions(&$db)
    {
        
        return $db;
    }


	function get_ip()
 	{
 		if(!empty($_SERVER['HTTP_CLIENT_IP']))
 		{
 			$ip = $_SERVER['HTTP_CLIENT_IP'];
 		}
 		elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
 		{
 			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
 		}
 		else
 		{
 			$ip = $_SERVER['REMOTE_ADDR'];
 		}
 		return $ip;
 	}

	function get_user_agent()
 	{
 		return $_SERVER['HTTP_USER_AGENT'];
 	}

	public function get_user ($where)
	{
		return $this->_join ('user', 'user_id', $where);
	}

	public function get_user_paginated ($page, $limit, $where)
	{
		return $this->_join_paginate ('user', 'user_id', $where, $page, $limit);
	}


}
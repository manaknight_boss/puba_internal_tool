<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Actor_model Model
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Actor_model extends Manaknight_Model
{
	protected $_table = 'actor';
	protected $_primary_key = 'id';
	protected $_return_type = 'array';
	protected $_allowed_fields = [
    'id',
		'gender',
		'name',
		'stage_name',
		'dob',
		'expiry_date',
		'status',
		'image',
		'image_id',
		
    ];
	protected $_label_fields = [
    'ID','Gender','Name','Stage Name','Date of Birth','Expiry Date','Status','Upload ID','File ID',
    ];
	protected $_use_timestamps = TRUE;
	protected $_created_field = 'created_at';
	protected $_updated_field = 'updated_at';
	protected $_validation_rules = [
    ['id', 'ID', ''],
		['gender', 'Gender', 'required'],
		['name', 'Name', 'required'],
		['stage_name', 'Stage Name', 'required'],
		['dob', 'Date of Birth', 'required|date'],
		['expiry_date', 'Expiry Date', 'required|date'],
		['status', 'Status', ''],
		['image', 'Upload ID', 'required'],
		['image_id', 'File ID', ''],
		
    ];
	protected $_validation_edit_rules = [
    ['id', 'ID', ''],
		['gender', 'Gender', 'required'],
		['name', 'Name', 'required'],
		['stage_name', 'Stage Name', 'required'],
		['dob', 'Date of Birth', 'required|date'],
		['expiry_date', 'Expiry Date', 'required|date'],
		['status', 'Status', 'required|in_list[0,1,2]'],
		['image', 'Upload ID', 'required'],
		['image_id', 'File ID', ''],
		
    ];
	protected $_validation_messages = [

    ];

	public function __construct()
	{
		parent::__construct();
	}

    /**
     * If you need to modify payload before create, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _pre_create_processing($data)
    {
        $data['status'] = 1;

        return $data;
    }

    /**
     * If you need to modify payload before edit, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _post_edit_processing($data)
    {
        
        return $data;
    }

    /**
     * Allow user to add extra counting condition so user don't have to change main function
     *
     * @param mixed $parameters
     * @return $db
     */
    protected function _custom_counting_conditions(&$db)
    {
        
        return $db;
    }


	public function gender_mapping ()
	{
		return [
			0 => 'female',
			1 => 'male',
			3 => 'Unknown',
		];
	}

	public function status_mapping ()
	{
		return [
			0 => 'inactive',
			1 => 'active',
		];
	}



}
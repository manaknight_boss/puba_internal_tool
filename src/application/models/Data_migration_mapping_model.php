<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Data_migration_mapping_model Model
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Data_migration_mapping_model extends Manaknight_Model
{
	protected $_table = 'data_migration_mapping';
	protected $_primary_key = 'id';
	protected $_return_type = 'array';
	protected $_allowed_fields = [
    'id',
		'old_id',
		'new_id',
		'source_table',
		'destination_table',
		
    ];
	protected $_label_fields = [
    'ID','Old Id','New Id','Source table','Destination table',
    ];
	protected $_use_timestamps = TRUE;
	protected $_created_field = 'created_at';
	protected $_updated_field = 'updated_at';
	protected $_validation_rules = [
    ['id', 'ID', ''],
		['old_id', 'Old Id', ''],
		['new_id', 'New Id', ''],
		['source_table', 'Source table', 'required|max[255]'],
		['destination_table', 'Destination table', 'required|max[255]'],
		
    ];
	protected $_validation_edit_rules = [
    ['id', 'ID', ''],
		['old_id', 'Old Id', 'required'],
		['new_id', 'New Id', 'required'],
		['source_table', 'Source table', 'required|max[255]'],
		['destination_table', 'Destination table', 'required|max[255]'],
		
    ];
	protected $_validation_messages = [

    ];

	public function __construct()
	{
		parent::__construct();
	}

    /**
     * If you need to modify payload before create, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _pre_create_processing($data)
    {
        
        return $data;
    }

    /**
     * If you need to modify payload before edit, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _post_edit_processing($data)
    {
        
        return $data;
    }

    /**
     * Allow user to add extra counting condition so user don't have to change main function
     *
     * @param mixed $parameters
     * @return $db
     */
    protected function _custom_counting_conditions(&$db)
    {
        
        return $db;
    }


	public function batch_insert($params)
	{
		 return $this->db->insert_batch($this->_table,$params);
	}

	public function get_by_scene($scene_id)
	{
		$this->db->where('scene_id',$scene_id);
		return $this->db->get($this->_table)->result_array();
	}

	public function delete_by_scene($scene_id)
	{
		$this->db->where('scene_id',$scene_id);
		return $this->db->delete($this->_table); 
	 }


}
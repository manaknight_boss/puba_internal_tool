<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Producer_model Model
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 *
 */
class Producer_model extends Manaknight_Model
{
	protected $_table = 'producer';
	protected $_primary_key = 'id';
	protected $_return_type = 'array';
	protected $_allowed_fields = [
    'id',
		'name',
		'contact_name',
		'address',
		'email',
		'url',
		'note',
		'phone',
		'status',
		
    ];
	protected $_label_fields = [
    'ID','Name','Contact Name','Address','Email','Url','Note','Name','Status',
    ];
	protected $_use_timestamps = TRUE;
	protected $_created_field = 'created_at';
	protected $_updated_field = 'updated_at';
	protected $_validation_rules = [
    ['id', 'ID', ''],
		['name', 'Name', 'required'],
		['contact_name', 'Contact Name', 'required'],
		['address', 'Address', 'required'],
		['email', 'Email', 'trim|required|valid_email|is_unique[user.email]'],
		['url', 'Url', 'required'],
		['note', 'Note', ''],
		['phone', 'Name', ''],
		['status', 'Status', ''],
		
    ];
	protected $_validation_edit_rules = [
    ['id', 'ID', ''],
		['name', 'Name', 'required'],
		['contact_name', 'Contact Name', 'required'],
		['address', 'Address', 'required'],
		['email', 'Email', 'trim|required|valid_email'],
		['url', 'Url', ''],
		['note', 'Note', ''],
		['phone', 'Name', ''],
		['status', 'Status', 'required'],
		
    ];
	protected $_validation_messages = [

    ];

	public function __construct()
	{
		parent::__construct();
	}

    /**
     * If you need to modify payload before create, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _pre_create_processing($data)
    {
        
        return $data;
    }

    /**
     * If you need to modify payload before edit, overload this function
     *
     * @param mixed $data
     * @return mixed
     */
    protected function _post_edit_processing($data)
    {
        
        return $data;
    }

    /**
     * Allow user to add extra counting condition so user don't have to change main function
     *
     * @param mixed $parameters
     * @return $db
     */
    protected function _custom_counting_conditions(&$db)
    {
        
        return $db;
    }


	public function status_mapping ()
	{
		return [
			0 => 'inactive',
			1 => 'active',
		];
	}



}
<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Edit Producers View Model
 *
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 */
class Producers_admin_edit_view_model
{
    protected $_entity;
	protected $_id;
	protected $_name;
	protected $_address;
	protected $_phone;
	protected $_email;
	protected $_url;
	protected $_contact_name;
	protected $_status;
	protected $_note;


    public function __construct($entity)
    {
        $this->_entity = $entity;
    }

    public function get_entity ()
    {
        return $this->_entity;
    }

    /**
     * set_heading function
     *
     * @param string $heading
     * @return void
     */
    public function set_heading ($heading)
    {
        $this->_heading = $heading;
    }

    /**
     * get_heading function
     *
     * @return string
     */
    public function get_heading ()
    {
        return $this->_heading;
    }

    public function set_model ($model)
    {
        $this->_model = $model;
		$this->_id = $model->id;
		$this->_name = $model->name;
		$this->_address = $model->address;
		$this->_phone = $model->phone;
		$this->_email = $model->email;
		$this->_url = $model->url;
		$this->_contact_name = $model->contact_name;
		$this->_status = $model->status;
		$this->_note = $model->note;

    }

	public function status_mapping ()
	{
		return $this->_entity->status_mapping();

	}

	public function get_name ()
	{
		return $this->_name;
	}

	public function set_name ($name)
	{
		$this->_name = $name;
	}

	public function get_address ()
	{
		return $this->_address;
	}

	public function set_address ($address)
	{
		$this->_address = $address;
	}

	public function get_phone ()
	{
		return $this->_phone;
	}

	public function set_phone ($phone)
	{
		$this->_phone = $phone;
	}

	public function get_email ()
	{
		return $this->_email;
	}

	public function set_email ($email)
	{
		$this->_email = $email;
	}

	public function get_url ()
	{
		return $this->_url;
	}

	public function set_url ($url)
	{
		$this->_url = $url;
	}

	public function get_contact_name ()
	{
		return $this->_contact_name;
	}

	public function set_contact_name ($contact_name)
	{
		$this->_contact_name = $contact_name;
	}

	public function get_status ()
	{
		return $this->_status;
	}

	public function set_status ($status)
	{
		$this->_status = $status;
	}

	public function get_note ()
	{
		return $this->_note;
	}

	public function set_note ($note)
	{
		$this->_note = $note;
	}

	public function get_id ()
	{
		return $this->_id;
	}

	public function set_id ($id)
	{
		$this->_id = $id;
	}

}
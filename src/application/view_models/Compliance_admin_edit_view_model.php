<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Edit Compliance View Model
 *
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 */
class Compliance_admin_edit_view_model
{
    protected $_entity;
	protected $_id;
	protected $_legal_text;
	protected $_image;
	protected $_image_id;
	protected $_signer_name;
	protected $_file_name;
	protected $_file_name_id;


    public function __construct($entity)
    {
        $this->_entity = $entity;
    }

    public function get_entity ()
    {
        return $this->_entity;
    }

    /**
     * set_heading function
     *
     * @param string $heading
     * @return void
     */
    public function set_heading ($heading)
    {
        $this->_heading = $heading;
    }

    /**
     * get_heading function
     *
     * @return string
     */
    public function get_heading ()
    {
        return $this->_heading;
    }

    public function set_model ($model)
    {
        $this->_model = $model;
		$this->_id = $model->id;
		$this->_legal_text = $model->legal_text;
		$this->_image = $model->image;
		$this->_signer_name = $model->signer_name;
		$this->_file_name = $model->file_name;

    }

	public function get_legal_text ()
	{
		return $this->_legal_text;
	}

	public function set_legal_text ($legal_text)
	{
		$this->_legal_text = $legal_text;
	}

	public function get_image ()
	{
		return $this->_image;
	}

	public function set_image ($image)
	{
		$this->_image = $image;
	}

	public function get_image_id ()
	{
		return $this->_image_id;
	}

	public function set_image_id ($image)
	{
		$this->_image_id = $image;
	}

	public function get_signer_name ()
	{
		return $this->_signer_name;
	}

	public function set_signer_name ($signer_name)
	{
		$this->_signer_name = $signer_name;
	}

	public function get_file_name ()
	{
		return $this->_file_name;
	}

	public function set_file_name ($file_name)
	{
		$this->_file_name = $file_name;
	}

	public function get_file_name_id ()
	{
		return $this->_file_name_id;
	}

	public function set_file_name_id ($file_name)
	{
		$this->_file_name_id = $file_name;
	}

	public function get_id ()
	{
		return $this->_id;
	}

	public function set_id ($id)
	{
		$this->_id = $id;
	}

}
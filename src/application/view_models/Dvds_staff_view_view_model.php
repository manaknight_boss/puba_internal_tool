<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * View Dvds View Model
 *
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 */
class Dvds_staff_view_view_model
{
    protected $_entity;
    protected $_model;
	protected $_id;
	protected $_title;
	protected $_status;
	protected $_studio_id;
	protected $_release_date;
	protected $_company_id;
	protected $_type_id;
	protected $_filter_after;
	protected $_start_production_date;
	protected $_end_production_date;
	protected $_cover_image;
	protected $_cover_image_id;
	protected $_is_for_comp;


    public function __construct($entity)
    {
        $this->_entity = $entity;
    }

    public function get_entity ()
    {
        return $this->_entity;
    }

    /**
     * set_heading function
     *
     * @param string $heading
     * @return void
     */
    public function set_heading ($heading)
    {
        $this->_heading = $heading;
    }

    /**
     * get_heading function
     *
     * @return string
     */
    public function get_heading ()
    {
        return $this->_heading;
    }

    public function set_model ($model)
    {
        $this->_model = $model;
		$this->_id = $model->id;
		$this->_title = $model->title;
		$this->_status = $model->status;
		$this->_studio_id = $model->studio_id;
		$this->_release_date = $model->release_date;
		$this->_company_id = $model->company_id;
		$this->_type_id = $model->type_id;
		$this->_filter_after = $model->filter_after;
		$this->_start_production_date = $model->start_production_date;
		$this->_end_production_date = $model->end_production_date;
		$this->_cover_image = $model->cover_image;
		$this->_is_for_comp = $model->is_for_comp;

    }

	public function status_mapping ()
	{
		return $this->_entity->status_mapping();

	}

	public function is_for_comp_mapping ()
	{
		return $this->_entity->is_for_comp_mapping();

	}

	public function get_title ()
	{
		return $this->_title;
	}

	public function set_title ($title)
	{
		$this->_title = $title;
	}

	public function get_status ()
	{
		return $this->_status;
	}

	public function set_status ($status)
	{
		$this->_status = $status;
	}

	public function get_studio_id ()
	{
		return $this->_studio_id;
	}

	public function set_studio_id ($studio_id)
	{
		$this->_studio_id = $studio_id;
	}

	public function get_release_date ()
	{
		return $this->_release_date;
	}

	public function set_release_date ($release_date)
	{
		$this->_release_date = $release_date;
	}

	public function get_company_id ()
	{
		return $this->_company_id;
	}

	public function set_company_id ($company_id)
	{
		$this->_company_id = $company_id;
	}

	public function get_type_id ()
	{
		return $this->_type_id;
	}

	public function set_type_id ($type_id)
	{
		$this->_type_id = $type_id;
	}

	public function get_filter_after ()
	{
		return $this->_filter_after;
	}

	public function set_filter_after ($filter_after)
	{
		$this->_filter_after = $filter_after;
	}

	public function get_start_production_date ()
	{
		return $this->_start_production_date;
	}

	public function set_start_production_date ($start_production_date)
	{
		$this->_start_production_date = $start_production_date;
	}

	public function get_end_production_date ()
	{
		return $this->_end_production_date;
	}

	public function set_end_production_date ($end_production_date)
	{
		$this->_end_production_date = $end_production_date;
	}

	public function get_cover_image ()
	{
		return $this->_cover_image;
	}

	public function set_cover_image ($cover_image)
	{
		$this->_cover_image = $cover_image;
	}

	public function get_cover_image_id ()
	{
		return $this->_cover_image_id;
	}

	public function set_cover_image_id ($cover_image)
	{
		$this->_cover_image_id = $cover_image;
	}

	public function get_is_for_comp ()
	{
		return $this->_is_for_comp;
	}

	public function set_is_for_comp ($is_for_comp)
	{
		$this->_is_for_comp = $is_for_comp;
	}

	public function get_id ()
	{
		return $this->_id;
	}

	public function set_id ($id)
	{
		$this->_id = $id;
	}

	public function to_json ()
	{
		return [
		'title' => $this->get_title(),
		'status' => $this->get_status(),
		'studio_id' => $this->get_studio_id(),
		'release_date' => $this->get_release_date(),
		'company_id' => $this->get_company_id(),
		'type_id' => $this->get_type_id(),
		'filter_after' => $this->get_filter_after(),
		'start_production_date' => $this->get_start_production_date(),
		'end_production_date' => $this->get_end_production_date(),
		'cover_image' => $this->get_cover_image(),
		'is_for_comp' => $this->get_is_for_comp(),
		];
	}

}
<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Edit Scenes View Model
 *
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 */
class Scenes_admin_edit_view_model
{
    protected $_entity;
	protected $_id;
	protected $_title;
	protected $_date_released;
	protected $_tubeclip_url;
	protected $_next_studio_id;
	protected $_xvideos_url;
	protected $_type;
	protected $_primary_producer_id;
	protected $_group_id;
	protected $_channel;
	protected $_set_id;
	protected $_tss_url;
	protected $_status;
	protected $_media_type;
	protected $_studio_id;
	protected $_is_for_comp;


    public function __construct($entity)
    {
        $this->_entity = $entity;
    }

    public function get_entity ()
    {
        return $this->_entity;
    }

    /**
     * set_heading function
     *
     * @param string $heading
     * @return void
     */
    public function set_heading ($heading)
    {
        $this->_heading = $heading;
    }

    /**
     * get_heading function
     *
     * @return string
     */
    public function get_heading ()
    {
        return $this->_heading;
    }

    public function set_model ($model)
    {
        $this->_model = $model;
		$this->_id = $model->id;
		$this->_title = $model->title;
		$this->_date_released = $model->date_released;
		$this->_tubeclip_url = $model->tubeclip_url;
		$this->_next_studio_id = $model->next_studio_id;
		$this->_xvideos_url = $model->xvideos_url;
		$this->_type = $model->type;
		$this->_primary_producer_id = $model->primary_producer_id;
		$this->_group_id = $model->group_id;
		$this->_channel = $model->channel;
		$this->_set_id = $model->set_id;
		$this->_tss_url = $model->tss_url;
		$this->_status = $model->status;
		$this->_media_type = $model->media_type;
		$this->_studio_id = $model->studio_id;
		$this->_is_for_comp = $model->is_for_comp;

    }

	public function status_mapping ()
	{
		return $this->_entity->status_mapping();

	}

	public function is_for_comp_mapping ()
	{
		return $this->_entity->is_for_comp_mapping();

	}

	public function get_title ()
	{
		return $this->_title;
	}

	public function set_title ($title)
	{
		$this->_title = $title;
	}

	public function get_date_released ()
	{
		return $this->_date_released;
	}

	public function set_date_released ($date_released)
	{
		$this->_date_released = $date_released;
	}

	public function get_tubeclip_url ()
	{
		return $this->_tubeclip_url;
	}

	public function set_tubeclip_url ($tubeclip_url)
	{
		$this->_tubeclip_url = $tubeclip_url;
	}

	public function get_next_studio_id ()
	{
		return $this->_next_studio_id;
	}

	public function set_next_studio_id ($next_studio_id)
	{
		$this->_next_studio_id = $next_studio_id;
	}

	public function get_xvideos_url ()
	{
		return $this->_xvideos_url;
	}

	public function set_xvideos_url ($xvideos_url)
	{
		$this->_xvideos_url = $xvideos_url;
	}

	public function get_type ()
	{
		return $this->_type;
	}

	public function set_type ($type)
	{
		$this->_type = $type;
	}

	public function get_primary_producer_id ()
	{
		return $this->_primary_producer_id;
	}

	public function set_primary_producer_id ($primary_producer_id)
	{
		$this->_primary_producer_id = $primary_producer_id;
	}

	public function get_group_id ()
	{
		return $this->_group_id;
	}

	public function set_group_id ($group_id)
	{
		$this->_group_id = $group_id;
	}

	public function get_channel ()
	{
		return $this->_channel;
	}

	public function set_channel ($channel)
	{
		$this->_channel = $channel;
	}

	public function get_set_id ()
	{
		return $this->_set_id;
	}

	public function set_set_id ($set_id)
	{
		$this->_set_id = $set_id;
	}

	public function get_tss_url ()
	{
		return $this->_tss_url;
	}

	public function set_tss_url ($tss_url)
	{
		$this->_tss_url = $tss_url;
	}

	public function get_status ()
	{
		return $this->_status;
	}

	public function set_status ($status)
	{
		$this->_status = $status;
	}

	public function get_media_type ()
	{
		return $this->_media_type;
	}

	public function set_media_type ($media_type)
	{
		$this->_media_type = $media_type;
	}

	public function get_studio_id ()
	{
		return $this->_studio_id;
	}

	public function set_studio_id ($studio_id)
	{
		$this->_studio_id = $studio_id;
	}

	public function get_is_for_comp ()
	{
		return $this->_is_for_comp;
	}

	public function set_is_for_comp ($is_for_comp)
	{
		$this->_is_for_comp = $is_for_comp;
	}

	public function get_id ()
	{
		return $this->_id;
	}

	public function set_id ($id)
	{
		$this->_id = $id;
	}

}
<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Edit Studios View Model
 *
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 */
class Studios_staff_edit_view_model
{
    protected $_entity;
	protected $_id;
	protected $_name;
	protected $_company_id;
	protected $_status;


    public function __construct($entity)
    {
        $this->_entity = $entity;
    }

    public function get_entity ()
    {
        return $this->_entity;
    }

    /**
     * set_heading function
     *
     * @param string $heading
     * @return void
     */
    public function set_heading ($heading)
    {
        $this->_heading = $heading;
    }

    /**
     * get_heading function
     *
     * @return string
     */
    public function get_heading ()
    {
        return $this->_heading;
    }

    public function set_model ($model)
    {
        $this->_model = $model;
		$this->_id = $model->id;
		$this->_name = $model->name;
		$this->_company_id = $model->company_id;
		$this->_status = $model->status;

    }

	public function status_mapping ()
	{
		return $this->_entity->status_mapping();

	}

	public function get_name ()
	{
		return $this->_name;
	}

	public function set_name ($name)
	{
		$this->_name = $name;
	}

	public function get_company_id ()
	{
		return $this->_company_id;
	}

	public function set_company_id ($company_id)
	{
		$this->_company_id = $company_id;
	}

	public function get_status ()
	{
		return $this->_status;
	}

	public function set_status ($status)
	{
		$this->_status = $status;
	}

	public function get_id ()
	{
		return $this->_id;
	}

	public function set_id ($id)
	{
		$this->_id = $id;
	}

}
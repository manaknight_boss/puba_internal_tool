<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * View Actor View Model
 *
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 */
class Actor_staff_view_view_model
{
    protected $_entity;
    protected $_model;
	protected $_id;
	protected $_name;
	protected $_stage_name;
	protected $_gender;
	protected $_image;
	protected $_image_id;
	protected $_dob;
	protected $_expiry_date;
	protected $_status;


    public function __construct($entity)
    {
        $this->_entity = $entity;
    }

    public function get_entity ()
    {
        return $this->_entity;
    }

    /**
     * set_heading function
     *
     * @param string $heading
     * @return void
     */
    public function set_heading ($heading)
    {
        $this->_heading = $heading;
    }

    /**
     * get_heading function
     *
     * @return string
     */
    public function get_heading ()
    {
        return $this->_heading;
    }

    public function set_model ($model)
    {
        $this->_model = $model;
		$this->_id = $model->id;
		$this->_name = $model->name;
		$this->_stage_name = $model->stage_name;
		$this->_gender = $model->gender;
		$this->_image = $model->image;
		$this->_dob = $model->dob;
		$this->_expiry_date = $model->expiry_date;
		$this->_status = $model->status;

    }

	public function gender_mapping ()
	{
		return $this->_entity->gender_mapping();

	}

	public function status_mapping ()
	{
		return $this->_entity->status_mapping();

	}

	public function get_name ()
	{
		return $this->_name;
	}

	public function set_name ($name)
	{
		$this->_name = $name;
	}

	public function get_stage_name ()
	{
		return $this->_stage_name;
	}

	public function set_stage_name ($stage_name)
	{
		$this->_stage_name = $stage_name;
	}

	public function get_gender ()
	{
		return $this->_gender;
	}

	public function set_gender ($gender)
	{
		$this->_gender = $gender;
	}

	public function get_image ()
	{
		return $this->_image;
	}

	public function set_image ($image)
	{
		$this->_image = $image;
	}

	public function get_image_id ()
	{
		return $this->_image_id;
	}

	public function set_image_id ($image)
	{
		$this->_image_id = $image;
	}

	public function get_dob ()
	{
		return $this->_dob;
	}

	public function set_dob ($dob)
	{
		$this->_dob = $dob;
	}

	public function get_expiry_date ()
	{
		return $this->_expiry_date;
	}

	public function set_expiry_date ($expiry_date)
	{
		$this->_expiry_date = $expiry_date;
	}

	public function get_status ()
	{
		return $this->_status;
	}

	public function set_status ($status)
	{
		$this->_status = $status;
	}

	public function get_id ()
	{
		return $this->_id;
	}

	public function set_id ($id)
	{
		$this->_id = $id;
	}

	public function to_json ()
	{
		return [
		'name' => $this->get_name(),
		'stage_name' => $this->get_stage_name(),
		'gender' => $this->get_gender(),
		'image' => $this->get_image(),
		'dob' => $this->get_dob(),
		'expiry_date' => $this->get_expiry_date(),
		'status' => $this->get_status(),
		];
	}

}
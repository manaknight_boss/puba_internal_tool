<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * View Image View Model
 *
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 */
class Image_admin_view_view_model
{
    protected $_entity;
    protected $_model;
	protected $_id;
	protected $_url;
	protected $_type;


    public function __construct($entity)
    {
        $this->_entity = $entity;
    }

    public function get_entity ()
    {
        return $this->_entity;
    }

    /**
     * set_heading function
     *
     * @param string $heading
     * @return void
     */
    public function set_heading ($heading)
    {
        $this->_heading = $heading;
    }

    /**
     * get_heading function
     *
     * @return string
     */
    public function get_heading ()
    {
        return $this->_heading;
    }

    public function set_model ($model)
    {
        $this->_model = $model;
		$this->_id = $model->id;
		$this->_url = $model->url;
		$this->_type = $model->type;

    }

	public function type_mapping ()
	{
		return $this->_entity->type_mapping();

	}

	public function get_url ()
	{
		return $this->_url;
	}

	public function set_url ($url)
	{
		$this->_url = $url;
	}

	public function get_type ()
	{
		return $this->_type;
	}

	public function set_type ($type)
	{
		$this->_type = $type;
	}

	public function get_id ()
	{
		return $this->_id;
	}

	public function set_id ($id)
	{
		$this->_id = $id;
	}

	public function to_json ()
	{
		return [
		'url' => $this->get_url(),
		'type' => $this->get_type(),
		];
	}

}
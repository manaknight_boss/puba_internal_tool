<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * Edit Company View Model
 *
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 */
class Company_staff_edit_view_model
{
    protected $_entity;
	protected $_id;
	protected $_name;
	protected $_address;
	protected $_address_2257;
	protected $_status;


    public function __construct($entity)
    {
        $this->_entity = $entity;
    }

    public function get_entity ()
    {
        return $this->_entity;
    }

    /**
     * set_heading function
     *
     * @param string $heading
     * @return void
     */
    public function set_heading ($heading)
    {
        $this->_heading = $heading;
    }

    /**
     * get_heading function
     *
     * @return string
     */
    public function get_heading ()
    {
        return $this->_heading;
    }

    public function set_model ($model)
    {
        $this->_model = $model;
		$this->_id = $model->id;
		$this->_name = $model->name;
		$this->_address = $model->address;
		$this->_address_2257 = $model->address_2257;
		$this->_status = $model->status;

    }

	public function status_mapping ()
	{
		return $this->_entity->status_mapping();

	}

	public function get_name ()
	{
		return $this->_name;
	}

	public function set_name ($name)
	{
		$this->_name = $name;
	}

	public function get_address ()
	{
		return $this->_address;
	}

	public function set_address ($address)
	{
		$this->_address = $address;
	}

	public function get_address_2257 ()
	{
		return $this->_address_2257;
	}

	public function set_address_2257 ($address_2257)
	{
		$this->_address_2257 = $address_2257;
	}

	public function get_status ()
	{
		return $this->_status;
	}

	public function set_status ($status)
	{
		$this->_status = $status;
	}

	public function get_id ()
	{
		return $this->_id;
	}

	public function set_id ($id)
	{
		$this->_id = $id;
	}

}
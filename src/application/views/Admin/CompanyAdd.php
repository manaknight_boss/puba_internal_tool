<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
?>
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="page-header">
			<h2 class="pageheader-title"><?php echo $view_model->get_heading();?> </h2>
			<div class="page-breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/admin/dashboard" class="breadcrumb-link">Dashboard</a></li>
						<li class="breadcrumb-item"><a href="/admin/company/0" class="breadcrumb-link"><?php echo $view_model->get_heading();?></a></li>
						<li class="breadcrumb-item active" aria-current="page">Add</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</div>
<div class="row">
    <?php if (validation_errors()) : ?>
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?= validation_errors() ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (strlen($error) > 0) : ?>
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (strlen($success) > 0) : ?>
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    <?php endif; ?>
</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Add <?php echo $view_model->get_heading();?></h5>
                <div class="card-body">
                <?= form_open() ?>
				<div class="form-group">
					<label for="Name">Name </label>
					<input type="text" class="form-control" id="form_name" name="name" value="<?php echo set_value('name'); ?>"/>
				</div>
				<div class="form-group">
					<label for="Address">Address </label>
					<input type="text" class="form-control" id="form_address" name="address" value="<?php echo set_value('address'); ?>"/>
				</div>
				<div class="form-group">
					<label for="2257 Address">2257 Address </label>
					<input type="text" class="form-control" id="form_address_2257" name="address_2257" value="<?php echo set_value('address_2257'); ?>"/>
				</div>
                <div class="form-row">
                    <div class="form-group col-6">
                        <label>Select Studio</label>
						<select class='multiple-select-control'  multiple="multiple" name='company_studios[]' value="<?php echo set_value('company_studios'); ?>" class="form-control">
                    		<?php foreach($this->_data['view_data']['studios'] as $studio ): ?>
                    			<option value="<?php echo $studio->id; ?>"><?php echo $studio->name; ?></option>
                    		<?php endforeach;?>
                		</select>
                    </div>
                    <div class="form-group col-6">
                        <label>Select Users</label>
						<select class='multiple-select-control'  multiple="multiple" name='company_users[]' value="<?php echo set_value('company_users'); ?>" class="form-control" >
                    		<?php foreach($this->_data['view_data']['users'] as $user ): ?>
                    			<option value="<?php echo $user->id; ?>"><?php echo $user->first_name . ' '. $user->last_name ; ?></option>
                    		<?php endforeach;?>
                		</select>        
                    </div>
                </div>    

                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Submit">
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
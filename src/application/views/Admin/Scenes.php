<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
?>
<h2><?php echo $view_model->get_heading();?></h2>
<br>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header"><?php echo $view_model->get_heading();?> Search</h5>
            <div class="card-body">
                <?= form_open('', ['method' => 'get']) ?>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="title">title </label>
							<input type="text" class="form-control" id="title" name="title" value="<?php echo $this->_data['view_model']->get_title() ;?>"/>
                        </div>    
                        <div class="form-group col-md-4">
                            <label for="Categories">Categories </label>
                            <select class='multiple-select-control' name='categories[]'  id="example-filter-placeholder" multiple="multiple">
								<?php foreach($this->_data["view_data"]['categories'] as $cat ): ?>
                    				<option <?php if(in_array($cat->id, (array) $this->_data['view_model']->get_categories())){ echo "selected"; }?> value="<?= $cat->id ?>"><?= $cat->name ?></option>
                    			<?php endforeach;?>
							</select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="Not Categories">Not Categories </label>
                            <select class='multiple-select-control' name='not_categories[]' value='<?php echo set_value('not_categories'); ?>' id="example-filter-placeholder" multiple="multiple">
								<?php foreach($this->_data["view_data"]['categories'] as $cat ): ?>
                    				<option <?php if(in_array($cat->id, (array) $this->_data['view_model']->get_not_categories())){ echo "selected"; }?> value="<?= $cat->id ?>"><?= $cat->name ?></option>
                    			<?php endforeach;?>
							</select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
							<label for="Status">Status </label>
                            <select name="status" class="form-control"  value='<?php echo $this->_data['view_model']->get_status(); ?>'>
								<option value="" <?php if($this->input->get('status') == ''){ echo 'selected'; } ?>>All</option>
								 <?php foreach($view_model->status_mapping() as $key => $value):?>
                                    <option value='<?php echo $key; ?>' <?php  if($this->input->get('status') !== '' && $this->_data['view_model']->get_status() == $key){ echo 'selected'; } ?>><?php echo $value ?></option>
                                 <?php endforeach;?>
							</select>
						</div>
                        <div class="form-group col-md-4">
                            <label for="Status">Group </label>  
                            <select class="form-control" id="group_id" name="group_id">
                                <option value=''>Choose...</option>
                               <?php foreach($this->_data["view_data"]['groups'] as $group):?> 
                                    <option  <?php if($this->_data['view_model']->get_group_id() ===  $group->id){ echo 'selected'; }?>    value='<?php echo $group->id;?>'><?php echo $group->name; ?></option>
                               <?php endforeach;?>   
                            </select>      
                        </div>
                        <div class="form-group col-md-4"> 
							<label for="Release date">Date Of Production </label>
							<input type="date" value='<?php echo $this->_data['view_model']->get_date_released(); ?>' class="form-control" id="date_released" name="date_released" />
						</div>        
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="type">Type </label>
                            <select class="form-control" id="type_id" name="type_id">
                               <option value=''>Choose...</option>
                               <?php foreach($this->_data["view_data"]['types'] as $type):?> 
                                    <option  <?php if($this->_data['view_model']-> get_type()  ===  $type->id){ echo 'selected'; }?>    value='<?php echo $type->id;?>'><?php echo $type->name; ?></option>
                               <?php endforeach;?>   
                            </select>  
                        </div>           
                    </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="form-group">
                            <input type="submit" name='btn-filter' class="btn btn-primary" value="Search">
                        </div>
                    </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mt-3">
        <div class="card">
            <h5 class="card-header">
                <div class="float-left"><?php echo $view_model->get_heading();?></div>
                <div class="float-right"><a class="btn btn-primary btn-sm" target="__blank" href="/admin/scenes/add"><i class="fas fa-plus-circle"></i></a></div>
                <div class="float-right">&nbsp;<div class="mkd-upload-form-btn-wrapper">	    <button class="mkd-upload-btn">Import</button>	    <input type="file" name="file_import" id="file_import" onchange="onFileImport(event, 'scenes')" accept=".csv"/></div>&nbsp;</div>
                <div class="clearfix"></div>
            </h5>
            <div class="card-body">
                &nbsp;<div class="mkd-upload-form-btn-wrapper">	    <button class="mkd-upload-btn">Import</button>	    <input type="file" name="file_import" id="file_import" onchange="onFileImport(event, 'scenes')" accept=".csv"/></div>&nbsp;
                <div class="table-responsive">
                    <table class="table table-hover table-condensed table-striped">
                        <thead>
                             <th>Thumbnail</th>
                             <th>Name</th>
                             <th>Studio Name</th>  
                             <th>Comp After Date</th>
                             <th>Next Studio</th>
                             <th>Status</th>
                             <th>Action</th>    
                        </thead>
                        <tbody>
                             <?php foreach($view_model->get_list() as $data):?> 
                                <tr>
                                    <td><?php echo $data->thumbnail; ?></td>
                                    <td><?php echo $data->title; ?></td>
                                    <td><?php echo $data->current_studio_name; ?></td>
                                    <td><?php echo $data->comp_after_date; ?></td>
                                    <td><?php echo $data->next_studio_name; ?></td>
                                    <td><?php echo $view_model->status_mapping()[$data->status]; ?></td>
                                    <td>
                                        <a class="btn btn-primary btn-sm" target="__blank" href="<?php echo base_url('/admin/scenes/edit/' . $data->id ); ?>">Edit</a>&nbsp;
                                        <a class="btn btn-warning btn-sm" target="__blank" href="<?php echo base_url('/admin/scenes/view/' . $data->id ); ?>">View</a>
                                    </td>
                                </tr>
                             <?php endforeach;?>  
                        </tbody>
                    </table>
                    <p class="pagination_custom"><?php echo $view_model->get_links(); ?></p>
                </div>
            </div>
        </div>
    </div>
</div>

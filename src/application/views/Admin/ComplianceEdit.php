<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
?>
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="page-header">
			<h2 class="pageheader-title"><?php echo $view_model->get_heading();?> </h2>
			<div class="page-breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/admin/dashboard" class="breadcrumb-link">Dashboard</a></li>
						<li class="breadcrumb-item"><a href="/admin/compliance" class="breadcrumb-link"><?php echo $view_model->get_heading();?></a></li>
						<li class="breadcrumb-item active" aria-current="page">Edit</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</div>
<div class="row">
    <?php if (validation_errors()) : ?>
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?= validation_errors() ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (strlen($error) > 0) : ?>
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (strlen($success) > 0) : ?>
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    <?php endif; ?>
</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Edit <?php echo $view_model->get_heading();?></h5>
                <div class="card-body">
                <?= form_open() ?>
				<div class="form-group">
					<label for="Legal Text">Legal Text </label>
					<textarea id='form_legal_text' name='legal_text' class='form-control' rows='5'><?php echo set_value('legal_text', $this->_data['view_model']->get_legal_text());?></textarea>
				</div>
				<div class="form-group">
					<label for="Signature">Signature </label>
					<img id="output_image" src="<?php echo set_value('image', $this->_data['view_model']->get_image());?>"/>
					<br/><div class="btn btn-info btn-sm mkd-choose-image" data-image-url="image" data-image-id="image_id" data-image-preview="output_image" data-view-width="400" data-view-height="150" data-boundary-width="600" data-boundary-height="150">Choose Image</div>
					<input type="hidden" id="image" name="image" value="<?php echo set_value('image', $this->_data['view_model']->get_image());?>"/>
					<input type="hidden" id="image_id" name="image_id" value="<?php echo set_value('image_id', $this->_data['view_model']->get_image_id());?>"/>
				</div>				<div class="form-group">
					<label for="Signer Name">Signer Name </label>
					<input type="text" class="form-control" id="form_signer_name" name="signer_name" value="<?php echo set_value('signer_name', $this->_data['view_model']->get_signer_name());?>"/>
				</div>
				<div class="form-group">
					<label for="File Name">File Name </label>
					<input type="text" class="form-control" id="form_file_name" name="file_name" value="<?php echo set_value('file_name', $this->_data['view_model']->get_file_name());?>"/>
				</div>


                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Submit">
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
?>
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="page-header" id="top">
			<h2 class="pageheader-title"><?php echo $view_model->get_heading();?> </h2>
			<div class="page-breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/admin/dashboard" class="breadcrumb-link">Dashboard</a></li>
						<li class="breadcrumb-item"><a href="/admin/users/0" class="breadcrumb-link"><?php echo $view_model->get_heading();?></a></li>
						<li class="breadcrumb-item active" aria-current="page">View</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header"><?php echo $view_model->get_heading();?> Details</h5>
                <div class="card-body">
						<h6>ID:&nbsp; <?php echo $view_model->get_id();?></h6>
						<h6>Email:&nbsp; <?php echo $view_model->get_email();?></h6>
						<h6>First Name:&nbsp; <?php echo $view_model->get_first_name();?></h6>
						<h6>Last Name:&nbsp; <?php echo $view_model->get_last_name();?></h6>
						<h6>Phone #:&nbsp; <?php echo $view_model->get_phone();?></h6>
						<h6>Role:&nbsp; <?php echo $view_model->role_id_mapping()[$view_model->get_role_id()];?></h6>
						<h6>Status:&nbsp; <?php echo $view_model->status_mapping()[$view_model->get_status()];?></h6>

                </div>
        </div>
    </div>
</div>
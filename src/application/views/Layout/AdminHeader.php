<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Internal tool</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" crossorigin="anonymous">
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" crossorigin="anonymous"></script>
    <!-- Our Vendor CSS -->
    <!-- Our Custom CSS -->
    	<link rel="stylesheet" href="/assets/css/style.css"/>
	<link rel="stylesheet" href="/assets/css/style-reset.css"/>
	<link rel="stylesheet" href="/assets/css/BsMultiSelect.min.css"/>
	<link rel="stylesheet" href="/assets/css/admin-dashboard.css"/>

</head>

<body>
<div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3>Puba LLC.</h3>
            </div>

            <ul class="list-unstyled components">
			<li><a href='/admin/dashboard' class='<?php echo ($page_name == 'Dashboard') ? 'active': '';?>'>Dashboard</a></li>
			<li class='<?php echo ($page_name == 'Fresh Content') ? "active" :"";?>' >
				<a href='#89684d3a5c2653b72b2fb281c1c85186' data-toggle='collapse' aria-expanded='false' class='dropdown-toggle'>Reports</a>
					<ul class='collapse list-unstyled <?php echo ($page_name == 'Fresh Content') ? "show" :"";?>' id='89684d3a5c2653b72b2fb281c1c85186'>
						<li><a href='/admin/dashboard' class='<?php echo ($page_name == 'Fresh Content') ? 'active': '';?>'>Fresh Content</a></li>
					</ul>
			</li>
			<li class='<?php echo ($page_name == 'Scene' || $page_name == 'Categories' || $page_name == 'Types' || $page_name == 'Groups') ? "active" :"";?>' >
				<a href='#18a2d32907e38f49bba79c5b92f8523a' data-toggle='collapse' aria-expanded='false' class='dropdown-toggle'>Scenes</a>
					<ul class='collapse list-unstyled <?php echo ($page_name == 'Scene' || $page_name == 'Categories' || $page_name == 'Types' || $page_name == 'Groups') ? "show" :"";?>' id='18a2d32907e38f49bba79c5b92f8523a'>
						<li><a href='/admin/scenes' class='<?php echo ($page_name == 'Scene') ? 'active': '';?>'>Scene</a></li>
						<li><a href='/admin/category' class='<?php echo ($page_name == 'Categories') ? 'active': '';?>'>Categories</a></li>
						<li><a href='/admin/type' class='<?php echo ($page_name == 'Types') ? 'active': '';?>'>Types</a></li>
						<li><a href='/admin/group' class='<?php echo ($page_name == 'Groups') ? 'active': '';?>'>Groups</a></li>
					</ul>
			</li>
			<li><a href='/admin/dvds' class='<?php echo ($page_name == 'DVD') ? 'active': '';?>'>DVD</a></li>
			<li><a href='/admin/actor' class='<?php echo ($page_name == 'Actors') ? 'active': '';?>'>Actors</a></li>
			<li class='<?php echo ($page_name == 'Compliance statements' || $page_name == 'Music Release' || $page_name == 'Staff Log' || $page_name == 'Admin Log' || $page_name == '2257 Log' || $page_name == 'Login Log') ? "active" :"";?>' >
				<a href='#0e949258ba1c5f1ea62cc12861d22c73' data-toggle='collapse' aria-expanded='false' class='dropdown-toggle'>Settings</a>
					<ul class='collapse list-unstyled <?php echo ($page_name == 'Compliance statements' || $page_name == 'Music Release' || $page_name == 'Staff Log' || $page_name == 'Admin Log' || $page_name == '2257 Log' || $page_name == 'Login Log') ? "show" :"";?>' id='0e949258ba1c5f1ea62cc12861d22c73'>
						<li><a href='/admin/compliance/edit/1' class='<?php echo ($page_name == 'Compliance statements') ? 'active': '';?>'>Compliance statements</a></li>
						<li><a href='/admin/musicrelease/edit/1' class='<?php echo ($page_name == 'Music Release') ? 'active': '';?>'>Music Release</a></li>
						<li><a href='/admin/activitylog/staff' class='<?php echo ($page_name == 'Staff Log') ? 'active': '';?>'>Staff Log</a></li>
						<li><a href='/admin/activitylog/admin' class='<?php echo ($page_name == 'Admin Log') ? 'active': '';?>'>Admin Log</a></li>
						<li><a href='/admin/activitylog/auditor' class='<?php echo ($page_name == '2257 Log') ? 'active': '';?>'>2257 Log</a></li>
						<li><a href='/admin/loginlog' class='<?php echo ($page_name == 'Login Log') ? 'active': '';?>'>Login Log</a></li>
					</ul>
			</li>
			<li class='<?php echo ($page_name == 'Networks' || $page_name == 'Studios' || $page_name == 'Companies' || $page_name == 'VOD Companies' || $page_name == 'Broadcast Companies' || $page_name == 'Producers' || $page_name == 'Tube sites' || $page_name == 'Viewshare sites') ? "active" :"";?>' >
				<a href='#1cc1eb6e9bf868b4b12ab2dbe029662d' data-toggle='collapse' aria-expanded='false' class='dropdown-toggle'>Companies</a>
					<ul class='collapse list-unstyled <?php echo ($page_name == 'Networks' || $page_name == 'Studios' || $page_name == 'Companies' || $page_name == 'VOD Companies' || $page_name == 'Broadcast Companies' || $page_name == 'Producers' || $page_name == 'Tube sites' || $page_name == 'Viewshare sites') ? "show" :"";?>' id='1cc1eb6e9bf868b4b12ab2dbe029662d'>
						<li><a href='/admin/network' class='<?php echo ($page_name == 'Networks') ? 'active': '';?>'>Networks</a></li>
						<li><a href='/admin/studio' class='<?php echo ($page_name == 'Studios') ? 'active': '';?>'>Studios</a></li>
						<li><a href='/admin/company' class='<?php echo ($page_name == 'Companies') ? 'active': '';?>'>Companies</a></li>
						<li><a href='/admin/vodcompany' class='<?php echo ($page_name == 'VOD Companies') ? 'active': '';?>'>VOD Companies</a></li>
						<li><a href='/admin/broadcastcompany' class='<?php echo ($page_name == 'Broadcast Companies') ? 'active': '';?>'>Broadcast Companies</a></li>
						<li><a href='/admin/producer' class='<?php echo ($page_name == 'Producers') ? 'active': '';?>'>Producers</a></li>
						<li><a href='/admin/tubesite' class='<?php echo ($page_name == 'Tube sites') ? 'active': '';?>'>Tube sites</a></li>
						<li><a href='/admin/viewsharesite' class='<?php echo ($page_name == 'Viewshare sites') ? 'active': '';?>'>Viewshare sites</a></li>
					</ul>
			</li>
			<li><a href='/admin/image' class='<?php echo ($page_name == 'Images') ? 'active': '';?>'>Images</a></li>
			<li><a href='/admin/users/0' class='<?php echo ($page_name == 'Users') ? 'active': '';?>'>Users</a></li>
			<li><a href='/admin/users2257/0' class='<?php echo ($page_name == '2257 Users') ? 'active': '';?>'>2257 Users</a></li>
			<li><a href='/admin/profile' class='<?php echo ($page_name == 'Profile') ? 'active': '';?>'>Profile</a></li>
			<li><a href='/admin/logout' class='<?php echo ($page_name == 'Logout') ? 'active': '';?>'>Logout</a></li>

            </ul>
            <span class="copyright"><?php echo $setting['copyright'];?></span>
            <span class="copyright"></span>
        </nav>
        <div id="content">
            <nav>
                <div class="row">
                    <div class="container-fluid">
                        <button type="button" id="sidebarCollapse" class="btn btn-dark">
                            <span>☰</span>
                        </button>
                    </div>
                </div>
            </nav>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Internal tool</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" crossorigin="anonymous">
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" crossorigin="anonymous"></script>
    <!-- Our Vendor CSS -->
    <!-- Our Custom CSS -->
    	<link rel="stylesheet" href="/assets/css/style.css"/>
	<link rel="stylesheet" href="/assets/css/style-reset.css"/>
	<link rel="stylesheet" href="/assets/css/BsMultiSelect.min.css"/>
	<link rel="stylesheet" href="/assets/css/admin-dashboard.css"/>

</head>

<body>
<div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3>Puba LLC.</h3>
            </div>

            <ul class="list-unstyled components">
			<li><a href='/staff/dashboard' class='<?php echo ($page_name == 'Dashboard') ? 'active': '';?>'>Dashboard</a></li>
			<li class='<?php echo ($page_name == 'Scene' || $page_name == 'Categories' || $page_name == 'Types' || $page_name == 'Groups') ? "active" :"";?>' >
				<a href='#554a98ed4d29eb84b4e8be0cb2b5f8a0' data-toggle='collapse' aria-expanded='false' class='dropdown-toggle'>Scenes</a>
					<ul class='collapse list-unstyled <?php echo ($page_name == 'Scene' || $page_name == 'Categories' || $page_name == 'Types' || $page_name == 'Groups') ? "show" :"";?>' id='554a98ed4d29eb84b4e8be0cb2b5f8a0'>
						<li><a href='/staff/scenes' class='<?php echo ($page_name == 'Scene') ? 'active': '';?>'>Scene</a></li>
						<li><a href='/staff/category' class='<?php echo ($page_name == 'Categories') ? 'active': '';?>'>Categories</a></li>
						<li><a href='/staff/type' class='<?php echo ($page_name == 'Types') ? 'active': '';?>'>Types</a></li>
						<li><a href='/staff/group' class='<?php echo ($page_name == 'Groups') ? 'active': '';?>'>Groups</a></li>
					</ul>
			</li>
			<li><a href='/staff/dvds' class='<?php echo ($page_name == 'DVD') ? 'active': '';?>'>DVD</a></li>
			<li><a href='/staff/actor' class='<?php echo ($page_name == 'Actors') ? 'active': '';?>'>Actors</a></li>
			<li class='<?php echo ($page_name == 'Networks' || $page_name == 'Studios' || $page_name == 'Companies' || $page_name == 'VOD Companies' || $page_name == 'Broadcast Companies' || $page_name == 'Producers' || $page_name == 'Tube sites' || $page_name == 'Viewshare sites') ? "active" :"";?>' >
				<a href='#018c08b3b36d768d32f5e683688972c2' data-toggle='collapse' aria-expanded='false' class='dropdown-toggle'>Companies</a>
					<ul class='collapse list-unstyled <?php echo ($page_name == 'Networks' || $page_name == 'Studios' || $page_name == 'Companies' || $page_name == 'VOD Companies' || $page_name == 'Broadcast Companies' || $page_name == 'Producers' || $page_name == 'Tube sites' || $page_name == 'Viewshare sites') ? "show" :"";?>' id='018c08b3b36d768d32f5e683688972c2'>
						<li><a href='/staff/network' class='<?php echo ($page_name == 'Networks') ? 'active': '';?>'>Networks</a></li>
						<li><a href='/staff/studio' class='<?php echo ($page_name == 'Studios') ? 'active': '';?>'>Studios</a></li>
						<li><a href='/staff/company' class='<?php echo ($page_name == 'Companies') ? 'active': '';?>'>Companies</a></li>
						<li><a href='/staff/vodcompany' class='<?php echo ($page_name == 'VOD Companies') ? 'active': '';?>'>VOD Companies</a></li>
						<li><a href='/staff/broadcastcompany' class='<?php echo ($page_name == 'Broadcast Companies') ? 'active': '';?>'>Broadcast Companies</a></li>
						<li><a href='/staff/producer' class='<?php echo ($page_name == 'Producers') ? 'active': '';?>'>Producers</a></li>
						<li><a href='/staff/tubesite' class='<?php echo ($page_name == 'Tube sites') ? 'active': '';?>'>Tube sites</a></li>
						<li><a href='/staff/viewsharesite' class='<?php echo ($page_name == 'Viewshare sites') ? 'active': '';?>'>Viewshare sites</a></li>
					</ul>
			</li>
			<li><a href='/staff/profile' class='<?php echo ($page_name == 'Profile') ? 'active': '';?>'>Profile</a></li>
			<li><a href='/staff/logout' class='<?php echo ($page_name == 'Logout') ? 'active': '';?>'>Logout</a></li>

            </ul>
            <span class="copyright"><?php echo $setting['copyright'];?></span>
            <span class="copyright"></span>
        </nav>
        <div id="content">
            <nav>
                <div class="row">
                    <div class="container-fluid">
                        <button type="button" id="sidebarCollapse" class="btn btn-dark">
                            <span>☰</span>
                        </button>
                    </div>
                </div>
            </nav>
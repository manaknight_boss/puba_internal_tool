<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Internal tool</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" crossorigin="anonymous">
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" crossorigin="anonymous"></script>
    <!-- Our Vendor CSS -->
    <!-- Our Custom CSS -->
    	<link rel="stylesheet" href="/assets/css/style.css"/>

</head>

<body>
<div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3>Puba LLC.</h3>
            </div>

            <ul class="list-unstyled components">
			<li><a href='/auditor/dashboard' class='<?php echo ($page_name == 'Dashboard') ? 'active': '';?>'>Dashboard</a></li>
			<li><a href='/auditor/dvd' class='<?php echo ($page_name == 'DVD') ? 'active': '';?>'>DVD</a></li>
			<li><a href='/auditor/profile' class='<?php echo ($page_name == 'Profile') ? 'active': '';?>'>Profile</a></li>
			<li><a href='/auditor/logout' class='<?php echo ($page_name == 'Logout') ? 'active': '';?>'>Logout</a></li>

            </ul>
            <span class="copyright"><?php echo $setting['copyright'];?></span>
            <span class="copyright"></span>
        </nav>
        <div id="content">
            <nav>
                <div class="row">
                    <div class="container-fluid">
                        <button type="button" id="sidebarCollapse" class="btn btn-dark">
                            <span>☰</span>
                        </button>
                    </div>
                </div>
            </nav>
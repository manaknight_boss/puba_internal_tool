    </div>
</div>
<div id="snackbar">Saved</div>
<div class="modal fade" id="mkd-media-gallery" tabindex="-1" role="dialog" aria-labelledby="media-gallery" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="media-gallery">Media Gallery</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid" id="mkd-media-gallery-container">
            <div class="row" id="mkd-media-gallery-wrapper">

            </div>
            <div class="text-center" id="mkd-load-more-container">
                <button class="btn btn-primary" id="mkd-load-more">Load More</button>
            </div>
          </div>
        <div class="container-fluid" id="mkd-media-upload-container">
            <div class="row" id="mkd-media-upload-wrapper">
              <div class="mkd-upload-btn-wrapper">
                <button class="mkd-upload-btn">Upload a file</button>
                <input type="file" name="imagefile" onchange="onFileSelected(event)"/>
              </div>
            </div>
        </div>
        <div class="container-fluid" id="mkd-media-crop-container">
            <div class="row" id="mkd-media-crop-wrapper">
              <div id="mkd-crop-upload-container-wrapper">
                <div id="mkd-crop-upload-container">
                </div>
              </div>
            </div>
        </div>
      </div>
      <div class="modal-footer mkd-media-panel-1">
          <button type="button" class="btn btn-primary" id="mkd-media-upload">Upload</button>
          <button type="button" class="btn btn-dark" id="mkd-media-choose">Choose</button>
          <button type="button" class="btn btn-warning mkd-close-modal" data-dismiss="modal">Close</button>
        </div>
        <div class="modal-footer mkd-media-panel-2">
          <button type="button" class="btn btn-warning mkd-close-modal" data-dismiss="modal">Close</button>
        </div>
        <div class="modal-footer mkd-media-panel-3">
          <button type="button" class="btn btn-primary js-crop" id="mkd-media-crop">Crop & Upload</button>
          <button type="button" class="btn btn-warning mkd-close-modal" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" crossorigin="anonymous"></script>
    <!-- Our JS -->
	<script src="/assets/js/config.js"></script>
	<script src="/assets/js/event-binding.js"></script>
	<script src="/assets/js/media.js"></script>
	<script src="/assets/js/core.js"></script>
	<script src="/assets/js/setting.js"></script>
	<script src="/assets/js/mkd-image-gallery.js"></script>
	<script src="/assets/js/multiple-file-upload.js"></script>
	<script src="/assets/js/core.js"></script>
	<script src="/assets/js/BsMultiSelect.min.js"></script>
	<script src="/assets/js/staff.js"></script>

</body>

</html>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
?>
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="page-header" id="top">
			<h2 class="pageheader-title"><?php echo $view_model->get_heading();?> </h2>
			<div class="page-breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/staff/dashboard" class="breadcrumb-link">Dashboard</a></li>
						<li class="breadcrumb-item"><a href="/staff/dvds/0" class="breadcrumb-link"><?php echo $view_model->get_heading();?></a></li>
						<li class="breadcrumb-item active" aria-current="page">View</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header"><?php echo $view_model->get_heading();?> Details</h5>
                <div class="card-body">
						<h6>Title:&nbsp; <?php echo $view_model->get_title();?></h6>
						<h6>Status:&nbsp; <?php echo $view_model->status_mapping()[$view_model->get_status()];?></h6>
						<h6>Studio:&nbsp; <?php echo $view_model->get_studio_id();?></h6>
						<h6>Release date:&nbsp; <?php echo $view_model->get_release_date();?></h6>
						<h6>Company:&nbsp; <?php echo $view_model->get_company_id();?></h6>
						<h6>DVD Type:&nbsp; <?php echo $view_model->get_type_id();?></h6>
						<h6>Filter After:&nbsp; <?php echo $view_model->get_filter_after();?></h6>
						<h6>Start Of Production:&nbsp; <?php echo $view_model->get_start_production_date();?></h6>
						<h6>End Of Production:&nbsp; <?php echo $view_model->get_end_production_date();?></h6>
						<h6>Image:&nbsp; <img class="img-fluid" src="<?php echo $view_model->get_cover_image();?>"/></h6>
						<h6>Is for comp:&nbsp; <?php echo $view_model->is_for_comp_mapping()[$view_model->get_is_for_comp()];?></h6>

                </div>
        </div>
    </div>
</div>
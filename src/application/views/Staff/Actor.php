<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
?>
<h2><?php echo $view_model->get_heading();?></h2>
<br>
<div class="clear"></div>
<?php if (strlen($error) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (strlen($success) > 0) : ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header"><?php echo $view_model->get_heading();?> Search</h5>
            <div class="card-body">
                <?= form_open('', ['method' => 'get']) ?>
                    <div class="row">
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Name">Name </label>
								<input type="text" class="form-control" id="name" name="name" value="<?php echo $this->_data['view_model']->get_name();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Stage Name">Stage Name </label>
								<input type="text" class="form-control" id="stage_name" name="stage_name" value="<?php echo $this->_data['view_model']->get_stage_name();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Gender">Gender </label>
								<select name="gender" class="form-control">
									<option value="">All</option>
									<?php foreach ($view_model->gender_mapping() as $key => $value) {
										echo "<option value='{$key}' " . (($view_model->get_gender() == $key && $view_model->get_gender() != '') ? 'selected' : '') . "> {$value} </option>";
									}?>
								</select>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Date of Birth">Date of Birth </label>
								<input type="date" class="form-control" id="dob" name="dob" value="<?php echo $this->_data['view_model']->get_dob();?>"/>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<div class="form-group">
								<label for="Expiry Date">Expiry Date </label>
								<input type="date" class="form-control" id="expiry_date" name="expiry_date" value="<?php echo $this->_data['view_model']->get_expiry_date();?>"/>
							</div>
						</div>

                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" value="Search">
                        </div>
                    </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mt-3">
        <div class="card">
            <h5 class="card-header">
                <div class="float-left"><?php echo $view_model->get_heading();?></div>
                <div class="float-right"><a class="btn btn-primary btn-sm" target="__blank" href="/staff/actor/add"><i class="fas fa-plus-circle"></i></a></div>
                <div class="float-right"></div>
                <div class="clearfix"></div>
            </h5>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover table-condensed table-striped">
                        <thead>
                        <?php foreach ($view_model->get_column() as $data) {
                            echo "<th>{$data}</th>";
                        } ?>
                        </thead>
                        <tbody>
                        <?php foreach ($view_model->get_list() as $data) { ?>
                            <?php
                            echo '<tr>';
							echo "<td>{$data->id}</td>";
							echo'<td>' . $data->name . "<br/>" . $data->stage_name . "<br/>" . $data->additional_name . "<br/>" . '</td>';
							echo "<td>{$view_model->gender_mapping()[$data->gender]}</td>";
							echo "<td>{$data->dob}</td>";
							echo "<td>{$data->expiry_date}</td>";
							echo "<td>{$view_model->status_mapping()[$data->status]}</td>";
							echo '<td>';
							echo '<a class="btn btn-primary btn-sm" target="__blank" href="/staff/actor/edit/' . $data->id . '">Edit</a>';
							echo '&nbsp;<a class="btn btn-warning btn-sm" target="__blank" href="/staff/actor/view/' . $data->id . '">View</a>';
							echo '&nbsp;<a class="btn btn-danger btn-sm" target="__blank" href="/staff/actor/delete/' . $data->id . '">Remove</a>';
							echo '</td>';
                            echo '</tr>';
                            ?>
                        <?php } ?>
                        </tbody>
                    </table>
                    <p class="pagination_custom"><?php echo $view_model->get_links(); ?></p>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
?>
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="page-header">
			<h2 class="pageheader-title"><?php echo $view_model->get_heading();?> </h2>
			<div class="page-breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/staff/dashboard" class="breadcrumb-link">Dashboard</a></li>
						<li class="breadcrumb-item"><a href="/staff/producer/0" class="breadcrumb-link"><?php echo $view_model->get_heading();?></a></li>
						<li class="breadcrumb-item active" aria-current="page">Edit</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</div>
<div class="row">
    <?php if (validation_errors()) : ?>
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?= validation_errors() ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (strlen($error) > 0) : ?>
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (strlen($success) > 0) : ?>
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    <?php endif; ?>
</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Edit <?php echo $view_model->get_heading();?></h5>
                <div class="card-body">
                <?= form_open() ?>
				<div class="form-group">
					<label for="Name">Name </label>
					<input type="text" class="form-control" id="form_name" name="name" value="<?php echo set_value('name', $this->_data['view_model']->get_name());?>"/>
				</div>
				<div class="form-group">
					<label for="Address">Address </label>
					<input type="text" class="form-control" id="form_address" name="address" value="<?php echo set_value('address', $this->_data['view_model']->get_address());?>"/>
				</div>
				<div class="form-group">
					<label for="Name">Name </label>
					<input type="text" class="form-control" id="form_phone" name="phone" value="<?php echo set_value('phone', $this->_data['view_model']->get_phone());?>"/>
				</div>
				<div class="form-group">
					<label for="Email">Email </label>
					<input type="text" class="form-control" id="form_email" name="email" value="<?php echo set_value('email', $this->_data['view_model']->get_email());?>"/>
				</div>
				<div class="form-group">
					<label for="Url">Url </label>
					<input type="text" class="form-control" id="form_url" name="url" value="<?php echo set_value('url', $this->_data['view_model']->get_url());?>"/>
				</div>
				<div class="form-group">
					<label for="Contact Name">Contact Name </label>
					<input type="text" class="form-control" id="form_contact_name" name="contact_name" value="<?php echo set_value('contact_name', $this->_data['view_model']->get_contact_name());?>"/>
				</div>
				<div class="form-group">
					<label for="Status">Status </label>
					<select id="form_status" name="status" class="form-control">
						<?php foreach ($view_model->status_mapping() as $key => $value) {
							echo "<option value='{$key}' " . (($view_model->get_status() == $key && $view_model->get_status() != '') ? 'selected' : '') . "> {$value} </option>";
						}?>
					</select>
				</div>
				<div class="form-group">
					<label for="Note">Note </label>
					<textarea id='form_note' name='note' class='form-control' rows='5'><?php echo set_value('note', $this->_data['view_model']->get_note());?></textarea>
				</div>


                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Submit">
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
?>
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="page-header" id="top">
			<h2 class="pageheader-title"><?php echo $view_model->get_heading();?> </h2>
			<div class="page-breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/staff/dashboard" class="breadcrumb-link">Dashboard</a></li>
						<li class="breadcrumb-item"><a href="/staff/actor/0" class="breadcrumb-link"><?php echo $view_model->get_heading();?></a></li>
						<li class="breadcrumb-item active" aria-current="page">View</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header"><?php echo $view_model->get_heading();?> Details</h5>
                <div class="card-body">
						<h6>Name:&nbsp; <?php echo $view_model->get_name();?></h6>
						<h6>Stage Name:&nbsp; <?php echo $view_model->get_stage_name();?></h6>
						<h6>Gender:&nbsp; <?php echo $view_model->gender_mapping()[$view_model->get_gender()];?></h6>
						<h6>Upload ID:&nbsp; <?php echo $view_model->get_image();?></h6>
						<h6>Date of Birth:&nbsp; <?php echo $view_model->get_dob();?></h6>
						<h6>Expiry Date:&nbsp; <?php echo $view_model->get_expiry_date();?></h6>
						<h6>Status:&nbsp; <?php echo $view_model->status_mapping()[$view_model->get_status()];?></h6>

                </div>
        </div>
    </div>
</div>
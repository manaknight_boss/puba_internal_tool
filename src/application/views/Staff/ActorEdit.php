<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
?>
<div class="row">
	<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
		<div class="page-header">
			<h2 class="pageheader-title"><?php echo $view_model->get_heading();?> </h2>
			<div class="page-breadcrumb">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/staff/dashboard" class="breadcrumb-link">Dashboard</a></li>
						<li class="breadcrumb-item"><a href="/staff/actor/0" class="breadcrumb-link"><?php echo $view_model->get_heading();?></a></li>
						<li class="breadcrumb-item active" aria-current="page">Edit</li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</div>
<div class="row">
    <?php if (validation_errors()) : ?>
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?= validation_errors() ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (strlen($error) > 0) : ?>
        <div class="col-md-12">
            <div class="alert alert-danger" role="alert">
                <?php echo $error; ?>
            </div>
        </div>
    <?php endif; ?>
    <?php if (strlen($success) > 0) : ?>
        <div class="col-md-12">
            <div class="alert alert-success" role="success">
                <?php echo $success; ?>
            </div>
        </div>
    <?php endif; ?>
</div>
<div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Edit <?php echo $view_model->get_heading();?></h5>
                <div class="card-body">
                <?= form_open() ?>
				<div class="form-group">
					<label for="Name">Name </label>
					<input type="text" class="form-control" id="form_name" name="name" value="<?php echo set_value('name', $this->_data['view_model']->get_name());?>"/>
				</div>
				<div class="form-group">
					<label for="Stage Name">Stage Name </label>
					<input type="text" class="form-control" id="form_stage_name" name="stage_name" value="<?php echo set_value('stage_name', $this->_data['view_model']->get_stage_name());?>"/>
				</div>
				<div class="form-group">
					<label for="Gender">Gender </label>
					<select id="form_gender" name="gender" class="form-control">
						<?php foreach ($view_model->gender_mapping() as $key => $value) {
							echo "<option value='{$key}' " . (($view_model->get_gender() == $key && $view_model->get_gender() != '') ? 'selected' : '') . "> {$value} </option>";
						}?>
					</select>
				</div>
				<div class="form-group">
					<div class="mkd-upload-form-btn-wrapper">
						<label for="Upload ID">Upload ID </label>
						<button class="mkd-upload-btn">Upload a file</button>
						<input type="file" name="image_upload" id="image_upload" onchange="onFileUploaded(event, 'image')" accept=".gif,.jpg,.jpeg,.png,.doc,.docx,.pdf,.md,.txt,.rtf,.xls,.xlsx,.xml,.json,.html,.mp3,.mp4,.csv,.bmp,.mpeg,.ppt,.pptx,.svg,.wav,.webm,.weba,.woff,.tiff"/>
					<input type="hidden" id="image" name="image" value="<?php echo set_value('image', $this->_data['view_model']->get_image());?>"/>
					<input type="hidden" id="image_id" name="image_id" value="<?php echo set_value('image_id', $this->_data['view_model']->get_image_id());?>"/>
					<span id="image_text" class="mkd-upload-filename"><?php echo set_value('image', $this->_data['view_model']->get_image());?></span>
					</div>
				</div>
				<div class="form-group">
					<label for="Date of Birth">Date of Birth </label>
					<input type="date" class="form-control" id="form_dob" name="dob" value="<?php echo set_value('dob', $this->_data['view_model']->get_dob());?>"/>
				</div>
				<div class="form-group">
					<label for="Expiry Date">Expiry Date </label>
					<input type="date" class="form-control" id="form_expiry_date" name="expiry_date" value="<?php echo set_value('expiry_date', $this->_data['view_model']->get_expiry_date());?>"/>
				</div>
				<div class="form-group">
					<label for="Status">Status </label>
					<select id="form_status" name="status" class="form-control">
						<?php foreach ($view_model->status_mapping() as $key => $value) {
							echo "<option value='{$key}' " . (($view_model->get_status() == $key && $view_model->get_status() != '') ? 'selected' : '') . "> {$value} </option>";
						}?>
					</select>
				</div>


                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Submit">
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
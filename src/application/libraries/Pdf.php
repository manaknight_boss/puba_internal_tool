<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Dompdf\Dompdf;

class Pdf{

    public function create_pdf($html, $filename='', $download=TRUE, $paper='A4', $orientation='portrait')
    {
        $dompdf = new Dompdf();
        $dompdf->load_html($html);
        $dompdf->set_paper($paper, $orientation);
        $dompdf->render();
        
        if($download === TRUE)
        {
            $dompdf->stream($filename.'.pdf', array('Attachment' => 1));
        }
        else
        {
            $dompdf->stream($filename.'.pdf', array('Attachment' => 0));
        }
    }
}


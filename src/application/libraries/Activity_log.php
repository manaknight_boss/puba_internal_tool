<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Activity_log{

    public static function log_activity($model,$action, $detail, $id, $user_agent)
    {   
        $ci_instance =& get_instance();
        $ci_instance->load->database();
        $ci_instance->load->model($model);

        return $ci_instance->$model->create([
            'user_id' =>  1,
            'action' =>   $action,
            'detail' =>   json_encode($detail),
            'last_ip' =>  $user_ip_address,
            'user_agent'=>  $ci_instance->agent->agent_string()
        ]);

    }
}

<?php


use Phinx\Seed\AbstractSeed;
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
/**
 * User Seeder
 *
 * @copyright 2019 Manaknightdigital Inc.
 * @link https://manaknightdigital.com
 * @license Proprietary Software licensing
 * @author Ryan Wong
 */
class UserSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
		[
			'email' => 'brian@puba.com',
			'password' => str_replace('$2y$', '$2b$', password_hash('a123456', PASSWORD_BCRYPT)),
			'type' => 'n',
			'first_name' => 'Brian',
			'last_name' => ' ',
			'phone' => '12345678',
			'image' => 'https://i.imgur.com/AzJ7DRw.png',
			'image_id' => 1,
			'refer' => 'admin',
			'profile_id' => 0,
			'verify' => 1,
			'role_id' => 2,
			'stripe_id' => '',
			'status' => 1,
			'created_at' => date('Y-m-j'),
			'updated_at' => date('Y-m-j H:i:s'),
		],
		[
			'email' => 'ryan@manaknight.com',
			'password' => str_replace('$2y$', '$2b$', password_hash('a123456', PASSWORD_BCRYPT)),
			'type' => 'n',
			'first_name' => 'Ryan',
			'last_name' => 'Wong',
			'phone' => '12345678',
			'image' => 'https://i.imgur.com/AzJ7DRw.png',
			'image_id' => 1,
			'refer' => 'manaknight',
			'profile_id' => 0,
			'verify' => 1,
			'role_id' => 2,
			'stripe_id' => '',
			'status' => 1,
			'created_at' => date('Y-m-j'),
			'updated_at' => date('Y-m-j H:i:s'),
		],
		[
			'email' => 'staff@puba.com',
			'password' => str_replace('$2y$', '$2b$', password_hash('a123456', PASSWORD_BCRYPT)),
			'type' => 'n',
			'first_name' => 'Staff',
			'last_name' => 'Member',
			'phone' => '12345678',
			'image' => 'https://i.imgur.com/AzJ7DRw.png',
			'image_id' => 1,
			'refer' => 'staff',
			'profile_id' => 0,
			'verify' => 1,
			'role_id' => 1,
			'stripe_id' => '',
			'status' => 1,
			'created_at' => date('Y-m-j'),
			'updated_at' => date('Y-m-j H:i:s'),
		],
		[
			'email' => '2257user@puba.com',
			'password' => str_replace('$2y$', '$2b$', password_hash('a123456', PASSWORD_BCRYPT)),
			'type' => 'n',
			'first_name' => '2257',
			'last_name' => 'User',
			'phone' => '12345678',
			'image' => 'https://i.imgur.com/AzJ7DRw.png',
			'image_id' => 1,
			'refer' => 'auditor',
			'profile_id' => 0,
			'verify' => 1,
			'role_id' => 3,
			'stripe_id' => '',
			'status' => 1,
			'created_at' => date('Y-m-j'),
			'updated_at' => date('Y-m-j H:i:s'),
		],

        ];
        $model = $this->table('user');
        $model->truncate();
        $model->insert($data)->save();
    }
}

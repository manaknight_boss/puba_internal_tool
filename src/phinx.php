<?php
/*Powered By: Manaknightdigital Inc. https://manaknightdigital.com/ Year: 2019*/
return [
    'paths'        => [
        'migrations' => '%%PHINX_CONFIG_DIR%%/db/migrations',
        'seeds'      => '%%PHINX_CONFIG_DIR%%/db/seeds',
    ],
    'environments' => [
        'default_migration_table' => 'phinxlog',
        'default_database'        => 'development',
        'development'             => [
            'adapter' => 'mysql',
            'host'    => 'localhost:3306',
            'name'    => 'puba_internal_tool',
            'user'    => 'root',
            'pass'    => 'root',
            'port'    => 3306,
            'charset' => 'utf8'
        ]
    ],
];